import { Directive, HostListener, ElementRef, Input, Renderer2 } from '@angular/core';
@Directive({
  selector: '[defaultImage]',
  host: {
    '(error)':'updateUrl()',
    '[src]':'src'
   }
})
export class DefaultImage {
  @Input() src:string;
  @Input() default:string="assets/images/def.png";

  constructor(renderer: Renderer2, elmRef: ElementRef) {
    /////renderer.setStyle(elmRef.nativeElement, 'width', '50px');
    //setStyle(elmRef.nativeElement, 'height', '50px');
  }
  
  updateUrl() {
    this.src = this.default;
  }
}