import { Directive, HostListener, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appArabicNumbers]'
})
export class ArabicNumbersDirective {

  @Input() appArabicNumbersEnabled: boolean = true;

  private regex: RegExp = new RegExp(/^[\u0621-\u064A\u0660-\u0669\s\{0-9} ]+$/g);
  private navigationKeys = [
    'Backspace',
    'Delete',
    'Tab',
    'Escape',
    'Enter',
    'Home',
    'End',
    'ArrowLeft',
    'ArrowRight',
    'Clear',
    'Copy',
    'Paste'
  ];
  inputElement: HTMLElement;
  constructor(public el: ElementRef) {
    this.inputElement = el.nativeElement;
  }

  @HostListener('keydown', ['$event'])
  onKeyDown(e: KeyboardEvent) {
    if (this.appArabicNumbersEnabled) {
      if (
        this.navigationKeys.indexOf(e.key) > -1 || // Allow: navigation keys: backspace, delete, arrows etc.
        (e.key === 'a' && e.ctrlKey === true) || // Allow: Ctrl+A
        (e.key === 'c' && e.ctrlKey === true) || // Allow: Ctrl+C
        (e.key === 'v' && e.ctrlKey === true) || // Allow: Ctrl+V
        (e.key === 'x' && e.ctrlKey === true) || // Allow: Ctrl+X
        (e.key === 'a' && e.metaKey === true) || // Allow: Cmd+A (Mac)
        (e.key === 'c' && e.metaKey === true) || // Allow: Cmd+C (Mac)
        (e.key === 'v' && e.metaKey === true) || // Allow: Cmd+V (Mac)
        (e.key === 'x' && e.metaKey === true) // Allow: Cmd+X (Mac)
      ) {
        // let it happen, don't do anything
        return;
      }
      let current: string = this.el.nativeElement.value;
      let next: string = current.concat(e.key);
      if (next && !String(next).match(this.regex)) {
        event.preventDefault();
      }
    }
  }

  @HostListener('paste', ['$event'])
  onPaste(event: ClipboardEvent) {
    if (this.appArabicNumbersEnabled) {
      event.preventDefault();
      const pastedInput: string = event.clipboardData
        .getData('text/plain');
      this.inputElement.focus();
      if (String(pastedInput).match(this.regex) == null) {
        event.preventDefault();
      } else {
        document.execCommand('insertText', false, pastedInput);
      }
    }
  }

  @HostListener('drop', ['$event'])
  onDrop(event: DragEvent) {
    if (this.appArabicNumbersEnabled) {
      event.preventDefault();
      const pastedInput: string = event.dataTransfer.getData('text');
      this.inputElement.focus();
      if (String(pastedInput).match(this.regex) == null) {
        event.preventDefault();
      } else {
        document.execCommand('insertText', false, pastedInput);
      }
    }
  }

}
