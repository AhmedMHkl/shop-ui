import { AfterViewInit, Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appMiniForm]'
})
export class MiniFormDirective implements AfterViewInit {
  constructor(private _renderer: Renderer2, private _el: ElementRef) {

  }

  ngAfterViewInit() {
    // Get parent of the original input element
    var parent = this._el.nativeElement.parentNode;

    // Create a div
    var divElement = this._renderer.createElement("div");

    // Add class "input-wrapper"
    this._renderer.addClass(divElement, "text-danger");

    // Add the div, just before the input
    this._renderer.insertBefore(parent, divElement, this._el.nativeElement);

    // Remove the input
    //this._renderer.removeChild(parent, this._el.nativeElement);

    // Remove the directive attribute (not really necessary, but just to be clean)
    //this._renderer.removeAttribute(this._el.nativeElement, "inputWrapper");

    // Re-add it inside the div
    // this._renderer.appendChild(divElement, this._el.nativeElement);

  }


  @HostListener("focus")
  onFocus() {
    console.log("event");
    this._renderer.removeClass(this._el.nativeElement, "text-danger");
  }


  @HostListener("blur", ["$event.target.value"])
  onBlur(value) {
    //this.el.value = this.currencyPipe.transform(value); 
    console.log(value);
  }

}
