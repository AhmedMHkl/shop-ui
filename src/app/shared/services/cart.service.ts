import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable, Subscriber } from 'rxjs';
import { map } from 'rxjs/operators';
import { CartItem } from '../classes/cart-item';
import { Product } from '../classes/product';

// Get product from Localstorage
let products = JSON.parse(localStorage.getItem("cartItem")) || [];

@Injectable({
  providedIn: 'root'
})

export class CartService {

  // Array
  public cartItems: BehaviorSubject<CartItem[]> = new BehaviorSubject([]);
  public observer: Subscriber<{}>;

  constructor(
    private toastrService: ToastrService,
    private translateService: TranslateService) {
    this.cartItems.subscribe(products => products = products);
  }

  // Get Products
  public getItems(): Observable<CartItem[]> {
    const itemsStream = new Observable(observer => {
      observer.next(products);
      observer.complete();
    });
    return <Observable<CartItem[]>>itemsStream;
  }

  // Add to cart
  public addToCart(product: Product, quantity: number): CartItem | boolean {
    var item: CartItem | boolean = false;
    // If Products exist
    let hasItem = products.find((cuurentItem, index) => {
      if (cuurentItem.product.id == product.id) {
        let qty = products[index].quantity + quantity;
        let stock = this.calculateStockCounts(products[index], quantity);
        if (qty != 0 && stock) {
          products[index]['quantity'] = qty;
          /* let msg = this.translateService.instant('This product has been added.');
           this.toastrService.success(msg);*/
        }
        return true;
      }
    });
    // If Products does not exist (Add New Products)
    if (!hasItem && product['available'] || product['productAvailabilities']) {
      if (typeof hasItem == "undefined") {
        item = { product: product, quantity: quantity };
        products.push(item);
      }
      let msg = this.translateService.instant('This product has been added.');
      this.toastrService.success(msg);
      localStorage.setItem("cartItem", JSON.stringify(products));
    } else {
      if (!product['available'] || !product['productAvailabilities']) {
        let msg = this.translateService.instant('This product is not available.');
        this.toastrService.error(msg);
      }

    }
    return item;
  }

  // Update Cart Value
  public updateCartQuantity(product: Product, quantity: number): CartItem | boolean {
    return products.find((items, index) => {
      if (items.product.id == product.id) {
        let qty = products[index].quantity + quantity;
        //let stock = this.calculateStockCounts(products[index], quantity);
        let stock = this.calculateStockCounts(product, qty);
        if (qty != 0 && stock)
          products[index]['quantity'] = qty;
        localStorage.setItem("cartItem", JSON.stringify(products));
        return true;
      }
    });
  }

  // Calculate Product stock Counts
  public calculateStockCounts(product: any, quantity): CartItem | Boolean {

    // checked in the backend
    /* let stock = product.quantity;
    if(stock < quantity) {
      this.toastrService.error('You can not add more items than available. In stock '+ stock +' items.');
      return false
    }*/
    return true
  }

  // Removed in cart
  public removeFromCart(item: CartItem) {
    if (item === undefined) return false;
    const index = products.indexOf(item);
    products.splice(index, 1);
    localStorage.setItem("cartItem", JSON.stringify(products));
  }

  // Total amount 
  public getTotalAmount(): Observable<number> {
    return this.cartItems.pipe(map((product: CartItem[]) => {
      return products.reduce((prev, curr: CartItem) => {
        return prev + curr.product.price * curr.quantity;
      }, 0);
    }));
  }


}