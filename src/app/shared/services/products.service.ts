import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable, Subscriber } from 'rxjs';
import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';
import { CategoryService } from 'src/app/core/services/category.service';
import { environment } from 'src/environments/environment';
import { Category } from '../classes/category';
import { Product } from '../classes/product';
import { Product2 } from '../classes/product2';

// Get product from Localstorage
let products = JSON.parse(localStorage.getItem("compareItem")) || [];

@Injectable()

export class ProductsService {
  private endPoint = "/api/v1";
  private url = environment.apiUrl;
  private currentLanguage = localStorage.getItem("currentLanguage")
  public currency: string = 'USD';
  public catalogMode: boolean = false;

  public compareProducts: BehaviorSubject<Product[]> = new BehaviorSubject([]);
  public observer: Subscriber<{}>;

  // Initialize 
  constructor(private http: HttpClient,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private categoryService: CategoryService) {
    this.compareProducts.subscribe(products => products = products);
  }

  // Observable Product Array
  private products(): Observable<Product[]> {
    return this.http.get<Product[]>('assets/data/products.json');

  }

  public products2(): Observable<Product2[]> {
    //return this.http.get('assets/data/products.json').map((res:any) => res.json())
    return this.http.get<Product2[]>(`${this.url}${this.endPoint}/products`);
  }

  public newProducts(): Observable<Product2[]> {
    //return this.http.get('assets/data/products.json').map((res:any) => res.json())
    return this.http.get<Product2[]>(`${this.url}${this.endPoint}/products?newProduct=true`);
  }

  // Get Products
  public getProducts(): Observable<Product[]> {
    return this.products();
  }

  // Get Products By Id
  public getProduct(id: number): Observable<Product> {
    return this.products().pipe(map(items => { return items.find((item: Product) => { return item.id === id; }); }));
  }

  // Get Products By category
  public getProductByCategory(category: string): Observable<Product2[]> {
    return this.products2().pipe(map(items =>
      items['products'].filter((item: Product2) => {
        if (category == 'all') return item;
        else {
          if (item.categories)
            for (let index = 0; index < item.categories.length; index++) {
              let cat: Category = item.categories[index];
              let allRelatedCats: any[] = [];
              if (cat.parent)
                allRelatedCats.push(cat.parent.code)
              this.categoryService.getCategoriesChild(cat, allRelatedCats);
              if (cat['code'] == category || (allRelatedCats.indexOf(category) != -1)) {
                return item;
              }
            }
        }//
      })
    ));
  }

  /*
     ---------------------------------------------
     ----------  Compare Product  ----------------
     ---------------------------------------------
  */

  // Get Compare Products
  public getComapreProducts(): Observable<Product[]> {
    const itemsStream = new Observable(observer => {
      observer.next(products);
      observer.complete();
    });
    return <Observable<Product[]>>itemsStream;
  }

  // If item is aleready added In compare
  public hasProduct(product: Product): boolean {
    const item = products.find(item => item.id === product.id);
    return item !== undefined;
  }

  // Add to compare
  public addToCompare(product: Product): Product | boolean {
    var item: Product | boolean = false;
    if (this.hasProduct(product)) {
      item = products.filter(item => item.id === product.id)[0];
      const index = products.indexOf(item);
    } else {
      if (products.length < 3) {
        products.push(product);
        this.toastrService.success(this.translateService.instant('product has been  added successfully To compare page')); // toasr services

      } else
        this.toastrService.warning(this.translateService.instant('Maximum 3 products are in compare.')); // toasr services
    }
    localStorage.setItem("compareItem", JSON.stringify(products));
    return item;
  }

  // Removed Product
  public removeFromCompare(product: Product) {
    if (product === undefined) { return; }
    const index = products.indexOf(product);
    products.splice(index, 1);
    localStorage.setItem("compareItem", JSON.stringify(products));
  }

  public getCategoryVariants(cat: any): Observable<any> {
    return this.http.get(`${this.url}${this.endPoint}/category/name/${cat}/variants`)
  }

  public getCategoryVariantsById(cat: any): Observable<any> {
    return this.http.get(`${this.url}${this.endPoint}/category/${cat}/variants`, {
      params: new HttpParams()
        .set('lang', this.currentLanguage || 'ar')
    });
  }

  public getCategoryById(cat: any): Observable<any> {
    return this.http.get(`${this.url}${this.endPoint}/category/${cat}`, {
      params: new HttpParams()
        .set('lang', this.currentLanguage)
    })
  }

  public getManufacturer(): Observable<any> {
    return this.http.get<any>(`${this.url}${this.endPoint}/manufacturers/getLight`);
  }
  getManufacturerById(id: any): Observable<any> {
    return this.http.get<any>(`${this.url}${this.endPoint}/manufacturers/${id}`, {
      params: new HttpParams()
        .set('lang', this.currentLanguage)
    });
  }

}