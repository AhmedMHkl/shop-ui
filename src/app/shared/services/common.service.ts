import { Injectable } from '@angular/core';
import { BaseService } from 'src/app/core/services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService extends BaseService<any> {
  private endPoint = "api/v1";
  constructor(protected http: HttpClient) {
    super(http, 'api/v1');
  }


  SendsAnEmailContactUsToStoreOwner(content: any): Observable<any> {
    return this.http.post<any>(`${this.url}/${this.endPoint}/customer/contact`, content);
  }
}
