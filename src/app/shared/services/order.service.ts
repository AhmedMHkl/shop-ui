import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AppConstants } from 'src/app/core/utils/AppConstants';
import { environment } from 'src/environments/environment';
import { Order } from '../classes/order';
import { CartService } from './cart.service';

@Injectable({
  providedIn: 'root'
})

export class OrderService {

  // Array
  public OrderDetails;
  private host = environment.apiUrl;

  constructor(
    private router: Router,
    private http: HttpClient,
    private cartService: CartService

  ) { }

  // Get order items
  public getOrderItems(): Order {
    return this.OrderDetails;
  }

  // Create order
  public createOrder(product: any, details: any, orderId: any, amount: any) {
    var item = {
      shippingDetails: details,
      product: product,
      orderId: orderId,
      totalAmount: amount
    };
    this.OrderDetails = item;
    console.log("Shipping Infoooooooooooooooooooo ..............", item);
    // save order in backend
    const cartId = localStorage.getItem("cartId");
    const orderInfo = {
      comments: "abcd",
      currency: "CAD",
      customerAgreement: true,
      delivery: JSON.parse(details),
      id: 0,
      payment: {
        amount: amount,
        paymentModule: "paypal-express-checkout",
        paymentToken: orderId,
        paymentType: "PAYPAL",
        transactionType: "CAPTURE",

      },
      shippingQuote: 0
    }
    this.shopizerProcessOrder(cartId, orderInfo).subscribe(res => {
      console.log(res);
      localStorage.removeItem(AppConstants.CART_ID);
      //localStorage.removeItem(AppConstants.CARD_ITEM);
      localStorage.removeItem(AppConstants.CART_CODE);
      JSON.parse(localStorage.getItem("cartItem")).forEach(element => {
        this.cartService.removeFromCart(element);
      });
      localStorage.removeItem(AppConstants.SHIPPING_DETAILS);
      this.router.navigate(['/home/checkout/success']);
    },
      error => {
        localStorage.removeItem(AppConstants.SHIPPING_DETAILS);
      });
  }

  shopizerProcessOrder(cartId: any, orderInfo: any): Observable<any> {
    return this.http.post(`${this.host}/api/v1/auth/cart/${cartId}/checkout`, orderInfo);
  }

  generateToken(cartId: any): Observable<any> {
    return this.http.get(`${this.host}/api/v1/auth/order/cart/${cartId}/payment/init`);
  }

  // /auth/orders
  listCustomerOrders(): Observable<any> {
    return this.http.get(`${this.host}/api/v1/auth/orders`);
  }

  stripePayment(){
    return this.http.get(`${this.host}/api/v1/createUser`);
  }


}
