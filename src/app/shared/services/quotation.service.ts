import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppConstants } from 'src/app/core/utils/AppConstants';
import { environment } from 'src/environments/environment';
import { QuotationItem } from '../classes/quotation-item';
import { Product } from '../classes/product';

let quotations = JSON.parse(localStorage.getItem("quotationItems")) || [];

@Injectable({
  providedIn: 'root'
})
export class QuotationService {

  private endPoint = "/api/v1";
  private url = environment.apiUrl;
  public quotationItem: BehaviorSubject<QuotationItem[]> = new BehaviorSubject([]);

  constructor(private toastrService: ToastrService, 
    private translateService: TranslateService,
    private httpClient : HttpClient) { 
      this.quotationItem.subscribe(quotations => quotations = quotations);
    }

  // Add to quote list
  public addToQuoteList(product: Product) {
    if (this.alreadyExist(product)) {
      this.toastrService.warning(this.translateService.instant('This product has been added before'));
    } else {
      let quote : QuotationItem = new QuotationItem();
      quote.product = product;
      quote.quotation.customerId = JSON.parse(localStorage.getItem(AppConstants.LOGGED_IN_USER_Id));
      quotations.push(quote);
      this.toastrService.success(this.translateService.instant('This product added to Quote list'));
      localStorage.setItem("quotationItems", JSON.stringify(quotations));
    }
  }

  // If item is aleready added In Quote list
  public alreadyExist(product: Product): boolean {
    const item = quotations.find(item => item.product.id === product.id);
    return item !== undefined;
  }

  // Get  Quotation Products
  public getProducts(): Observable<QuotationItem[]> {
    const itemsStream = new Observable(observer => {
      observer.next(quotations);
      observer.complete();
    });
    return <Observable<QuotationItem[]>>itemsStream;
  }

  // Set  Quotation Products
  public setProducts(quotationItems) {
    quotations = quotationItems;
    localStorage.setItem("quotationItems", JSON.stringify(quotations));
    this.quotationItem.subscribe(quotations => quotations = quotations);
    console.log("quotations", quotations);
    
  }

  // Removed Product
  public removeFromQuotations(index: number) {
    quotations.splice(index, 1);
    localStorage.setItem("quotationItems", JSON.stringify(quotations));
  }

  // save quoteList
  public saveQuoteList(quotationItems : any) : Observable<any> {
    return this.httpClient.post<any>(`${this.url}${this.endPoint}/quotation/items`, quotationItems, { observe: 'response' });
  }

  // get quotation history list
  public getQuotationHistoryByCustomerId(page: number, pageSize: number) : Observable<any> {
    return this.httpClient.get<any>(`${this.url}${this.endPoint}/quotations/`+JSON.parse(localStorage.getItem(AppConstants.LOGGED_IN_USER_Id))+`/`+page+`/`+pageSize);
  }

  // get quotation history list by mohammed
  public getQuotationByCustomerId() : Observable<any> {
    return this.httpClient.get<any>(`${this.url}${this.endPoint}/quotations-list/`+JSON.parse(localStorage.getItem(AppConstants.LOGGED_IN_USER_Id)));
  }

  // get quotation items list by mohammed
  public getQuotationItemsByQuotationId(quotationId: number) : Observable<any> {
    return this.httpClient.get<any>(`${this.url}${this.endPoint}/quotation/items/`+quotationId);
  }

  public Unix_time(t) {
    var dt = new Date(t);
    const year = dt.getFullYear();
    const mon = dt.getMonth();
    const day = dt.getDate();
    return `${day}/${mon}+1/${year}`;
  }

  public updateQuotQuantity(quotation: QuotationItem, quantity: number): QuotationItem | boolean {
    return quotations.find((items, index) => {
      if (items.product.id == quotation.product.id) {
        let qty = quotations[index].count + quantity;
        if (qty != 0)
        quotations[index].count = qty;
        localStorage.setItem("cartItem", JSON.stringify(quotations));
        return true;
      }
    });
  }

  // Total amount 
  public getTotalAmount(quantity: number): Observable<number> {
    return this.quotationItem.pipe(map((quotationItem: QuotationItem[]) => {
      return quotations.reduce((prev, curr: QuotationItem) => {
          return prev + curr.product.price * curr.count;
      }, 0);
    }));
  }
  
}
