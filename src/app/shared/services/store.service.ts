import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  private endPoint = "/api/v1";
  private url = environment.apiUrl;

  constructor(
    private http: HttpClient,

  ) { }


  storeBrandingAndMarketingDetails(code: string): Observable<any> {
    return this.http.get<any>(`${this.url}${this.endPoint}/private/store/${code}/marketing`);

  }
}
