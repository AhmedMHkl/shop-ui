import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ProductReview } from '../classes/product-review';

@Injectable({
  providedIn: 'root'
})
export class ProductReviewService {

  private endPoint = "/api/v1"
  private serverUrl = environment.apiUrl;

  constructor(
    private http: HttpClient,
  ) { }

  
  public getProductReview(productId: number): Observable<any> {
    return this.http.get(`${this.serverUrl}${this.endPoint}/products/${productId}/reviews`);
  }

  public productReview(review: ProductReview): Observable<any> {
    return this.http.post(`${this.serverUrl}${this.endPoint}/auth/products/${review.productId}/reviews`, review);
  }

  public removeReview(review: ProductReview): Observable<any> {
    return this.http.delete(`${this.serverUrl}${this.endPoint}/auth/products/${review.productId}/reviews/${review.id}`);
  }

  public updateReview(review: ProductReview): Observable<any> {
    return this.http.put(`${this.serverUrl}${this.endPoint}/auth/products/${review.productId}/reviews/${review.id}`, review);
  }

}
