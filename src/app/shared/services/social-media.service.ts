import { Injectable } from '@angular/core';
import { BaseService } from 'src/app/core/services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocialMediaService 
  extends BaseService<any> {
    private endPoint = "api/v1";
    constructor(protected http: HttpClient) {
      super(http, 'api/v1');
    }
  
  
    public SotialMediaUrls(): Observable<any> {
      return this.http.get<any>(`${this.url}/${this.endPoint}/config`);
    }
}
