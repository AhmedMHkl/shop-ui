import { Component, OnInit } from '@angular/core';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { EchoMessageService } from 'src/app/core/services/echo-message.service';
import { SocialMediaService } from 'src/app/shared/services/social-media.service';

@Component({
  selector: 'app-social',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.scss']
})
export class SocialComponent implements OnInit {

  constructor(private sotialMediaService: SocialMediaService,
    private echoMessageService: EchoMessageService) { }
  entity: any;
  facebook: any;
  google: any;
  instgram: any;
  pinterest: any;

  ngOnInit() {
    this.sotialMediaService.SotialMediaUrls().subscribe(res => {
      this.facebook = res.facebook;
      this.google = res.ga;
      this.instgram = res.instagram
      this.pinterest = res.pinterest;
      //  this.entity=res;
      console.log(res)
    })
  }

  goToLink(link) {
    console.log(this.facebook)
    if (link == 'facebook') {
      window.open(this.facebook, "_blank");
    }
    else if (link == 'google') {
      window.open(this.google, "_blank");
    }
    else if (link == 'instgram') {
      window.open(this.instgram, "_blank");
    }
    else if (link == 'pinterest') {
      window.open(this.pinterest, "_blank");
    }
    else {
      this.echoMessageService.warningMessage("The link is not currently available", "sorry");
    }
  }


}
