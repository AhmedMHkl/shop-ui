import { Component, OnInit } from '@angular/core';
import { CommonCoreService } from 'src/app/core/services/common-core.service';
import { CategoryService } from 'src/app/core/services/category.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  private endPoint: string = '/api/v1';
  public footerCategories: [];
  http: any;
  constructor(private categoryService: CategoryService,
  ) {

  }

  ngOnInit() {
    this.categoryService.getCategoryHierarchy().subscribe(res => {
      console.log("getCategoryHierarchy : ", res);
      this.footerCategories = res
    })
  };

  onActivate(event) {
    window.scroll(0, 0);
  }
}
