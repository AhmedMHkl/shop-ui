import { Component, OnInit } from '@angular/core';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { OptinModel } from '../../model/optin-model';
import { EchoMessageService } from 'src/app/core/services/echo-message.service';
import { StoreService } from '../../services/store.service';

@Component({
  selector: 'app-footer-one',
  templateUrl: './footer-one.component.html',
  styleUrls: ['./footer-one.component.scss']
})
export class FooterOneComponent implements OnInit {

  optin: OptinModel = new OptinModel();
  constructor(
    private echoProductsService: EchoProductsService,
    private alerMessage: EchoMessageService,
    private storeService: StoreService,


  ) { }

  ngOnInit() {
    //this.storeService.storeBrandingAndMarketingDetails("DEFAULT").subscribe(res => console.log(res));



    this.optin.firstName = 'mohammed';
    this.optin.lastName = 'elhusary';



    // get anonymus cart from backend
    /* let isLoggedin = localStorage.getItem("isLoggedin");
    let cartCode = localStorage.getItem("cartCode");
    let customerCartItems = JSON.parse(localStorage.getItem("cartItem"));
    if (!isLoggedin && !customerCartItems && cartCode) {
      const cartItems = [];
      this.echoProductsService.getShopingCartByCode(cartCode).subscribe(
        res => {
          console.log(res);
          res.products.forEach(element => {
            const item = { product: element, quantity: element.quantity }
            cartItems.push(item);
          });
          localStorage.setItem("cartItem", JSON.stringify(cartItems));
          localStorage.setItem("cartCode", res.code);
        },
        error => {
          console.log(error);
        });
    } */
  }

  onSubmit() {
    this.echoProductsService.createNewsLetter(this.optin).subscribe(
      res => {
        console.log("mmmmmmmmmmmmmmmmmmm", res);
        this.alerMessage.successMessage("Subscribeed Done", "Subscribe");
      }, err => {
        console.log("errrrrrrrrrrrrrrrrror", err);
      }
    );
  }

}
