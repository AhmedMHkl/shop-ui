export class ProductCriteria {
    public startIndex: number = 0;
    public maxCount: number = 0;
    public code: string;
    public name: string;
    public language: string;
    public user: string;
    public storeCode: string;
    public productName: string;
    public productPrice: number;
    public sortByProductName: string;
    public sortByProductPrice: number;
    public available: boolean = null;
    public newProduct: boolean = null;
    // public List<AttributeCriteria> attributeCriteria;
    // public List<Long> categoryIds;
    // public List<string> categoryNames;
    // public List<string> availabilities;
    // public List<Long> productIds;
    public status: string;
    public manufacturerId: number = null;
    public ownerId: number = null;

}
