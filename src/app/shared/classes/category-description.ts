export class CategoryDescription {
    id?: number;
    language?: string;
    name?: string;
    description?: string;
    friendlyUrl?: string;
    highlights?: string;
    keyWords?: string
    metaDescription?: string;
    title?: string;
}
