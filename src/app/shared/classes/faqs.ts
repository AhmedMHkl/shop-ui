export class FAQs {
    question: string;
    answer: string;
    visible: boolean;
    id: number;
}
