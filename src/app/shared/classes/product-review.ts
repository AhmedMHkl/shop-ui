export class ProductReview {
    customerId?: number = null;
    date?: string;
    description?: string = null;
    id?: number = null;
    language?: string = null;
    productId?: number = null;
    rating?: number = 1;
    title?: string = null
}
