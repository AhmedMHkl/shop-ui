export class Description {
    id?: number;
    language?: string;
    name?: string;
    description?: string;
    friendlyUrl?: string;
    highlights?: string;
    keyWords?: string
    metaDescription?: string;
    title?: string;
}
