export class OptionValue {
    id?: number;
    code?: string;
    defaultValue?: boolean;
    sortOrder?: number;
    description?: string;
    name?: string;
    lang?: string;
    price?: number;
    image?: string;
}
