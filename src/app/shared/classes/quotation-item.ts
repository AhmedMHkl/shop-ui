import { Quotation } from "./quotation";
import { Product } from "./product";

export class QuotationItem {
    id: number = null;
    product: Product = null;
    quotation: Quotation = new Quotation();
    count: number = 1;
    createdDate: Date = null;
    newPrice: any;
}
