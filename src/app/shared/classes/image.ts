export class Image {
    defaultImage?: boolean;
    externalUrl?: string;
    id?: number
    imageName?: string;
    imageType?: number;
    imageUrl?: string;
    videoUrl?: string;
}
