import { BaseEntity } from "./base-entity";
import { Description } from "./description";
import { Option } from "./option";
import { Manufacturer } from "./product2";
import { Image } from "./image";
import { Category } from "./category";

export class Product extends BaseEntity {
    name?: string;
    price?: number;
    prices: any[];
    salePrice?: number;
    discount?: number;
    discounted: any;
    pictures?: string;
    shortDetails?: string;
    //description?: string;
    description?: Description;
    quantity?: number;
    new?: boolean;
    sale?: boolean;
    category?: string;
    colors?: any[];
    size?: any[];
    tags?: any[];
    variants?: any[];
    descriptions?: Description[];
    manufacturer?: Manufacturer;
    categories?: Category[];
    productColor?: string;
    images?: Image[];
    options?: Option[];
    originalPriceAmount: any;
    discountPercentage: any;
    rating: any;
    stock: any;
}
