import { BaseComponent } from "./base-component";
import { WishlistService } from "../services/wishlist.service";
import { EchoProductsService } from "src/app/services/echo-products.service";
import { CartService } from "../services/cart.service";
import { AuthService } from "src/app/auth/auth.service";
import { Router } from "@angular/router";
import { Product } from "./product";
import { ProductsService } from "../services/products.service";
import { QuotationService } from "../services/quotation.service";

export class BaseComponentWithCart extends BaseComponent {

    productOPtionsArr: any[] = [];

    constructor(
        protected wishlistService: WishlistService,
        protected echoProductService: EchoProductsService,
        protected cartService: CartService,
        protected authService: AuthService,
        protected router: Router,
        protected productsService: ProductsService,
        protected quotationService: QuotationService
    ) {
        super()
    }

    // Add to cart
    public addToCart(product: Product, quantity) {
        if (quantity == 0) return false;
        //const cartItem2 : CartItem = { attributes: this.productOPtionsArr, product: product.id, quantity: quantity };
        //this.cartService.removeFromCart(cartItem2);
        let res = this.cartService.addToCart(product, parseInt(quantity));
        // >>>>>>> backend
        const cartItem = { attributes: this.productOPtionsArr, product: product.id, quantity: quantity };
        console.log("cartItem :- ", cartItem);
        const customerId = this.authService.loggedInUserId();
        //if(res){
        if (customerId !== null && customerId !== undefined && customerId !== "") {
            // add tocustomer cart
            this.echoProductService.addLoggedInCustomerProductToCart(customerId, cartItem).subscribe(res => {
                console.log(res)
                localStorage.setItem("cartCode", res.code);
                localStorage.setItem("cartId", res.id);
            });
        }
        else {
            // add to Anonymous cart
            this.echoProductService.addAnonymousProductToCart(cartItem).subscribe(res => {
                console.log(res)
                localStorage.setItem("cartCode", res.code);
                localStorage.setItem("cartId", res.id);
            });
        }
        //}
    }

    public addToWishlist(product: Product) {
        this.wishlistService.addToWishlist(product);
    }

    public buyNow(product: Product, quantity) {
        if (quantity > 0)
            this.router.navigate(['/home/checkout']);
    }

    public addToCompare(product: Product) {
        this.productsService.addToCompare(product);
    }

    public addToQuotationList(product: Product){
        this.quotationService.addToQuoteList(product);
      }

}
