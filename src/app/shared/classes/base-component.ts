export class BaseComponent {
    protected currentCustomerId: string = "-1";

    protected getCurrentCustomerId() {
        let currentCustomerId = localStorage.getItem("loggedInUserId")
        if (currentCustomerId != null && currentCustomerId != null && currentCustomerId != '')
            this.currentCustomerId = currentCustomerId
    }
}
