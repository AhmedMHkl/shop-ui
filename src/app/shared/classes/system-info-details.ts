import { SystemInfo } from "./system-info";

export class SystemInfoDetails {
    id:number;
    descriptions:{
        id: number;
        name: string;
        title: string;
        description: string;
        language: string;
    };
    readableSystemInfo: SystemInfo = new SystemInfo();
}
