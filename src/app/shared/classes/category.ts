import { CategoryDescription } from "./category-description";

export class Category {
    id?: number;
    code?: string;
    sortOrder?: number;
    productCount?: number;
    featured?: boolean;
    visible?: boolean;
    depth?: number;
    description?: CategoryDescription;
    children: Category[];
    parent: Category;
}
