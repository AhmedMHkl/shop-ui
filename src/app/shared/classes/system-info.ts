export class SystemInfo {
    name: string;
    title: string;
    type: string;
    id:number;
    descriptions:{
        id: number;
        name: string;
        title: string;
        description: string;
        language: string;
    }
}
