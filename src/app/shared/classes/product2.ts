import { Category } from "./category";

// Product Colors
export type ProductColor = 'white' | 'black' | 'red' | 'green' | 'purple' | 'yellow' | 'blue' | 'gray' | 'orange' | 'pink';

// Product Size
export type ProductSize = 'M' | 'L' | 'XL';

// Product Tag
export type ProductTags = 'nike' | 'puma' | 'lifestyle' | 'caprese';

// // Product
// export interface Product {
//   id?: number;
//   name?: string;
//   price?: number;
//   prices: any[];
//   salePrice?: number;
//   discount?: number;
//   discounted: any;
//   pictures?: string;
//   shortDetails?: string;
//   //description?: string;
//   description?: Description;
//   quantity?: number;
//   new?: boolean;
//   sale?: boolean;
//   category?: string;
//   colors?: ProductColor[];
//   size?: ProductTags[];
//   tags?: ProductSize[];
//   variants?: any[];
//   descriptions?: Description[];
//   manufacturer?: Manufacturer;
//   categories?: Category[];
//   productColor?: string;
//   images?: Images[];
//   options?: Options[];
//   originalPriceAmount: any;
//   discountPercentage: any;
//   rating: any;
//   stock: any;
// }

export interface Product2 {
  id?: number;
  name?: string;
  price?: number;
  prices: any[];
  salePrice?: number;
  discount?: number;
  discounted: any;
  pictures?: string;
  shortDetails?: string;
  // description?: Description;
  // descriptions?: Description[];
  manufacturer?: Manufacturer;
  //stock?: number;
  quantity?: number;
  new?: boolean;
  sale?: boolean;
  categories?: Category[];
  // colors?: ProductColor[];
  // size?: ProductTags[];
  // tags?: ProductSize[];
  variants?: any[];
  productColor?: string;
  // images?: Images[];
  // options?: Options[];
  originalPriceAmount: any;
  discountPercentage: any;
  rating: any;
  stock: any;
}

export interface Manufacturer {
  code?: string;
  // description?: Description;
  id?: number;
  order?: number;
}

// export interface Description {
//   id?: number;
//   language?: string;
//   name?: string;
//   description?: string;
//   friendlyUrl?: string;
//   highlights?: string;
//   keyWords?: string
//   metaDescription?: string;
//   title?: string;
// }

// export interface Images {
//   defaultImage?: boolean;
//   externalUrl?: string;
//   id?: number
//   imageName?: string;
//   imageType?: number;
//   imageUrl?: string;
//   videoUrl?: string;
// }


// Color Filter
export interface ColorFilter {
  color?: ProductColor;
}

export interface SizeFilter {
  size?: ProductSize;
}

// // Tag Filter
// export interface TagFilter {
//   tag?: ProductTags
// }

// export interface Options {
//   id?: number;
//   code?: string;
//   type?: string;
//   name?: string;
//   lang?: string;
//   optionValues?: OptionValues[];
// }

// export interface OptionValues {
//   id?: number;
//   code?: string;
//   defaultValue?: boolean;
//   sortOrder?: number;
//   description?: string;
//   name?: string;
//   lang?: string;
//   price?: number;
//   image?: string;
// }