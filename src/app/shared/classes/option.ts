import { OptionValue } from "./option-value";

export class Option {
    id?: number;
    code?: string;
    type?: string;
    name?: string;
    lang?: string;
    optionValues?: OptionValue[];
}
