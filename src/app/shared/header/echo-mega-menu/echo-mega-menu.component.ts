import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { CommonCoreService } from 'src/app/core/services/common-core.service';
import { CategoryService } from 'src/app/core/services/category.service';

@Component({
  selector: 'echo-mega-menu',
  templateUrl: './echo-mega-menu.component.html',
  styleUrls: ['./echo-mega-menu.component.scss']
})
export class EchoMegaMenuComponent implements OnInit {

  constructor(
    private categoryService: CategoryService,
  ) { }

  MENUITEMS: any[] = [
    {
      title: 'home', type: 'link', path: "/home"
    },
    {
      title: 'category', type: 'sub', megaMenu: true, megaMenuType: 'large', children: [
      ]
    },
    {
      title: 'products', type: 'link', path: "/home/collection/all"
    },

    {
      title: 'Gallery', type: 'link', path: "/home/portfolio"
    },
    {
      title: 'services', type: 'sub', children: [
        { path: '/pages/about-us', title: 'about-us', type: 'link' },
        { path: '/pages/search', title: 'search', type: 'link' },
        { path: '/pages/collection', title: 'collection', type: 'link' },
        { path: '/pages/contact', title: 'contact', type: 'link' },
        { path: '/pages/faq', title: 'FAQ', type: 'link' },
      ]
    },
    { path: '/home/get_the_app', title: 'get_the_app', type: 'link' }
  ]

  items: MenuItem[];
  categoryList: any[] = [];

  ngOnInit() {

    this.categoryService.getCategoryHierarchy().subscribe(res => {
      this.categoryList = res
      console.log("getCategoryHierarchy  :", res);
      this.createCat(res);
      console.log("after getCategoryHierarchy  :", this.categories);
    })

    this.items = [
      {
        label: 'TV', icon: 'fa fa-fw fa-check',
        items: [
          [
            {
              label: 'TV 1',
              items: [{ label: 'TV 1.1' }, { label: 'TV 1.2' }]
            },
            {
              label: 'TV 2',
              items: [{ label: 'TV 2.1' }, { label: 'TV 2.2' }]
            }
          ],
          [
            {
              label: 'TV 3',
              items: [{ label: 'TV 3.1' }, { label: 'TV 3.2' }]
            },
            {
              label: 'TV 4',
              items: [{ label: 'TV 4.1' }, { label: 'TV 4.2' }]
            }
          ]
        ]
      },
      {
        label: 'Sports', icon: 'fa fa-fw fa-soccer-ball-o',
        items: [
          [
            {
              label: 'Sports 1',
              items: [{ label: 'Sports 1.1' }, { label: 'Sports 1.2' }]
            },
            {
              label: 'Sports 2',
              items: [{ label: 'Sports 2.1' }, { label: 'Sports 2.2' }]
            },

          ],
          [
            {
              label: 'Sports 3',
              items: [{ label: 'Sports 3.1' }, { label: 'Sports 3.2' }]
            },
            {
              label: 'Sports 4',
              items: [{ label: 'Sports 4.1' }, { label: 'Sports 4.2' }]
            }
          ],
          [
            {
              label: 'Sports 5',
              items: [{ label: 'Sports 5.1' }, { label: 'Sports 5.2' }]
            },
            {
              label: 'Sports 6',
              items: [{ label: 'Sports 6.1' }, { label: 'Sports 6.2' }]
            }
          ]
        ]
      },
      {
        label: 'Entertainment', icon: 'fa fa-fw fa-child',
        items: [
          [
            {
              label: 'Entertainment 1',
              items: [{ label: 'Entertainment 1.1' }, { label: 'Entertainment 1.2' }]
            },
            {
              label: 'Entertainment 2',
              items: [{ label: 'Entertainment 2.1' }, { label: 'Entertainment 2.2' }]
            }
          ],
          [
            {
              label: 'Entertainment 3',
              items: [{ label: 'Entertainment 3.1' }, { label: 'Entertainment 3.2' }]
            },
            {
              label: 'Entertainment 4',
              items: [{ label: 'Entertainment 4.1' }, { label: 'Entertainment 4.2' }]
            }
          ]
        ]
      },
      {
        label: 'Technology', icon: 'fa fa-fw fa-gears',
        items: [
          [
            {
              label: 'Technology 1',
              items: [{ label: 'Technology 1.1' }, { label: 'Technology 1.2' }]
            },
            {
              label: 'Technology 2',
              items: [{ label: 'Technology 2.1' }, { label: 'Technology 2.2' }]
            },
            {
              label: 'Technology 3',
              items: [{ label: 'Technology 3.1' }, { label: 'Technology 3.2' }]
            }
          ],
          [
            {
              label: 'Technology 4',
              items: [{ label: 'Technology 4.1' }, { label: 'Technology 4.2' }]
            }
          ]
        ]
      }
    ];


    // this.items = [
    //   {
    //     label: 'services', icon: 'fa fa-fw fa-check', items: [[
    //       { routerLink: '/pages/about-us', label: 'about-us', icon: 'fa fa-fw fa-check' },
    //       { routerLink: '/pages/search', label: 'search', icon: 'fa fa-fw fa-check' },
    //       { routerLink: '/pages/collection', label: 'collection', icon: 'fa fa-fw fa-check' },
    //       { routerLink: '/pages/contact', label: 'contact', icon: 'fa fa-fw fa-check' },
    //       { url: '/pages/faq', label: 'FAQ', icon: 'fa fa-fw fa-check' },
    //     ]]
    //   }
    // ];
  }

  categories: any[] = [];
  createCat(arr: any[]) {
    arr.forEach(cat => {
      const _cat = { title: cat.descriptions[0].name, type: 'link', children: [] }
      if (cat.children.length > 0) {
        cat.children.forEach(sub => {
          const ele = { path: '/home/collection/' + sub.code, title: sub.descriptions[0].name, type: 'link' }
          _cat.children.push(ele)
        })
      }
      this.categories.push(_cat);
    })
  }

}
