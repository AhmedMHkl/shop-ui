import { Component, Inject, HostListener, OnInit, ViewEncapsulation } from '@angular/core';
import { LandingFixService } from '../../services/landing-fix.service';
import { DOCUMENT } from "@angular/common";
import { WINDOW } from '../../services/windows.service';
import { CartItem } from '../../classes/cart-item';
import { CartService } from '../../services/cart.service';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { ProductsService } from '../../services/products.service';
import { AuthService } from 'src/app/auth/auth.service';
declare var $: any;

@Component({
  selector: 'app-header-two',
  templateUrl: './header-two.component.html',
  styleUrls: ['./header-two.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderTwoComponent implements OnInit {

  public shoppingCartItems: CartItem[] = [];
  showSearch: boolean = false;
  searchTerm: string = '';

  isLoggedIn: boolean = false;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    @Inject(WINDOW) private window,
    private fix: LandingFixService,
    private cartService: CartService,
    private router: Router,
    public productsService: ProductsService,
    private authService: AuthService,
  ) {
    this.cartService.getItems().subscribe(shoppingCartItems => this.shoppingCartItems = shoppingCartItems);
  }

  ngOnInit() {
    $(document).on('click', '.mega-dropdown', function(e) {
      e.stopPropagation()
    })

    $('.toggle-nav').on('click', function() {
      $('.sm-horizontal').css("display", "none");
    });
    $(".mobile-back").on('click', function() {
      $('.sm-horizontal').css("right", "-410px");
    });
    

    $.getScript('assets/js/menu.js');
    this.authService.isLoggedIn.subscribe(res => {
      if (res == true) this.isLoggedIn = true
    })
  }

  openNav() {
    this.fix.addNavFix();
  }

  closeNav() {
    this.fix.removeNavFix();
  }
  openSearch($event) {
    console.log($event);
    this.showSearch = $event;
  }
  closeSearch() {
    this.showSearch = false;
  }

  gotoDetails(keyword: string) {
    this.router.navigate(['/home/search?keyword=' + keyword]);
  }

  // @HostListener Decorator
  @HostListener("window:scroll", [])
  onWindowScroll() {
    let number = this.window.pageYOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
    if (number >= 300) {
      this.document.getElementById("sticky").classList.add('fixed');
    } else {
      this.document.getElementById("sticky").classList.remove('fixed');
    }
  }

}