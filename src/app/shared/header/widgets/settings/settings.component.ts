import { EchoProductsService } from './../../../../services/echo-products.service';
import { AppConstants } from 'src/app/core/utils/AppConstants';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CartItem } from '../../../../shared/classes/cart-item';
import { CartService } from '../../../../shared/services/cart.service';
import { ProductsService } from '../../../../shared/services/products.service';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-header-widgets',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  @Input() shoppingCartItems: CartItem[] = [];
  @Output() showSearchEvent: EventEmitter<Boolean> = new EventEmitter<Boolean>();

  public show: boolean = false;
  searchShowed: boolean = false;
  searchTerm: string = ''
  constructor(private translate: TranslateService,
    private cartService: CartService,
    public productsService: ProductsService,
    private router: Router,
    private echoProductService: EchoProductsService) { }

  ngOnInit() { }

  public updateCurrency(curr) {
    this.productsService.currency = curr;
  }

  public changeLanguage(lang) {
    //this.translate.use(lang)
    localStorage.setItem(AppConstants.CURRENT_LANGUAGE_KEY, lang);
    /*
    const dom: any = document.querySelector('body');

    var elem = document.querySelector("html");
    if (lang == "ar") {
      elem.removeAttribute("lang");
      elem.setAttribute("lang", "rtl");
    } else {
      elem.removeAttribute("lang");
      elem.setAttribute("lang", "ltr")
    }
    const rtl = dom.classList.contains('rtl');
    if (rtl && lang != "ar") {
      this.rltAndLtr();
    }
    if (rtl == false && lang == "ar") {
      this.rltAndLtr();
    }
*/
    location.reload();

  }

  rltAndLtr() {
    const dom: any = document.querySelector('body');
    dom.classList.toggle('rtl');
  }

  public openSearch() {
    this.show = true;
    if (!this.searchShowed) {
      this.searchShowed = true;
      this.showSearchEvent.emit(true);
    } else {
      this.searchShowed = false;
      this.showSearchEvent.emit(false);
    }

  }

  public closeSearch() {
    this.show = false;
  }

  public getTotal(): Observable<number> {
    return this.cartService.getTotalAmount();
  }

  public removeItem(item: CartItem) {
    this.cartService.removeFromCart(item);

    // remove from backend : just set cartItem->quantity = 0  --- working with customers and anonymous
    const cartCode = localStorage.getItem("cartCode");
    const cartItem = {
      product: item.product.id,
      quantity: 0
    };
    this.echoProductService.modifyCart(cartCode, cartItem).subscribe(res => {
      console.log(res)
    });
  }

  doSearch() {
    if (this.searchTerm != '')
      this.router.navigate(['/home/search'], { queryParams: { keyword: this.searchTerm } });
    this.searchTerm = ''
  }

}
