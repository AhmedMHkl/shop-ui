import { Component, OnInit } from '@angular/core';
import { MENUITEMS, Menu } from './left-menu-items';
import { CommonCoreService } from 'src/app/core/services/common-core.service';
import { CategoryService } from 'src/app/core/services/category.service';
declare var $: any;

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.scss']
})
export class LeftMenuComponent implements OnInit {

  public menuItems: Menu[];
  categories: Menu[] = [];

  constructor(
    private categoryService: CategoryService
  ) { }

  ngOnInit() {
    this.menuItems = MENUITEMS.filter(menuItem => menuItem);

    this.categoryService.getAllCategories().subscribe(
      (res) => {
        console.log("all cats - from left menu : ", res);
        this.createCat(res);
        this.menuItems = this.categories
        console.log("all categories : ", this.menuItems);

      })

  }

  createCat(arr: any[]) {
    arr.forEach(cat => {

      const _cat: Menu = { title: cat.descriptions[0].name }
      if (cat.children.length > 0) {
        _cat.type = 'sub'
        cat.children.forEach(sub => {
          const subCat: Menu = { path: '/home/collection/' + sub.code, title: sub.descriptions[0].name, type: 'link' }
          _cat.children = []
          _cat.children.push(subCat)
        })
      }
      else {
        _cat.type = 'link'
        _cat.path = '/home/collection/' + cat.code
      }
      this.categories.push(_cat);

    })
  }

}
