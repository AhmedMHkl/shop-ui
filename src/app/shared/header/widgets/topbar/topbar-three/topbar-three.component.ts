import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/auth/auth.service';
import { AppConstants } from 'src/app/core/utils/AppConstants';
import { ProductsService } from '../../../../../shared/services/products.service';
declare var $: any;

@Component({
  selector: 'app-topbar-three',
  templateUrl: './topbar-three.component.html',
  styleUrls: ['./topbar-three.component.scss']
})
export class TopbarThreeComponent implements OnInit {

  isLoggedIn: boolean = false;
  public selectedLanguage: string = "";
  public countryFlag: string = "";

  constructor(
    public productsService: ProductsService,
    private authService: AuthService,
    private router: Router,
    private translate: TranslateService,
    
  ) {
    
  }

  ngOnInit() {


    this.authService.isLoggedIn.subscribe(res => {
      if (res == true) this.isLoggedIn = true
    })


    let logged = localStorage.getItem(AppConstants.IS_LOGGED_IN_KEY);
    if (logged && logged == "yes") {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }

    this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS', 'ar']);
    let browserLang = localStorage.getItem(AppConstants.CURRENT_LANGUAGE_KEY);
    if (browserLang) {
      this.translate.setDefaultLang(browserLang);
    } else {
      this.translate.setDefaultLang('en');
      browserLang = this.translate.getBrowserLang();
    }

    this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS|ar/) ? browserLang : 'en');
    if (browserLang === "en") {
      this.selectedLanguage = "English";
      this.countryFlag = "flag-icon flag-icon-gb";
    } else {
      this.selectedLanguage = "Arabic";
      this.countryFlag = "flag-icon flag-icon-eg";
    }
    const dom: any = document.querySelector('body');
    const rtl = dom.classList.contains('rtl');
    if (rtl && browserLang != "ar") {
      this.rltAndLtr();
    }
    if (rtl == false && browserLang == "ar") {
      this.rltAndLtr();
    }



  }

  logout() {
    this.authService.logout()
  }



  public changeLanguage(lang) {
    //this.translate.use(lang)
    localStorage.setItem(AppConstants.CURRENT_LANGUAGE_KEY, lang);
    location.reload();

  }

  rltAndLtr() {
    const dom: any = document.querySelector('body');
    dom.classList.toggle('rtl');
  }

  changeLang(language: string) {
    this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS', 'ar']);
    this.translate.setDefaultLang(language);
    this.translate.use(language);
    if (language == "en") {
      this.selectedLanguage = "English";
      this.countryFlag = "flag-icon flag-icon-gb";
    } else {
      this.selectedLanguage = "Arabic";
      this.countryFlag = "flag-icon flag-icon-eg";
    }
    localStorage.setItem(AppConstants.CURRENT_LANGUAGE_KEY, language);
    const dom: any = document.querySelector('body');
    const rtl = dom.classList.contains('rtl');
    if (rtl && language != "ar") {
      this.rltAndLtr();
    }
    if (rtl == false && language == "ar") {
      this.rltAndLtr();
    }
    location.reload();
  }
}
