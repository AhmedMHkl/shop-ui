import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { AppConstants } from 'src/app/core/utils/AppConstants';
import { ProductsService } from '../../../../../shared/services/products.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar-one.component.html',
  styleUrls: ['./topbar-one.component.scss']
})
export class TopbarOneComponent implements OnInit {

  isLoggedIn: boolean = false;
  constructor(
    public productsService: ProductsService,
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit() {

    this.authService.isLoggedIn.subscribe(res => {
      if (res == true) this.isLoggedIn = true
    })

    
    let logged = localStorage.getItem(AppConstants.IS_LOGGED_IN_KEY);
    if (logged && logged == "yes") {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }
    


  }

  logout() {
    this.authService.logout()
  }

}
