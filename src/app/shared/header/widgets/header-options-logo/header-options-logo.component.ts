import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { CategoryService } from 'src/app/core/services/category.service';
import { AppConstants } from 'src/app/core/utils/AppConstants';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { CartItem } from 'src/app/shared/classes/cart-item';
import { Product } from 'src/app/shared/classes/product';
import { CartService } from 'src/app/shared/services/cart.service';
import { ProductsService } from 'src/app/shared/services/products.service';
import { QuotationService } from 'src/app/shared/services/quotation.service';
import { WishlistService } from 'src/app/shared/services/wishlist.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-header-options-logo',
  templateUrl: './header-options-logo.component.html',
  styleUrls: ['./header-options-logo.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderOptionsLogoComponent implements OnInit {

  isLoggedIn: boolean = false;
  @Input() shoppingCartItems: CartItem[] = [];
  quotationItems: any[] = [];
  compareItems: any[] = [];
  wishlistCartItems: any[] = [];
  categoryList: any[] = [];
  categories: any[] = [];
  selectedCategories: any[] = [];
  selectedCategory: any;
  getProductBYCategory: boolean = false;
  public searchProducts: Product[] = [];

  public selectedLanguage: string = "";
  public countryFlag: string = "";
  public productName: string;
  categoryName: string;
  host = ''

  constructor(
    public productsService: ProductsService,
    private authService: AuthService,
    private translate: TranslateService,
    private cartService: CartService,
    private router: Router,
    private wishlistService: WishlistService,
    private echoProductService: EchoProductsService,
    private categoryService: CategoryService,
    private quotationService: QuotationService
  ) {
    this.wishlistService.getProducts().subscribe(wishlistCartItems => this.wishlistCartItems = wishlistCartItems);
    this.productsService.getComapreProducts().subscribe(compareItems => this.compareItems = compareItems);
  }

  ngOnInit() {
    // this.compareItems = JSON.parse(localStorage.getItem("compareItem")) || [];
    this.host = environment.apiUrl
    // console.log("shoppingCartItems", this.shoppingCartItems);
    // console.log("wishlistCartItems", this.wishlistCartItems);
    this.quotationService.getProducts().subscribe(quotationItems => this.quotationItems = quotationItems);
    // console.log("quotationItems>>>>>>>>>>", this.quotationItems);

    this.authService.isLoggedIn.subscribe(res => {
      if (res == true) this.isLoggedIn = true
    })

    this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS', 'ar']);
    let browserLang = localStorage.getItem(AppConstants.CURRENT_LANGUAGE_KEY);
    if (browserLang) {
      this.translate.setDefaultLang(browserLang);
    } else {
      this.translate.setDefaultLang('en');
      browserLang = this.translate.getBrowserLang();
    }

    this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS|ar/) ? browserLang : 'en');
    if (browserLang === "en") {
      this.selectedLanguage = "English";
      this.countryFlag = "flag-icon flag-icon-gb";
    } else {
      this.selectedLanguage = "Arabic";
      this.countryFlag = "flag-icon flag-icon-eg";
    }
    const dom: any = document.querySelector('body');
    const rtl = dom.classList.contains('rtl');
    if (rtl && browserLang != "ar") {
      this.rltAndLtr();
    }
    if (rtl == false && browserLang == "ar") {
      this.rltAndLtr();
    }

    this.categories.push({ label: this.translate.instant('categoriesSearch'), value: '' });
    this.categoryService.getAllCategories().subscribe(
      (res) => {
        // console.log("all cats - from left menu : ", res);
        this.categoryList = res
        // console.log("all categories : ", this.categoryList);

        for (let c of this.categoryList) {
          this.categories.push({ label: c.description.name, value: c });
        }

      })
    // this.categoryList = this.categoryService.cachedCategories()
    // console.log("categoryListcategoryList?>?>", this.categoryList);

  }

  public updateCurrency(curr) {
    this.productsService.currency = curr;
  }

  public changeLanguage(lang) {
    //this.translate.use(lang)
    localStorage.setItem(AppConstants.CURRENT_LANGUAGE_KEY, lang);
    location.reload();

  }

  onSaveUsernameChanged() {
    console.log(this.selectedCategories);

  }

  rltAndLtr() {
    const dom: any = document.querySelector('body');
    dom.classList.toggle('rtl');
  }
  public getTotal(): Observable<number> {
    return this.cartService.getTotalAmount();
  }

  public removeItem(item: CartItem) {
    this.cartService.removeFromCart(item);

    // remove from backend : just set cartItem->quantity = 0  --- working with customers and anonymous
    const cartCode = localStorage.getItem("cartCode");
    const cartItem = {
      product: item.product.id,
      quantity: 0
    };
    this.echoProductService.modifyCart(cartCode, cartItem).subscribe(res => {
      console.log(res)
    });
  }




  changeLang(language: string) {
    this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS', 'ar']);
    this.translate.setDefaultLang(language);
    this.translate.use(language);
    if (language == "en") {
      this.selectedLanguage = "English";
      this.countryFlag = "flag-icon flag-icon-gb";
    } else {
      this.selectedLanguage = "Arabic";
      this.countryFlag = "flag-icon flag-icon-eg";
    }
    localStorage.setItem(AppConstants.CURRENT_LANGUAGE_KEY, language);
    const dom: any = document.querySelector('body');
    const rtl = dom.classList.contains('rtl');
    if (rtl && language != "ar") {
      this.rltAndLtr();
    }
    if (rtl == false && language == "ar") {
      this.rltAndLtr();
    }

  }

  searchByCategory() {
    console.log("selectedCategory", this.selectedCategory);
    this.categoryName = this.selectedCategory.description.name;
    this.router.navigate(['/home/search'], { queryParams: { categoryName: this.categoryName } });
  }



  search(value) {
    this.router.navigate(['/home/search'], { queryParams: { productName: value } });
  }
}
