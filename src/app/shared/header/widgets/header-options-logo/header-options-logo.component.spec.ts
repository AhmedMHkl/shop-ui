import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderOptionsLogoComponent } from './header-options-logo.component';

describe('HeaderOptionsLogoComponent', () => {
  let component: HeaderOptionsLogoComponent;
  let fixture: ComponentFixture<HeaderOptionsLogoComponent>;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderOptionsLogoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderOptionsLogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
