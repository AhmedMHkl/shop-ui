// Menu
export interface Menu {
	path?: string;
	title?: string;
	type?: string;
	megaMenu?: boolean;
	megaMenuType?: string; // small, medium, large
	image?: string;
	children?: Menu[];
}

export const MENUITEMS: Menu[] = [
	{
		title: 'home', type: 'link', path: "/home"
	},
	{
		title: 'category', type: 'sub', megaMenu: true, megaMenuType: 'large', children: [
			{
				title: 'mens-fashion', type: 'link', children: [
					{ path: '/home/collection/all', title: 'sports-wear', type: 'link' },
					{ path: '/home/collection/all', title: 'top', type: 'link' },
					{ path: '/home/collection/all', title: 'bottom', type: 'link' },
					{ path: '/home/collection/all', title: 'ethic-wear', type: 'link' },
					{ path: '/home/collection/all', title: 'sports-wear', type: 'link' },
					{ path: '/home/collection/all', title: 'shirts', type: 'link' }
				]
			}
		]
	},
	{
		title: 'Gallery', type: 'link', path: "/home/portfolio"
	},
	{
		title: 'services', type: 'sub', children: [
			{ path: '/pages/about-us', title: 'about-us', type: 'link' },
			{ path: '/pages/search', title: 'search', type: 'link' },
			{ path: '/pages/collection', title: 'collection', type: 'link' },
			{ path: '/pages/contact', title: 'contact', type: 'link' },
			{ path: '/pages/faq', title: 'FAQ', type: 'link' },
		]
	},
	{ path: '/home/get_the_app', title: 'get_the_app', type: 'link' }
]