import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CategoryService } from 'src/app/core/services/category.service';
import { Menu, MENUITEMS } from './navbar-items';
import { MenuItem } from 'primeng/api';
declare var $: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NavbarComponent implements OnInit {

  public menuItems: any[] = [];
  categoryList: any[] = [];
  categories: any[] = [];

  constructor(
    private categoryService: CategoryService,

  ) { }

  ngOnInit() {

    this.initMenu()

    $('.toggle-nav').on('click', function () {
      $('.sm-horizontal').css("display", "none");
    });
    $(".mobile-back").on('click', function () {
      $('.sm-horizontal').css("right", "-410px");
    });


    // this.menuItems = MENUITEMS.filter(menuItem => menuItem);
    // this.categoryService.getAllCategories().subscribe(
    //   (res) => {
    //     console.log("all cats - from navbar : ", res);
    //     this.createCat(res);
    //     this.menuItems[1].children = this.categories
    //   })
    // this.categoryList = this.categoryService.cachedCategories()


    this.menuItems.push({ title: 'home', type: 'link', path: "/home" })
    this.menuItems.push({ title: 'Gallery', type: 'link', path: "/home/portfolio" })
    this.menuItems.push({
      title: 'services', type: 'sub', children: [
        { path: '/pages/about-us', title: 'about-us', type: 'link' },
        { path: '/pages/search', title: 'search', type: 'link' },
        { path: '/pages/collection', title: 'collection', type: 'link' },
        { path: '/pages/contact', title: 'contact', type: 'link' },
        { path: '/pages/faq', title: 'FAQ', type: 'link' },
      ]
    },
      { path: '/home/get_the_app', title: 'get_the_app', type: 'link' }
    )


    this.categoryService.getCategoryHierarchy().subscribe(
      (res) => {
        console.log("getCategoryHierarchy navbar : ", res);
        // this.categoryList = res;
        for (let index = 0; index < res.length; index++) {
          let arr: any[] = []
          const element = res[index];
          let childs: any[] = [];
          let item = {}
          if (element.children.length == 0) {
            item = { label: element.description.name, routerLink: ['/home/collection/' + element.id] }
          } else {
            item = { label: element.description.name, routerLink: ['/home/collection/' + element.id] }
            element.children.forEach((chleve1) => {  // start level 1
              let firstChild = null;
              if (chleve1.children.length > 0) {
                let subchildslevel2: any[] = []
                chleve1.children.forEach(subch => {
                  let item = { label: subch.description.name, routerLink: ['/home/collection/' + subch.id] }
                  let chleve1item = { label: chleve1.description.name, routerLink: ['/home/collection/' + chleve1.id] }
                  subchildslevel2.push(chleve1item)
                  subchildslevel2.push(item)

                })
                firstChild = {
                  label: chleve1.description.name,
                  routerLink: ['/home/collection/' + chleve1.id],
                  items: subchildslevel2
                }
                childs.push(firstChild)
              } else {
                firstChild = {
                  label: chleve1.description.name,
                  routerLink: ['/home/collection/' + chleve1.id]
                }
                childs.push(firstChild)
              }

            })
          }
          // chunk subchilds
          let resultArray: any[] = [];
          let i, j, temparray, chunk = 2;
          for (i = 0, j = childs.length; i < j; i += chunk) {
            temparray = childs.slice(i, i + chunk);
            resultArray.push(temparray)
          }
          item['items'] = resultArray
          this.items.push(item)
        }
        console.log("Items navbar : ", this.items);

      })

  }
  items: MenuItem[] = [];

  initMenu() {

    // this.items = [
    //   {
    //     label: 'TV', icon: 'fa fa-fw fa-check',
    //     items: [
    //       [
    //         {
    //           label: 'TV 1',
    //           items: [{ label: 'TV 1.1' }, { label: 'TV 1.2' }]
    //         },
    //         {
    //           label: 'TV 2',
    //           items: [{ label: 'TV 2.1' }, { label: 'TV 2.2' }]
    //         }
    //       ],
    //       [
    //         {
    //           label: 'TV 3',
    //           items: [{ label: 'TV 3.1' }, { label: 'TV 3.2' }]
    //         },
    //         {
    //           label: 'TV 4',
    //           items: [{ label: 'TV 4.1' }, { label: 'TV 4.2' }]
    //         }
    //       ]
    //     ]
    //   },
    //   {
    //     label: 'Sports', icon: 'fa fa-fw fa-soccer-ball-o',
    //     items: [
    //       [
    //         {
    //           label: 'Sports 1',
    //           items: [{ label: 'Sports 1.1' }, { label: 'Sports 1.2' }, { label: 'Sports 1.3' }, { label: 'Sports 1.4' }]
    //         },
    //         {
    //           label: 'Sports 2',
    //           items: [{ label: 'Sports 2.1' }, { label: 'Sports 2.2' }]
    //         },

    //       ],
    //       [
    //         {
    //           label: 'Sports 3',
    //           items: [{ label: 'Sports 3.1' }, { label: 'Sports 3.2' }]
    //         },
    //         {
    //           label: 'Sports 4',
    //           items: [{ label: 'Sports 4.1' }, { label: 'Sports 4.2' }]
    //         }
    //       ],
    //       [
    //         {
    //           label: 'Sports 5',
    //           items: [{ label: 'Sports 5.1' }, { label: 'Sports 5.2' }]
    //         },
    //         {
    //           label: 'Sports 6',
    //           items: [{ label: 'Sports 6.1' }, { label: 'Sports 6.2' }]
    //         }
    //       ]
    //     ]
    //   },
    //   {
    //     label: 'Entertainment', icon: 'fa fa-fw fa-child',
    //     items: [
    //       [
    //         {
    //           label: 'Entertainment 1',
    //           items: [{ label: 'Entertainment 1.1' }, { label: 'Entertainment 1.2' }]
    //         },
    //         {
    //           label: 'Entertainment 2',
    //           items: [{ label: 'Entertainment 2.1' }, { label: 'Entertainment 2.2' }]
    //         }
    //       ],
    //       [
    //         {
    //           label: 'Entertainment 3',
    //           items: [{ label: 'Entertainment 3.1' }, { label: 'Entertainment 3.2' }]
    //         },
    //         {
    //           label: 'Entertainment 4',
    //           items: [{ label: 'Entertainment 4.1' }, { label: 'Entertainment 4.2' }]
    //         }
    //       ]
    //     ]
    //   },
    //   {
    //     label: 'Technology', icon: 'fa fa-fw fa-gears',
    //     items: [
    //       [
    //         {
    //           label: 'Technology 1',
    //           items: [{ label: 'Technology 1.1' }, { label: 'Technology 1.2' }]
    //         },
    //         {
    //           label: 'Technology 2',
    //           items: [{ label: 'Technology 2.1' }, { label: 'Technology 2.2' }]
    //         },
    //         {
    //           label: 'Technology 3',
    //           items: [{ label: 'Technology 3.1' }, { label: 'Technology 3.2' }]
    //         }
    //       ],
    //       [
    //         {
    //           label: 'Technology 4',
    //           items: [{ label: 'Technology 4.1' }, { label: 'Technology 4.2' }]
    //         }
    //       ]
    //     ]
    //   }
    // ];
  }


  createCat(arr: any[]) {
    arr.forEach(cat => {
      const _cat = { title: cat.descriptions[0].name, type: 'link', children: [] }
      if (cat.children.length > 0) {
        cat.children.forEach(sub => {
          const ele = { path: '/home/collection/' + sub.code, title: sub.descriptions[0].name, type: 'link' }
          _cat.children.push(ele)
        })
      }
      this.categories.push(_cat);
    })
  }


}
