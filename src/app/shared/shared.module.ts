import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { RouterModule } from "@angular/router";
import { TranslateModule } from '@ngx-translate/core';
// Services
import { WINDOW_PROVIDERS } from "./services/windows.service";
import { LandingFixService } from '../shared/services/landing-fix.service';
import { InstagramService } from "./services/instagram.service";
import { ProductsService } from "./services/products.service";
import { WishlistService } from "./services/wishlist.service";
import { CartService } from "./services/cart.service";
import { OrderService } from "./services/order.service";
import { PaginationService } from "./classes/paginate";
// Pipes
import { OrderByPipe } from './pipes/order-by.pipe';
// directives
// components
import { HeaderOneComponent } from './header/header-one/header-one.component';
import { HeaderTwoComponent } from './header/header-two/header-two.component';
import { LeftSidebarComponent } from './header/left-sidebar/left-sidebar.component';
import { TopbarOneComponent } from './header/widgets/topbar/topbar-one/topbar-one.component';
import { TopbarTwoComponent } from './header/widgets/topbar/topbar-two/topbar-two.component';
import { TopbarThreeComponent } from './header/widgets/topbar/topbar-three/topbar-three.component';
import { NavbarComponent } from './header/widgets/navbar/navbar.component';
import { SettingsComponent } from './header/widgets/settings/settings.component';
import { LeftMenuComponent } from './header/widgets/left-menu/left-menu.component';
import { FooterOneComponent } from './footer/footer-one/footer-one.component';
import { InformationComponent } from './footer/widgets/information/information.component';
import { CategoriesComponent } from './footer/widgets/categories/categories.component';
import { WhyWeChooseComponent } from './footer/widgets/why-we-choose/why-we-choose.component';
import { CopyrightComponent } from './footer/widgets/copyright/copyright.component';
import { SocialComponent } from './footer/widgets/social/social.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DataViewModule } from 'primeng/dataview';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ProgressBarModule } from 'primeng/progressbar';
import { NgxSpinnerModule } from "ngx-spinner";
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { DialogModule } from 'primeng/dialog';
import { DigitOnlyDirective } from './directives/digit-only.directive';
import { LetterOnlyDirective } from './directives/letter-only.directive';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ArabicCharOnlyDirective } from './directives/arabic-char-only.directive';
import { ArabicNumbersDirective } from './directives/arabic-numbers.directive';
import { EnglishCharOnlyDirective } from './directives/english-char-only.directive';
import { EnglishNumberCharDirective } from './directives/english-number-char.directive';
import { LogtitudeLatitudeDirective } from './directives/logtitude-latitude.directive';
import { OnlyDigitDirective } from './directives/only-digit.directive';
import { OnlyNumberDecimalDirective } from './directives/only-number-decimal.directive';
import { DynamicFormFieldComponent } from './components/dynamicForm/components/dynamic-form-field/dynamic-form-field.component';
import { DynamicFormComponent } from './components/dynamicForm/components/dynamic-form/dynamic-form.component';
import { FieldService } from './components/dynamicForm/service/field.service';
import { DialogService } from 'primeng/api';
import { TabViewModule } from 'primeng/tabview';
import { MiniFormDirective } from './directives/mini-form.directive';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderOptionsLogoComponent } from './header/widgets/header-options-logo/header-options-logo.component';
import { CarouselModule } from 'primeng/carousel';
import { MegaMenuModule } from 'primeng/megamenu';
import { EchoMegaMenuComponent } from './header/echo-mega-menu/echo-mega-menu.component';
import { DefaultImage } from './directives/defaultImage.directive';
import { RatingModule } from 'primeng/rating';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import { LightboxModule } from 'primeng/lightbox';
import { SystemInformationComponent } from './footer/widgets/system-information/system-information.component';
import { PaginatorModule } from 'primeng/paginator';
import { AutoCompleteModule } from 'primeng/autocomplete';

@NgModule({
  exports: [
    CommonModule,
    TranslateModule,
    HeaderOneComponent,
    HeaderTwoComponent,
    LeftSidebarComponent,
    FooterOneComponent,
    OrderByPipe,
    ReactiveFormsModule,
    FormsModule,
    DataViewModule,
    LightboxModule,
    PaginatorModule,
    ProgressSpinnerModule,
    ProgressBarModule,
    NgxSpinnerModule,
    ConfirmDialogModule,
    DialogModule,
    DigitOnlyDirective,
    LetterOnlyDirective,
    DefaultImage,
    RatingModule,
    AutoCompleteModule,
    OverlayPanelModule,
    ArabicCharOnlyDirective,
    ArabicNumbersDirective,
    EnglishCharOnlyDirective,
    EnglishNumberCharDirective,
    LogtitudeLatitudeDirective,
    OnlyDigitDirective,
    OnlyNumberDecimalDirective,
    // dform
    DynamicFormFieldComponent,
    DynamicFormComponent,
    TabViewModule,
    MiniFormDirective,
    CarouselModule,
    MegaMenuModule,
    EchoMegaMenuComponent,


  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    ReactiveFormsModule,
    FormsModule,
    DataViewModule,
    AutoCompleteModule,
    LightboxModule,
    ProgressSpinnerModule,
    ProgressBarModule,
    NgxSpinnerModule,
    PaginatorModule,
    ConfirmDialogModule,
    DialogModule,
    RatingModule,
    OverlayPanelModule,
    TabViewModule,
    CarouselModule,
    MegaMenuModule,
    NgbModule,
    CheckboxModule,
    DropdownModule
  ],
  declarations: [
    HeaderOneComponent,
    HeaderTwoComponent,
    LeftSidebarComponent,
    FooterOneComponent,
    OrderByPipe,
    NavbarComponent,
    SettingsComponent,
    LeftMenuComponent,
    TopbarOneComponent,
    TopbarTwoComponent,
    TopbarThreeComponent,
    InformationComponent,
    CategoriesComponent,
    WhyWeChooseComponent,
    CopyrightComponent,
    SocialComponent,
    DigitOnlyDirective,
    LetterOnlyDirective,
    DefaultImage,
    ArabicCharOnlyDirective,
    ArabicNumbersDirective,
    EnglishCharOnlyDirective,
    EnglishNumberCharDirective,
    LogtitudeLatitudeDirective,
    OnlyDigitDirective,
    OnlyNumberDecimalDirective,
    // dform
    DynamicFormFieldComponent,
    DynamicFormComponent,
    MiniFormDirective,
    HeaderOptionsLogoComponent,
    EchoMegaMenuComponent,
    SystemInformationComponent,
  ],
  providers: [
    WINDOW_PROVIDERS,
    LandingFixService,
    InstagramService,
    ProductsService,
    WishlistService,
    CartService,
    OrderService,
    PaginationService,
    ConfirmationService,
    FieldService,
    DatePipe,
    DialogService
  ]
})
export class SharedModule { }
