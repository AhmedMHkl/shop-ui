export class BaseField<T> {
    id: string;
    name: string;
    label: string;
    required: boolean;
    field_order: number;
    controlType: string;
    directives: string;
    field_type: string;
    createdAt: number;


    constructor() { }

    /*  constructor(options: {
         value?: T,
         fkey?: string,
         label?: string,
         required?: boolean,
         forder?: number,
         controlType?: string
     } = {}) {
         this.value = options.value;
         this.fkey = options.fkey || '';
         this.label = options.label || '';
         this.required = !!options.required;
         this.forder = options.forder === undefined ? 1 : options.forder;
         this.controlType = options.controlType || '';
     } */
}
