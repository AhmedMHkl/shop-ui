import { BaseField } from "./base-field";
export class TextboxField extends BaseField<string> {
    controlType = 'textbox';
    ftype: string;

    constructor() {
        super();
    }

    /* constructor(options: {} = {}) {
        super(options);
        this.ftype = options['type'] || '';
    } */
}
