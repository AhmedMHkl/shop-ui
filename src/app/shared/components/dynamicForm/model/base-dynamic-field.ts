export class BaseDynamicField {

    doClick(event) {
        console.log("click",event);
    }

    doBlur(event) {
        console.log("blur",event);
    }

    doFocus(event) {
        console.log("focus",event);
    }

    doKeyup($event) {
        console.log("keyup",event);
    }
}
