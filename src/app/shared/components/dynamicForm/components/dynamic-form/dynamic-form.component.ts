import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { FieldService } from '../../service/field.service';
import { BaseField } from '../../model/base-field';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent implements OnInit, AfterViewInit {

  @Input() fields: BaseField<any>[] = [];
  @Input() funcs: any[] = []
  @Input() formData: any = {};
  @Input() submitFunc: any;
  @Output() outData: EventEmitter<any> = new EventEmitter();
  form: FormGroup;


  constructor(private fieldService: FieldService) { }

  ngOnInit() {
    //this.form.patchValue(this.formData)
    this.form = this.fieldService.toFormGroup(this.fields);

    if (this.submitFunc === undefined)
      this.submitFunc = () => this.doNothing()


    console.log(this.form);

  }

  ngAfterViewInit() {



  }

  outputs() {
    this.fieldService.formSubmitted.next(true);
    this.outData.emit(this.form);
  }

  doNothing() { }

}
