import { Component, OnInit, Input, HostBinding, ViewChild, ElementRef, HostListener } from '@angular/core';
import { BaseField } from '../../model/base-field';
import { FormGroup } from '@angular/forms';
import { BaseDynamicField } from '../../model/base-dynamic-field';
import { FieldService } from '../../service/field.service';

@Component({
  selector: 'app-dynamic-form-field',
  templateUrl: './dynamic-form-field.component.html',
  styleUrls: ['./dynamic-form-field.component.scss']
})
export class DynamicFormFieldComponent implements OnInit {

  @Input() field: BaseField<any>;
  @Input() form: FormGroup;

  submitted: boolean = false;

  get isValid() { return this.form.controls[this.field.name].valid; }


  @Input() blurFunction: any;
  @Input() focusFunction: any;

  constructor(private fieldService: FieldService) {

  }

  ngOnInit() {

    this.fieldService.formSubmitted.subscribe(res => this.submitted = res)

    if (this.blurFunction === undefined)
      this.blurFunction = () => this.doNothing()
    if (this.focusFunction === undefined)
      this.focusFunction = () => this.doNothing()
  }

  doNothing() {

  }

  // @HostListener('click', ['$event.target']) onClick(btn) {
  //   console.log("button", btn, "number of clicks:");
  // }

  //@HostBinding('attr.appArabicNumbers') appArabicNumbers = new ArabicNumbersDirective(this.textBox);
  // @HostBinding('attr.appArabicNumbers')
  // get focus() {
  //   console.log(this.field['description']);
  //   return this.field['description'] == 'appArabicNumbers';
  // }

}
