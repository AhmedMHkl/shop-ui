import { Injectable } from '@angular/core';
import { BaseField } from '../model/base-field';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TextboxField } from '../model/textbox-field';
import { DropdownField } from '../model/dropdown-field';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FieldService {

  private url = environment.apiUrl;

  public formSubmitted = new Subject<boolean>();

  constructor(
    private http: HttpClient,

  ) { }

  toFormGroup(fields: BaseField<any>[]) {
    let group: any = {};
    fields.forEach(field => {
      group[field.name] = field.required ? new FormControl('', Validators.required)
        : new FormControl('');
    });
    return new FormGroup(group);
  }

  public getAllFields(): Observable<any> {
    return this.http.get(`${this.url}/api/form/builder/fields`);
  }

  // dummy data
  // getFields() {
  //   let fields: BaseField<any>[] = [
  //     new DropdownField({
  //       key: 'brave',
  //       label: 'Bravery Rating',
  //       options: [
  //         { key: 'solid', value: 'Solid' },
  //         { key: 'great', value: 'Great' },
  //         { key: 'good', value: 'Good' },
  //         { key: 'unproven', value: 'Unproven' }
  //       ],
  //       order: 3
  //     }),
  //     new TextboxField({
  //       key: 'firstName',
  //       label: 'First name',
  //       value: 'Bombasto',
  //       required: true,
  //       order: 1
  //     }),
  //     new TextboxField({
  //       key: 'emailAddress',
  //       label: 'Email',
  //       type: 'email',
  //       order: 2
  //     })
  //   ];
  //   return fields.sort((a, b) => a.forder - b.forder);
  // }

}
