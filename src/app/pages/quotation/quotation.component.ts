import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ConfirmationService } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';
import { QuotationService } from 'src/app/shared/services/quotation.service';
import { QuotationItem } from 'src/app/shared/classes/quotation-item';
import { ToastrService } from 'ngx-toastr';
import { AppConstants } from 'src/app/core/utils/AppConstants';
import { ProductsService } from 'src/app/shared/services/products.service';
import { WishlistService } from 'src/app/shared/services/wishlist.service';
import { CartService } from 'src/app/shared/services/cart.service';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { AuthService } from 'src/app/auth/auth.service';
import { count } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Product } from 'src/app/shared/classes/product';

@Component({
  selector: 'app-quotation',
  templateUrl: './quotation.component.html',
  styleUrls: ['./quotation.component.scss']
})
export class QuotationComponent implements OnInit {
  public Relatedproducts: Product[] = [];
  productList: Product[] = [];
  ids = [];
  count: number = 0;
  quantity: number = 1;

  columns = [
    { header: 'image', field: 'images', type: 'isImage' },
    { header: 'Product Name', field: 'descriptions', type: 'notBtn' },
    { header: 'price', field: 'price', type: 'notBtn' },
    { header: 'Availability', field: 'available', type: 'notBtn' },
    { header: 'count', field: '', type: 'notBtn' },
    { header: 'total', field: '', type: 'notBtn' },
    { header: 'Action', field: '', type: 'isBtn' }
  ];

  columnsWithoutAction = [
    { header: 'image', field: 'images', type: 'isImage' },
    { header: 'Product Name', field: 'descriptions', type: 'notBtn' },
    { header: 'price', field: 'price', type: 'notBtn' },
    { header: 'Availability', field: 'available', type: 'notBtn' },
    { header: 'count', field: '', type: 'notBtn' },
    { header: 'creation_date', field: 'createdDate', type: 'notBtn' }
  ];

  host = environment.apiUrl;
  quotations: QuotationItem[] = [];
  quoteItemHistories: QuotationItem[] = [];
  totalCount: number = 0;
  first: number = 1;
  pageSize: number = AppConstants.PAGE_SIZE;
  product: Product = null;


  constructor(private quotationService: QuotationService,
    private confirmationService: ConfirmationService,
    private translateService: TranslateService,
    private toastrService: ToastrService,
    private wishlistService: WishlistService,
    private cartService: CartService,
    private echoProductsService: EchoProductsService,
    private authService: AuthService,
    private productsService: ProductsService) { }




  ngOnInit() {
    //  this.getQuotationHistoryByCustomerId(this.first-1,this.pageSize)
    this.quotationService.getProducts().subscribe(
      res => {

        this.quotations = res;
        console.log("quotations", this.quotations)

        this.AllProductsRealatedItems();
      }, err => {
        console.error(err);
      }
    );

    // if(this.count == 0){
    //   for (let index = 0; index < this.quotations.length; index++) {
    //     const element = this.quotations[index];
    //     element.count = 1;
    //   }
    // }
  }

  // Remove from Quotation
  public removeQuotation(index: number) {
    this.confirmationService.confirm({
      message: this.translateService.instant('Are you sure that you want to perform this action?'),
      acceptLabel: this.translateService.instant('yes'),
      rejectLabel: this.translateService.instant('no'),
      header: this.translateService.instant('Confirmation'),
      icon: 'fa fa-exclamation-triangle',
      accept: () => {
        this.quotationService.removeFromQuotations(index);
      }
    });
  }

  saveQuotationList() {
    console.error(this.quotations);

    for (let index = 0; index < this.quotations.length; index++) {
      const element = this.quotations[index];
      element.newPrice = 0.0;
      element.product.categories = null;
      if (element.count == null) {
        this.toastrService.warning(this.translateService.instant('must have count For') + element.product['descriptions'][0].title);
        return;
      }
    }

    this.quotationService.saveQuoteList(this.quotations).subscribe(
      res => {
        this.toastrService.success(this.translateService.instant('quotation order has been Saved successfully'));
        localStorage.removeItem("quotationItems");
        this.quotations = [];
        this.quotationService.setProducts([]);
        // this.getQuotationHistoryByCustomerId(this.first - 1, this.pageSize);
      }, err => {
        this.toastrService.error(this.translateService.instant('Error In Save'));
      }
    );
  }

  // getQuotationHistoryByCustomerId(page, pageSize){
  //   this.quotationService.getQuotationByCustomerId().subscribe(
  //     res => {
  //       console.error("hfhfh",res)
  //       this.quoteItemHistories = res.quoteItemHistories;
  //       this.totalCount = res.totalCount;
  //     } , err => {
  //       console.error(err);
  //     }
  //   );
  // }

  getQuotationHistoryByCustomerId(page, pageSize) {
    this.quotationService.getQuotationHistoryByCustomerId(page, pageSize).subscribe(
      res => {
        this.quoteItemHistories = res.quoteItemHistories;
        this.totalCount = res.totalCount;
      }, err => {
        console.error(err);
      }
    );
  }

  loadPage(page: number) {
    this.getQuotationHistoryByCustomerId(page - 1, this.pageSize);
  }


  public addToCart(product: Product, quantity: number = 1) {
    if (quantity > 0)
      this.cartService.addToCart(product, quantity);
    this.wishlistService.removeFromWishlist(product);
    const cartItem = { attributes: [], product: product.id, quantity: quantity };
    const customerId = this.authService.loggedInUserId();
    if (customerId !== null && customerId !== undefined && customerId !== "") {
      // add tocustomer cart
      this.echoProductsService.addLoggedInCustomerProductToCart(customerId, cartItem).subscribe(res => console.log(res));
    }
    else {
      // add to Anonymous cart
      this.echoProductsService.addAnonymousProductToCart(cartItem).subscribe(res => console.log(res));
    }
  }

  public addToWishlist(product: Product) {
    this.wishlistService.addToWishlist(product);
  }


  public productSlideConfig: any = {
    infinite: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 2,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 420,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    ]
  };
  public AllProductsRealatedItems() {
    let ids = this.quotations.map(p => p.product.id)
    console.log("component ids ", ids);
    this.echoProductsService.AllProductsRelatedItems(ids).
      subscribe(res => {
        if (res) {
          this.Relatedproducts = res;
          console.log("RelatedItem", this.Relatedproducts)
        }
      });
  }

  // Increase Product Quantity
  public increment(item: QuotationItem, quantity: number = 1) {
    console.log(item);

    this.quotationService.updateQuotQuantity(item, quantity);
    // if(this.count < 100){
    //   this.count = this.count + 1;
    //   this.quotations[index].count = this.count;
    // }
  }

  // Decrease Product Quantity
  public decrement(item: QuotationItem, quantity: number = -1) {
    this.quotationService.updateQuotQuantity(item, quantity);
    // if(this.count >= 1){
    //   this.count = this.count - 1;
    //   this.quotations[index].count = this.count;
    // }
  }

  // Get Total
  public getTotal(): Observable<number> {
    return this.quotationService.getTotalAmount(this.quantity);
  }

  public addToCompare(product: Product) {
    this.productsService.addToCompare(product);
  }
}
