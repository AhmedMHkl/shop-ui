import { Component, OnInit } from '@angular/core';
import { SystemInfo } from 'src/app/shared/classes/system-info';
import { SystemInfoDetails } from 'src/app/shared/classes/system-info-details';
import { ShippingReturnService } from 'src/app/services/shipping-return.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ifo-details',
  templateUrl: './ifo-details.component.html',
  styleUrls: ['./ifo-details.component.scss']
})
export class IfoDetailsComponent implements OnInit {
  id: number;
  title: string;
  systemInfoList: SystemInfo[] = [];
  systemInfoDetails: SystemInfoDetails[] = [];
  constructor(private shippingReturnService: ShippingReturnService,
    private route: ActivatedRoute) {
    // window.location.reload(); 
  }

  ngOnInit() {
    this.route.queryParams.subscribe(routeParams => {
      this.title=routeParams.title
      this.getSystemInfoDetails(routeParams.id);
    });
  }


  getSystemInfoDetails(systemInfoId: number) {
    this.shippingReturnService.getSystemInfoDetails(systemInfoId).subscribe(res => {
      this.systemInfoDetails = res;
      console.log("systemInfoDetails:", this.systemInfoDetails);

    })
  }
}
