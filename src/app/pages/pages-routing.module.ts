import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { CompareComponent } from './compare/compare.component';
import { ContactComponent } from './contact/contact.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { FaqComponent } from './faq/faq.component';
import { IfoDetailsComponent } from './ifo-details/ifo-details.component';
import { LookbookComponent } from './lookbook/lookbook.component';
import { OrderSuccessComponent } from './order-success/order-success.component';
import { GridThreeColComponent } from './portfolio/grid-three-col/grid-three-col.component';
// Portfolio Page
import { GridTwoColComponent } from './portfolio/grid-two-col/grid-two-col.component';
import { MasonaryFourGridComponent } from './portfolio/masonary-four-grid/masonary-four-grid.component';
import { MasonaryFullwidthComponent } from './portfolio/masonary-fullwidth/masonary-fullwidth.component';
import { MasonaryThreeGridComponent } from './portfolio/masonary-three-grid/masonary-three-grid.component';
import { MasonaryTwoGridComponent } from './portfolio/masonary-two-grid/masonary-two-grid.component';
import { QuotationComponent } from './quotation/quotation.component';
import { SecureShoppingComponent } from './secure-shopping/secure-shopping.component';
import { ShippingReturnComponent } from './shipping-return/shipping-return.component';
import { ShowPriceComponent } from './show-price/show-price.component';
import { TypographyComponent } from './typography/typography.component';
import { WishlistComponent } from './wishlist/wishlist.component';



const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'about-us',
        component: AboutUsComponent
      },
      {
        path: '404',
        component: ErrorPageComponent
      },
      {
        path: 'lookbook',
        component: LookbookComponent
      },
      {
        path: 'wishlist',
        component: WishlistComponent
      },
      {
        path: 'contact',
        component: ContactComponent
      },
      {
        path: 'compare',
        component: CompareComponent
      },
      {
        path: 'order-success',
        component: OrderSuccessComponent
      },
      {
        path: 'typography',
        component: TypographyComponent
      },
      {
        path: 'faq',
        component: FaqComponent
      },
      {
        path: 'grid/two/column',
        component: GridTwoColComponent
      },
      {
        path: 'grid/three/column',
        component: GridThreeColComponent
      },
      {
        path: 'grid/two/masonary',
        component: MasonaryTwoGridComponent
      },
      {
        path: 'grid/three/masonary',
        component: MasonaryThreeGridComponent
      },
      {
        path: 'grid/four/masonary',
        component: MasonaryFourGridComponent
      },
      {
        path: 'fullwidth/masonary',
        component: MasonaryFullwidthComponent
      },
      {
        path: 'quotation',
        component: QuotationComponent
      }, {
        path: 'showPrice',
        component: ShowPriceComponent
      }, {
        path: 'shipping-return',
        component: ShippingReturnComponent
      }, {
        path: 'secure-shopping',
        component: SecureShoppingComponent
      },
      {
        path: 'info-details',
        component: IfoDetailsComponent
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
