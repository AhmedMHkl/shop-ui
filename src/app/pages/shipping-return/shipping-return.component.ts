import { Component, OnInit } from '@angular/core';
import { ShippingReturnService } from 'src/app/services/shipping-return.service';
import { SystemInfo } from 'src/app/shared/classes/system-info';
import { SystemInfoDetails } from 'src/app/shared/classes/system-info-details';

@Component({
  selector: 'app-shipping-return',
  templateUrl: './shipping-return.component.html',
  styleUrls: ['./shipping-return.component.scss']
})
export class ShippingReturnComponent implements OnInit {

  systemInfoList: SystemInfo[] = [];
  systemInfoDetails: SystemInfoDetails[] = [];
  constructor(private shippingReturnService: ShippingReturnService) { }

  ngOnInit() {
    this.getSystemInfo();
    // setTimeout(() => {
    //   for(let i = 0; i > this.systemInfoList.length; i++){
    //     this.getSystemInfoDetails(this.systemInfoList[i].id);
    //   }
    // }, 500);
    
  }

  getSystemInfo(){
    this.shippingReturnService.getSystemInfo().subscribe(res =>{
      this.systemInfoList = res;
      // for(let i = 0; i < this.systemInfoList.length; i++){
      //   this.getSystemInfoDetails(this.systemInfoList[i].id);
      // }
      console.log("systemInfoList:", this.systemInfoList);
      
    })
  }

  getSystemInfoDetails(systemInfoId: number){
    this.shippingReturnService.getSystemInfoDetails(systemInfoId).subscribe(res =>{
      this.systemInfoDetails = res;
      console.log("systemInfoDetails:", this.systemInfoDetails);
      
    })
  }
}
