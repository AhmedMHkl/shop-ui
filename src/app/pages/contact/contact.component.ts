import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/my-account/models/User';
import { CommonService } from 'src/app/shared/services/common.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { EchoMessageService } from 'src/app/core/services/echo-message.service';
import { Router } from '@angular/router';
import { AppConstants } from 'src/app/core/utils/AppConstants';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  entity: User = new User();

  constructor(private commonService: CommonService,
    private alerMessage: EchoMessageService,
    private router: Router,
    private customerService: CustomerService) { }

  ngOnInit() {
    let isLoggedIn = localStorage.getItem(AppConstants.IS_LOGGED_IN_KEY)
    if(isLoggedIn === 'yes')
      this.getCustomerInfo()
  }


  getCustomerInfo() {
    this.customerService.getCustomerInfo().subscribe(
      (res) => {
        if (res)
          this.entity.name = res.firstName + " " + res.lastName
        this.entity.email = res.emailAddress;
        let id: any;
        id = res.id
        this.entity.customer = res.id;
        this.entity.CustomerId = id;
        console.log("dashboard userInfo >> ", res);

      },
      (err) => {
        console.log(err);
      });
  }
  saveEntity() {
    console.log(this.entity, this.entity.CustomerId)
    this.commonService.SendsAnEmailContactUsToStoreOwner(this.entity).subscribe(
      res => {
        this.alerMessage.successMessage("An  Contact Us  Has been sended successfully To Store Owner", "congratulation");
        this.router.navigate(['/']);

      },
      err => {
        console.log(err);
        this.alerMessage.errorMessage(err.message, "Error Occured");
      }

    );

  }
}
