import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { IsotopeModule } from 'ngx-isotope';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { SharedModule } from '../shared/shared.module';
import { ShopModule } from '../shop/shop.module';
import { AboutUsComponent } from './about-us/about-us.component';
import { CompareComponent } from './compare/compare.component';
import { ContactComponent } from './contact/contact.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { FaqComponent } from './faq/faq.component';
import { IfoDetailsComponent } from './ifo-details/ifo-details.component';
import { LookbookComponent } from './lookbook/lookbook.component';
import { OrderSuccessComponent } from './order-success/order-success.component';
import { PagesRoutingModule } from './pages-routing.module';
import { GridThreeColComponent } from './portfolio/grid-three-col/grid-three-col.component';
import { GridTwoColComponent } from './portfolio/grid-two-col/grid-two-col.component';
import { MasonaryFourGridComponent } from './portfolio/masonary-four-grid/masonary-four-grid.component';
import { MasonaryFullwidthComponent } from './portfolio/masonary-fullwidth/masonary-fullwidth.component';
import { MasonaryThreeGridComponent } from './portfolio/masonary-three-grid/masonary-three-grid.component';
import { MasonaryTwoGridComponent } from './portfolio/masonary-two-grid/masonary-two-grid.component';
import { QuotationComponent } from './quotation/quotation.component';
import { SecureShoppingComponent } from './secure-shopping/secure-shopping.component';
import { ShippingReturnComponent } from './shipping-return/shipping-return.component';
import { ShowPriceComponent } from './show-price/show-price.component';
import { TypographyComponent } from './typography/typography.component';
import { WishlistComponent } from './wishlist/wishlist.component';



@NgModule({
  imports: [
    CommonModule,
    PagesRoutingModule,
    SlickCarouselModule,
    IsotopeModule,
    TranslateModule,
    SharedModule,
    NgbPaginationModule,
    ShopModule
  ],
  declarations: [
    AboutUsComponent,
    ErrorPageComponent,
    LookbookComponent,
    WishlistComponent,
    ContactComponent,
    CompareComponent,
    OrderSuccessComponent,
    FaqComponent,
    TypographyComponent,
    GridTwoColComponent,
    GridThreeColComponent,
    MasonaryTwoGridComponent,
    MasonaryThreeGridComponent,
    MasonaryFourGridComponent,
    MasonaryFullwidthComponent,
    QuotationComponent,
    ShippingReturnComponent,
    SecureShoppingComponent,
    ShowPriceComponent,
    IfoDetailsComponent
  ],
  exports: [ShowPriceComponent]
})
export class PagesModule { }
