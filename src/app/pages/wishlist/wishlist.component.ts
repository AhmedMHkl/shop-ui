import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { AuthService } from 'src/app/auth/auth.service';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { Product } from 'src/app/shared/classes/product';
import { CartService } from 'src/app/shared/services/cart.service';
import { ProductsService } from 'src/app/shared/services/products.service';
import { QuotationService } from 'src/app/shared/services/quotation.service';
import { WishlistService } from 'src/app/shared/services/wishlist.service';
import { environment } from 'src/environments/environment';
declare var $: any;

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.scss']
})
export class WishlistComponent implements OnInit {

  wishlist: any[] = [];
  constructor(
    private wishlistService: WishlistService,
    private cartService: CartService,
    private echoProductsService: EchoProductsService,
    private authService: AuthService,
    private translateService: TranslateService,
    private confirmationService: ConfirmationService,
    private quotationService: QuotationService,
    private productsService: ProductsService
  ) {
  }

  host = '';
  currentImg = '';
  defaultImageUrl = "assets/images/fashion/product/1.jpg";
  public Relatedproducts: Product[] = [];

  ngOnInit() {
    this.host = environment.apiUrl;
    this.wishlistService.getProducts().subscribe(res => {
      console.log("wishlist : ", res)
      this.wishlist = res;
      this.AllProductsRealatedItems();
    }
    );
  }

  // Add to cart
  public addToCart(product: Product, quantity: number = 1) {
    if (quantity > 0)
      this.cartService.addToCart(product, quantity);
    // this.wishlistService.removeFromWishlist(product);
    const cartItem = { attributes: [], product: product.id, quantity: quantity };
    const customerId = this.authService.loggedInUserId();
    if (customerId !== null && customerId !== undefined && customerId !== "") {
      // add tocustomer cart
      this.echoProductsService.addLoggedInCustomerProductToCart(customerId, cartItem).subscribe(res => console.log(res));
    }
    else {
      // add to Anonymous cart
      this.echoProductsService.addAnonymousProductToCart(cartItem).subscribe(res => console.log(res));
    }
  }

  // Remove from wishlist
  public removeItem(product: Product) {
    this.confirmationService.confirm({
      message: this.translateService.instant('Are you sure that you want to perform this action?'),
      acceptLabel: this.translateService.instant('yes'),
      rejectLabel: this.translateService.instant('no'),
      accept: () => {
        console.log(product);
        this.wishlistService.removeFromWishlist(product);

      }
    });

  }

  public addToQuotationList(product: Product) {
    this.quotationService.addToQuoteList(product);
  }

  public addToCompare(product: Product) {
    this.productsService.addToCompare(product);
  }

  public productSlideConfig: any = {
    infinite: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 2,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 420,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    ]
  };

  public AllProductsRealatedItems() {
    let ids = this.wishlist.map(p => p.id)
    console.log("component ids ", ids);
    this.echoProductsService.AllProductsRelatedItems(ids).
      subscribe(res => {
        if (res) {
          this.Relatedproducts = res;
          console.log("RelatedItem", this.Relatedproducts)
        }
      });
  }
}
