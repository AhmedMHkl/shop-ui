import { Component, OnInit } from '@angular/core';
import { QuotationService } from 'src/app/shared/services/quotation.service';
import { QuotationItem } from 'src/app/shared/classes/quotation-item';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-show-price',
  templateUrl: './show-price.component.html',
  styleUrls: ['./show-price.component.scss']
})
export class ShowPriceComponent implements OnInit {

  host = '';
  quotations: QuotationItem[] = [];
  quoteItemItems: QuotationItem[] = [];
  defaultImageUrl = "assets/images/fashion/product/1.jpg";

  columnsWithoutAction = [
    { header: 'image', field: 'images', type: 'isImage'},
    { header: 'Product Name', field: 'descriptions', type: 'notBtn'},
    { header: 'price', field: 'price', type: 'notBtn'},
    // { header: 'Availability', field: 'available', type: 'notBtn'},
    { header: 'count', field: '', type: 'notBtn'},
    { header: 'creation_date', field: 'createdDate', type: 'notBtn'}
  ];
  constructor(private quotationService: QuotationService) { }

  ngOnInit() {
    this.host = environment.apiUrl;
    this.getQuotationsByCustomerId();
  }

  getQuotationsByCustomerId(){
    this.quotationService.getQuotationByCustomerId().subscribe(res =>{
      console.log("Resulte :", res);
      this.quotations = res.quotationItems;
      console.log("Quotations List :", this.quotations);
    });
  }

  getQuotationItems(quotationId: number){
    console.log("QUOTATION ID", quotationId);
    this.quotationService.getQuotationItemsByQuotationId(quotationId).subscribe(res =>{
      this.quoteItemItems = res.quotationItems;
      console.log("QUOTATION ITEMS", this.quoteItemItems);
    });
  }
}
