import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecureShoppingComponent } from './secure-shopping.component';

describe('SecureShoppingComponent', () => {
  let component: SecureShoppingComponent;
  let fixture: ComponentFixture<SecureShoppingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecureShoppingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecureShoppingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
