import { Component, OnInit } from '@angular/core';
import { FaqServiceService } from 'src/app/services/faq-service.service';
import { FAQs } from 'src/app/shared/classes/faqs';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  faqList: FAQs;
  constructor(private faqServiceService: FaqServiceService) { }

  ngOnInit() {
    this.getAllFAQs();
  }

  getAllFAQs(){
    this.faqServiceService.getAllFAQs().subscribe(res =>{
      console.log("RESULT ::::",res );
      this.faqList = res.faqs;
      console.log("FAQs ::::",this.faqList );
      
    })
  }

}
