import { AppConstants } from 'src/app/core/utils/AppConstants';
import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {


  public url: any;

  constructor(private translate: TranslateService,
    private router: Router) {

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.url = event.url;
      }
    });
    let currentLang = localStorage.getItem(AppConstants.CURRENT_LANGUAGE_KEY);
    if (currentLang) {
      translate.setDefaultLang(currentLang);
    } else {
      localStorage.setItem(AppConstants.CURRENT_LANGUAGE_KEY, "ar");
      currentLang = "ar";
      translate.setDefaultLang('ar')
    }

    translate.addLangs(['en', 'fr', 'ar']);

    const dom: any = document.querySelector('body');

    var elem = document.querySelector("html");
    if (currentLang == "ar") {
      elem.removeAttribute("lang");
      elem.setAttribute("lang", "rtl");
      // elem.classList.add('ui-rtl')
    } else {
      elem.removeAttribute("lang");
      elem.setAttribute("lang", "ltr")
      // elem.classList.remove('ui-rtl')
    }
    const rtl = dom.classList.contains('rtl');
    if (rtl && currentLang != "ar") {
      this.rltAndLtr();
    }
    if (rtl == false && currentLang == "ar") {
      this.rltAndLtr();
    }


  }



  rltAndLtr() {
    const dom: any = document.querySelector('body');
    dom.classList.toggle('rtl');
  }

}
