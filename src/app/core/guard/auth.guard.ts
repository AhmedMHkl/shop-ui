import { AppConstants } from 'src/app/core/utils/AppConstants';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let rememberMe = localStorage.getItem(AppConstants.REMEMBER_ME);

        if ((rememberMe != null && rememberMe == "yes" && localStorage.getItem(AppConstants.AUTHORIZATION_KEY)) ||
            (rememberMe != null && rememberMe == "no" && sessionStorage.getItem(AppConstants.AUTHORIZATION_KEY))) {
            return true;
        }
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/auth/login'], { queryParams: { returnUrl: state.url } });
        return false;

    }
}
