import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppConstants } from '../utils/AppConstants';


@Injectable()
export class RequestInterceptor implements HttpInterceptor {

    constructor() {}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let token  = "";
        let rememberMe = localStorage.getItem(AppConstants.REMEMBER_ME);
        if(rememberMe == "yes"){
            token = localStorage.getItem(AppConstants.AUTHORIZATION_KEY);
        }else{
            token = sessionStorage.getItem(AppConstants.AUTHORIZATION_KEY);
        }
        if (token) {
            request = request.clone({
                setHeaders: { 
                    Authorization: 'Bearer '+token
                }
            });
        }
        return next.handle(request);
    }
}