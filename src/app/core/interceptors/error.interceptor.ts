import { AppConstants } from 'src/app/core/utils/AppConstants';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { AuthenticationService } from '../services/authentication.service';
import { EchoMessageService } from '../services/echo-message.service';
import { JsonPipe } from '@angular/common';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(
        private authenticationService: AuthenticationService,
        private alert: EchoMessageService,
        private router: Router) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                this.authenticationService.logout().subscribe(
                    res => {
                        localStorage.removeItem(AppConstants.AUTHORIZATION_KEY);
                        localStorage.removeItem(AppConstants.REMEMBER_ME);
                        sessionStorage.removeItem(AppConstants.AUTHORIZATION_KEY);
                        localStorage.removeItem(AppConstants.LOGGED_IN_USER_KEY);
                        localStorage.removeItem(AppConstants.IS_LOGGED_IN_KEY);
                        localStorage.removeItem(AppConstants.LOGGED_IN_USER_Id);
                        this.router.navigate(['auth/login']);
                        //location.reload();
                    },
                    error => {
                        localStorage.removeItem(AppConstants.AUTHORIZATION_KEY);
                        sessionStorage.removeItem(AppConstants.AUTHORIZATION_KEY);
                        localStorage.removeItem(AppConstants.REMEMBER_ME);
                        this.router.navigate(['auth/login']);
                    });
            } else if (err.status === 404 || err.status === 400) {
                //this.router.navigate(['/pages/404']);
                return throwError(err);
            } else if (err.status === 500 && err.error !== undefined && err.error.length > 0) {
                if (err.error[0].message.includes('exception')) {
                    //this.router.navigate(['/pages/404']);
                    return null;
                } else {

                }
            } else if (err.status === 500 && err.error !== undefined) {
                this.alert.errorMessage("Connection error 500" , 'Error');
                console.log(err);
                
                // if (err.error.message.includes('exception')) {
                //     this.router.navigate(['/pages/404']);
                //     return null;
                // }

            } else if (err.status === 0 && err.error !== undefined) {
                this.alert.errorMessage("Connection down", 'Error');
                return throwError(err);
            }

            return throwError(err);
        }),
            tap(event => {
                if (event instanceof HttpResponse) {
                    if (event.status === 200 || event.status === 201) {
                        let response = event.body;
                        if (response != null && response.message !== undefined) {
                            if (response.type == 'SUCCESS')
                                console.log('success');
                            else if (response.type === 'WARN')
                                console.log('warn');
                        }
                    }
                }
            }))
    }
}