import { environment } from 'src/environments/environment';

export abstract class AppConstants {
    public static USER_MENU_PERMISSIONS_KEY: string = 'userMenuPermissions';
    public static USER_PERMISSIONS_KEY: string = 'userPermissions';
    public static AUTHORIZATION_KEY: string = 'Authorization';
    public static LOGGED_IN_USER_Id: string = 'loggedInUserId';
    public static IS_LOGGED_IN_KEY: string = 'isLoggedin';
    public static CURRENT_LANGUAGE_KEY: string = 'currentLanguage';
    public static PERMISSIONS_KEY = "permissionsKey";
    public static ROOT_USER_ID: number = 1;
    public static PAGE_SIZE: number = 10;
    public static OFFSET: number = 0;
    public static REMEMBER_ME = "rememberMe";
    public static wishlistItem = "wishlistItem";
    public static quotationItems = "quotationItems";
    public static FILEUPLOADURL = environment.apiUrl + '/api/file/uploadFile';
    public static FILEDOWNLOADURL = environment.apiUrl + '/api/file/downloadFile';
    public static LOGGED_IN_USER_KEY = 'loggedInUserKey';
    public static CART_ID = 'cartId';
    public static CART_CODE = 'cartCode';
    public static CARD_ITEM = 'cartItem';
    public static SHIPPING_DETAILS = "shippingDetails";
    public static CATEGORIES = "categories";

    //
    public static  DEFAULTPRODUCTIMAGEURL = "assets/images/fashion/product/1.jpg";
}