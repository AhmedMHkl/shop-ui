import { BaseDto } from "./base-dto";

export class CommonDto extends BaseDto {
	public id: number = null;
	public name: string = null;
	public arabicName: string = null;
	public isDeleted: boolean = false;
	public active: boolean = true;
	public creationDate: string = null;
	public blocked: boolean = false;
	public createdBy: number = null;
	public createdByFullname: string = null;
	constructor() {
		super();
	}



}
