export class BaseDto extends Object {
    public id: number = null;
    public description: string = null;

    constructor() {
        super();
    }
}
