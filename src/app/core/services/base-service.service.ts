import { CommonDto } from './../models/Common/common-dto';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable()
export class BaseService<T extends CommonDto> {
  private cancelAddModeForParent$: Subject<any> = new Subject();

  protected url: string = environment.apiUrl;

  constructor(protected http: HttpClient, protected endpoint: string) { }
  
  public save(item: T): Observable<any> {
    return this.http.post(`${this.url}${this.endpoint}`, item);
  }

  public saveMultiple(items: T[]) {
    this.http.post<T>(`${this.url}${this.endpoint}`, items);
  }
  public listAll(): Observable<T[]> {
    return this.http.get<T[]>(`${this.url}${this.endpoint}`);
  }
  public listAllByParam(param: any): Observable<T[]> {
    return this.http.get<T[]>(`${this.url}${this.endpoint}/${param}`);
  }
  public listAllPaginated(pageNumber: number, pageSize: number): Observable<T[]> {
    return this.http.get<T[]>(`${this.url}${this.endpoint}`, {
      params: new HttpParams()
        .set('first', pageNumber.toString())
        .set('pageSize', pageSize.toString())
    });
  }
  public getOne(id: number): Observable<T> {
    return this.http.get<T>(`${this.url}${this.endpoint}/${id}`);
  }
  public delete(id: number): Observable<any> {
    return this.http.delete(`${this.url}${this.endpoint}/${id}`);
  }
  public update(item: T): Observable<T> {
    return this.http.put<T>(`${this.url}${this.endpoint}`, item);
  }

  public getObjectsCount(): Observable<number> {
    return this.http.get<number>(`${this.url}${this.endpoint}/count`);
  }
  
  public cancelAddModeForParent() {
    this.cancelAddModeForParent$.next(true);
  }
  
  public cancelAddModeForParentObservable() {
    return this.cancelAddModeForParent$.asObservable();
  }

  public uploadFile(content:any): Observable<any> {
    return this.http.post(`${this.url +'/api/file/uploadFile'}`,content);
  }
  
   public Unix_timestamp(t) {
    var dt = new Date(t);
    const year = dt.getFullYear();
    const mon = dt.getMonth();
    const day = dt.getDay();
    const hr = dt.getHours();
    const m = "0" + dt.getMinutes();
    return `${hr}:${m.substr(-2)} ${day}/${mon}/${year}`;
  }

  public Unix_time(t) {
    var dt = new Date(t);
    const year = dt.getFullYear();
    const mon = dt.getMonth();
    const day = dt.getDay();
    return `${day}/${mon}/${year}`;
  }
}
