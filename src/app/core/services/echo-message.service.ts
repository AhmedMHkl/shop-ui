import { AppConstants } from 'src/app/core/utils/AppConstants';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EchoMessageService {

  /*
below list of position classes that can be applied to toastr
toast-center-center
toast-top-center
toast-bottom-center
toast-top-full-width 
toast-bottom-full-width 
toast-top-left
toast-top-right
toast-bottom-right
toast-bottom-left */
  constructor(private alertService: ToastrService,
    private translateService: TranslateService) { }


  successMessage(message: string, title: string, position?: string, timeout?: number) {
    if (typeof position != "undefined" && position.trim() != "" && typeof timeout != "undefined")
      this.alertService.success(this.translateService.instant(message), this.translateService.instant(title), {
        positionClass: position,
        timeOut: timeout
      });
    else if (typeof position != "undefined" && position.trim() != "")
      this.alertService.success(this.translateService.instant(message), this.translateService.instant(title), {
        positionClass: position
      });
    else if (typeof timeout != "undefined")
      this.alertService.success(this.translateService.instant(message), this.translateService.instant(title), {
        timeOut: timeout
      });
    else
      this.alertService.success(this.translateService.instant(message), this.translateService.instant(title));
  }

  errorMessage(message: string, title: string, position?: string, timeout?: number) {
    if (typeof position != "undefined" && position.trim() != "" && typeof timeout != "undefined")
      this.alertService.error(this.translateService.instant(message), this.translateService.instant(title), {
        positionClass: position,
        timeOut: timeout
      });
    else if (typeof position != "undefined" && position.trim() != "")
      this.alertService.error(this.translateService.instant(message), this.translateService.instant(title), {
        positionClass: position
      });
    else if (typeof timeout != "undefined")
      this.alertService.error(this.translateService.instant(message), this.translateService.instant(title), {
        timeOut: timeout
      });
    else
      this.alertService.error(this.translateService.instant(message), this.translateService.instant(title));
  }
  infoMessage(message: string, title: string, position?: string, timeout?: number) {
    if (typeof position != "undefined" && position.trim() != "" && typeof timeout != "undefined")
      this.alertService.info(this.translateService.instant(message), this.translateService.instant(title), {
        positionClass: position,
        timeOut: timeout
      });
    else if (typeof position != "undefined" && position.trim() != "")
      this.alertService.info(this.translateService.instant(message), this.translateService.instant(title), {
        positionClass: position
      });
    else if (typeof timeout != "undefined")
      this.alertService.info(this.translateService.instant(message), this.translateService.instant(title), {
        timeOut: timeout
      });
    else
      this.alertService.info(this.translateService.instant(message), this.translateService.instant(title));
  }

  warningMessage(message: string, title: string, position?: string, timeout?: number) {
    if (typeof position != "undefined" && position.trim() != "" && typeof timeout != "undefined")
      this.alertService.warning(this.translateService.instant(message), this.translateService.instant(title), {
        positionClass: position,
        timeOut: timeout
      });
    else if (typeof position != "undefined" && position.trim() != "")
      this.alertService.warning(this.translateService.instant(message), this.translateService.instant(title), {
        positionClass: position
      });
    else if (typeof timeout != "undefined")
      this.alertService.warning(this.translateService.instant(message), this.translateService.instant(title), {
        timeOut: timeout
      });
    else
      this.alertService.warning(this.translateService.instant(message), this.translateService.instant(title));
  }
  //to show toastr with html content
  showSuccessHTMLMessage(message: string, title: string, position?: string, timeout?: number) {

    if (typeof position != "undefined" && position.trim() != "" && typeof timeout != "undefined")
      this.alertService.success(message, this.translateService.instant(title), {
        enableHtml: true,
        positionClass: position,
        timeOut: timeout
      });
    else if (typeof position != "undefined" && position.trim() != "")
      this.alertService.success(message, this.translateService.instant(title), {
        enableHtml: true,
        positionClass: position,
      });
    else if (typeof timeout != "undefined")
      this.alertService.success(message, this.translateService.instant(title), {
        enableHtml: true,
        timeOut: timeout
      });
    else
      this.alertService.success(message, this.translateService.instant(title), {
        enableHtml: true
      });

  }

  showErrorHTMLMessage(message: string, title: string, position?: string, timeout?: number) {
    if (typeof position != "undefined" && position.trim() != "" && typeof timeout != "undefined")
      this.alertService.error(message, this.translateService.instant(title), {
        enableHtml: true,
        positionClass: position,
        timeOut: timeout
      });
    else if (typeof position != "undefined" && position.trim() != "")
      this.alertService.error(message, this.translateService.instant(title), {
        enableHtml: true,
        positionClass: position,
      });
    else if (typeof timeout != "undefined")
      this.alertService.error(message, this.translateService.instant(title), {
        enableHtml: true,
        timeOut: timeout
      });
    else
      this.alertService.error(message, this.translateService.instant(title), {
        enableHtml: true
      });
  }
  showWarningHTMLMessage(message: string, title: string, position?: string, timeout?: number) {
    if (typeof position != "undefined" && position.trim() != "" && typeof timeout != "undefined")
      this.alertService.warning(message, this.translateService.instant(title), {
        enableHtml: true,
        positionClass: position,
        timeOut: timeout
      });
    else if (typeof position != "undefined" && position.trim() != "")
      this.alertService.warning(message, this.translateService.instant(title), {
        enableHtml: true,
        positionClass: position,
      });
    else if (typeof timeout != "undefined")
      this.alertService.warning(message, this.translateService.instant(title), {
        enableHtml: true,
        timeOut: timeout
      });
    else
      this.alertService.warning(message, this.translateService.instant(title), {
        enableHtml: true
      });
  }
  showInfoHTMLMessage(message: string, title: string, position?: string, timeout?: number) {
    if (typeof position != "undefined" && position.trim() != "" && typeof timeout != "undefined")
      this.alertService.info(message, this.translateService.instant(title), {
        enableHtml: true,
        positionClass: position,
        timeOut: timeout
      });
    else if (typeof position != "undefined" && position.trim() != "")
      this.alertService.info(message, this.translateService.instant(title), {
        enableHtml: true,
        positionClass: position,
      });
    else if (typeof timeout != "undefined")
      this.alertService.info(message, this.translateService.instant(title), {
        enableHtml: true,
        timeOut: timeout
      });
    else
      this.alertService.info(message, this.translateService.instant(title), {
        enableHtml: true
      });
  }
  getCurrentLang(): string {
    var currentLang = "ar";
    if (sessionStorage.getItem(AppConstants.CURRENT_LANGUAGE_KEY) != null)
      currentLang = sessionStorage.getItem(AppConstants.CURRENT_LANGUAGE_KEY);
    else if (localStorage.getItem(AppConstants.CURRENT_LANGUAGE_KEY))
      currentLang = localStorage.getItem(AppConstants.CURRENT_LANGUAGE_KEY);

    return currentLang;
  }

}
