import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Category } from 'src/app/shared/classes/category';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConstants } from '../utils/AppConstants';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  protected url: string = environment.apiUrl;
  private endPoint: string = '/api/v1';
  private currentLanguage = localStorage.getItem("currentLanguage")
  public categoryList: Subject<Array<any>> = new Subject<Array<any>>();

  constructor(
    protected http: HttpClient,

  ) { }

  categoriesCountByProduct(): Observable<any> {
    return this.http.get(`${environment.apiUrl}${this.endPoint}/count/by/product`);
  }


  cachedCategories(): any[] {
    let cats = localStorage.getItem(AppConstants.CATEGORIES)
    if (cats != null && cats != undefined && cats != '') {
      let res = JSON.parse(cats)
      this.categoryList.next(res)
      return res;
    } else {
      this.categoriesCountByProduct().subscribe(res => {
        localStorage.setItem(AppConstants.CATEGORIES, JSON.stringify(res))
        this.cachedCategories()
      })
    }
  }

  getCategoriesChild(cat: Category, allRelatedCats: any[]): any[] {
    if (cat.children.length > 0)
      cat.children.forEach(cat => {
        allRelatedCats.push(cat.code);
        if (cat.children.length > 0) {
          this.getCategoriesChild(cat, allRelatedCats);
        } else
          return;
      });
    else
      return;
  }

  getAllCategories(): Observable<any> {
    return this.http.get(`${environment.apiUrl}${this.endPoint}/categories`, {
      params: new HttpParams().set("lang", this.currentLanguage)
    });
  }

  public getCategoryHierarchy(): Observable<any> {
    return this.http.get<any>(`${this.url}${this.endPoint}/category`, {
      params: new HttpParams().set("lang", this.currentLanguage)
    });
  }

}
