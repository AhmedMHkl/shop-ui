import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { Category } from 'src/app/shared/classes/category';
import { environment } from '../../../environments/environment';
import { AppConstants } from '../utils/AppConstants';
import { BaseService } from './base-service.service';

@Injectable({
  providedIn: 'root'
})
export class CommonCoreService extends BaseService<any>{


  private endPoint: string = '/api/v1';
  private currentLanguage = localStorage.getItem("currentLanguage")
  public categoryList: Subject<Array<any>> = new Subject<Array<any>>();

  constructor(
    protected http: HttpClient,
    private router: Router,

  ) {
    super(http, '/api/v1');
  }

  listAllSystemLanguage() {
  }

  getAllCountries(): Observable<any> {
    return this.http.get(`${environment.apiUrl}${this.endPoint}/country`);
  }

  getCountryZones(countryCode: string): Observable<any> {
    return this.http.get(`${environment.apiUrl}${this.endPoint}/zones?code=${countryCode}`);
  }

  register(customer: Object): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}${this.endPoint}/customer/register`, customer, { observe: 'response' });
  }

}
