import { Subject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../environments/environment'
import { AppConstants } from 'src/app/core/utils/AppConstants';
import { BaseService } from './base-service.service';

@Injectable()
export class AuthenticationService extends BaseService<any>{
    loggedIn = new Subject();

    private endPoint: string = 'api/auth';
    constructor(protected http: HttpClient,private router: Router) {
        super(http, 'api/auth');
    }
  

    login(username: string, password: string) {
        return this.http.post<any>(`${environment.apiUrl}/${this.endPoint}/signin`, { username: username, password: password }, {observe: 'response'});
    }
    
    logout() {
        return this.http.get<any>(`${environment.apiUrl}/logout`);
    }

    get isLoggedIn() {
        if(localStorage.getItem(AppConstants.AUTHORIZATION_KEY) || sessionStorage.getItem(AppConstants.AUTHORIZATION_KEY)){
            this.loggedIn.next(true);
        }else{
            this.loggedIn.next(false);
        }
        return this.loggedIn.asObservable();
    }
    setloggedIn(value){
         this.loggedIn.next(value);
    }
}