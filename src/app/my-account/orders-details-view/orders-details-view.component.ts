import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as $ from 'jquery';
import { ConfirmationService, DialogService, MenuItem } from 'primeng/api';
import { CommonCoreService } from 'src/app/core/services/common-core.service';
import { EchoMessageService } from 'src/app/core/services/echo-message.service';
import { OrderService } from 'src/app/shared/services/order.service';
import { StoreService } from 'src/app/shared/services/store.service';
import { OrdersDetails } from '../models/orders-details';
import { ProductEntity } from '../models/product-entity';

@Component({
  selector: 'app-orders-details-view',
  templateUrl: './orders-details-view.component.html',
  styleUrls: ['./orders-details-view.component.scss']
})
export class OrdersDetailsViewComponent implements OnInit {

  @Input() order: any;

  entity: OrdersDetails = new OrdersDetails();
  entityAgreed:any;
  Product: ProductEntity = new ProductEntity();
  orderStates: any[] = [];
  total: any[] = [];
  subtotal: any[] = [];
  ProductList: any[] = [];
  Countrys: any[] = [];
  billingCountrys: any[] = [];
  languages: any[] = [];
  zones: any[] = [];
  Billingzones: any[] = [];
  view: boolean = true;
  billingview: boolean = true;
  editMode = false;
  menuItems: MenuItem[];
  public SupTotal = 0;
  public Total = 0;
  columnDefs = [
    { header: 'Item', field: 'productName', filter: false, sort: false, translate: false },
    { header: 'Quantity', field: 'orderedQuantity', filter: false, sort: false, translate: false },
    { header: 'Price', field: 'price', filter: false, sort: false, translate: false },
    { header: 'Total', field: 'subTotal', filter: false, sort: false, translate: false },

  ];

  constructor(private orderService: OrderService,
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    private storeService: StoreService,
    private alertMessage: EchoMessageService,
    private router: Router,
    public dialogService: DialogService,
    public commonService: CommonCoreService,
    private confirmationService: ConfirmationService,
    private alerMessage: EchoMessageService,
    private translateService: TranslateService,
  ) {

  }

  ngOnInit() {

    // tab js
    $("#tab-1").css("display", "Block");
    $(".default").css("display", "Block");
    $(".tabs li a").on('click', function () {
      event.preventDefault();
      $(this).parent().parent().find("li").removeClass("current");
      $(this).parent().addClass("current");
      var currunt_href = $(this).attr("href");
      $('#' + currunt_href).show();
      $(this).parent().parent().parent().find(".tab-content").not('#' + currunt_href).css("display", "none");
    });
    this.getOrder();
    this.listCountries();
    console.log("this.entity", this.entity.billing)
  }



  getOrder() {
    let res = this.order
    console.log("current order : ", res)
    this.entity = res;
    this.ProductList = res.products;
    this.entity.datePurchased = this.datePipe.transform(this.entity.datePurchased, "dd-MM-yyyy");
    this.SupTotal = res.totals[0].value;
    this.Total = res.totals[1].value;
    if (this.entity.billing.country != null) {
      console.log(this.entity.billing.country)
    }
    if (this.entity.delivery.country != null) { this.listZons(this.entity.delivery.country); }
  }

  public listZons(code) {
    this.commonService.getCountryZones(code).subscribe(
      res => {
        this.zones = res;
        this.view = true;
        this.Billingzones = res;
        this.billingview = true;
      }, err => {
        this.view = false;
        this.billingview = false;
      }
    );
  }

  public listCountries() {
    this.commonService.getAllCountries().subscribe(
      res => {
        this.Countrys = res;
        this.billingCountrys = res;
      }, err => {
        console.error(err);
      }
    );
  }

}
