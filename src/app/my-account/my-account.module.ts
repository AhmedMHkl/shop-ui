import { DashboardComponent } from './dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyAccountRoutingModule } from './my-account-routing.module';
import { SharedModule } from '../shared/shared.module';
import { OrdersDetailsViewComponent } from './orders-details-view/orders-details-view.component';
import { PagesModule } from '../pages/pages.module';

@NgModule({
  declarations: [
    DashboardComponent,
    OrdersDetailsViewComponent,

  ],
  imports: [
    CommonModule,
    MyAccountRoutingModule,
    SharedModule,
    PagesModule
  ]
})
export class MyAccountModule { }
