import { BillingAddress } from "./billing-address";
import { ShippingAddress } from "./shipping-address";

export class UserInfo {
    id: string;
    firstName: string;
    lastName: string;
    userName: string;
    gender: string;
    emailAddress: string;
    billing: BillingAddress;
    delivery: ShippingAddress;
    city: string;
    company: string;
    country: string;
    phone: string;
    language: string;
}
