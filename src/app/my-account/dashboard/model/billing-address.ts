export class BillingAddress {
    address: string;
    billingAddress: boolean;
    bilstateOther: string;
    city: string;
    company: string;
    country: string;
    latitude: any;
    longitude: any;
    phone: string;
    postalCode: string;
    stateProvince: any;
    zone: string;
}
