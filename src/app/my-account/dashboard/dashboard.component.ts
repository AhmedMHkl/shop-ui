import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/customer.service';
import { UserInfo } from './model/user-info';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CartService } from 'src/app/shared/services/cart.service';
import { OrderService } from 'src/app/shared/services/order.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductsService } from 'src/app/shared/services/products.service';
import { CommonCoreService } from 'src/app/core/services/common-core.service';
import { AuthService } from 'src/app/auth/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { EchoMessageService } from 'src/app/core/services/echo-message.service';
import { ShippingAddress } from './model/shipping-address';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  userInfo: UserInfo = new UserInfo();
  currentSection = 'accountInfo'
  currentOrder: any;
  displayOrder = false;
  public billingForm: FormGroup;
  public shippingForm: FormGroup;
  countries: any[] = [];
  customerOrders: any[] = [];

  constructor(
    private customerService: CustomerService,
    private fb: FormBuilder,
    private cartService: CartService,
    public productsService: ProductsService,
    private orderService: OrderService,
    private route: ActivatedRoute,
    private router: Router,
    private commonCoreService: CommonCoreService,
    private spinner: NgxSpinnerService,
    private authService: AuthService,
    private alert: EchoMessageService,
  ) {

    this.billingForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      lastName: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      phone: ['', [Validators.required, Validators.pattern('[0-9]+')]],
      address: ['', [Validators.required, Validators.maxLength(50)]],
      country: ['', Validators.required],
      city: ['', Validators.required],
      postalCode: ['', Validators.required],
      billingAddress: [false],
      bilstateOther: [''],
      company: [''],
      latitude: [''],
      longitude: [''],
      stateProvince: [''],
      zone: ['']
    })

    this.shippingForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      lastName: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      phone: ['', [Validators.required, Validators.pattern('[0-9]+')]],
      address: ['', [Validators.required, Validators.maxLength(50)]],
      country: ['', Validators.required],
      city: ['', Validators.required],
      postalCode: ['', Validators.required],
      billingAddress: [false],
      bilstateOther: [''],
      company: [''],
      latitude: [''],
      longitude: [''],
      stateProvince: [''],
      zone: ['']
    })

  }

  billingFields: any[]

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      this.currentSection = params['section'] || 'accountInfo'
    })

    this.getCustomerInfo();
    this.commonCoreService.getAllCountries().subscribe(res => {
      this.countries = res;
    });



    this.orderService.listCustomerOrders().subscribe(resp => {
      console.log("orders", resp);
      this.customerOrders = resp.orders
    });

    /*
    setTimeout(() => {
      this.billingFields = [
        { val: 'test', required: false, label: 'First Name', name: 'firstName', controlType: 'textbox', fieldType: 'text' },
        { label: 'Last Name', name: 'lastName', controlType: 'textbox', fieldType: 'text' },
        { label: 'Phone No', name: 'phone', controlType: 'textbox', fieldType: 'text' },
        { label: 'Address', name: 'address', controlType: 'textbox', fieldType: 'text' },
        { label: 'Country', name: 'country', controlType: 'dropdown', lookups: this.countries },
        { label: 'City', name: 'city', controlType: 'textbox', fieldType: 'text' },
        { label: 'Postal Code', name: 'postalCode', controlType: 'textbox', fieldType: 'text' }
      ]
    }, 1000);
    */

  }

  getCustomerInfo() {
    this.customerService.getCustomerInfo().subscribe(
      res => {
        this.userInfo = res;
        if (this.userInfo.delivery == null || this.userInfo.delivery == undefined)
          this.userInfo.delivery = new ShippingAddress()
        console.log("dashboard component >> ", res);
      }, err => {
        console.log(err);
      });
  }

  /*************************************
 * save billingFields
 * 
**************************************/
  submitted = false
  showBillingData = true
  showBillingForm = false
  showShippingData = true
  showShippingForm = false

  startEditMode(section: string) {
    if (section === 'billing') {
      this.billingForm.patchValue(this.userInfo.billing)
      this.showBillingData = false
      this.showShippingData = false
      this.showBillingForm = true
      this.submitted = false
    } else if (section === 'shipping') {
      this.shippingForm.patchValue(this.userInfo.delivery)
      this.showBillingData = false
      this.showShippingData = false
      this.showShippingForm = true
      this.submitted = false
    }
  }

  endEditMode(section: string) {
    if (section === 'billing') {
      this.showBillingData = true
      this.showShippingData = true
      this.showBillingForm = false
      this.submitted = false
      this.billingForm.reset
    } else if (section === 'shipping') {
      this.showBillingData = true
      this.showShippingData = true
      this.showShippingForm = false
      this.submitted = false
      this.shippingForm.reset
    }
  }

  saveBillingData() {
    this.submitted = true
    console.log(this.billingForm);
    if (this.billingForm.valid && this.billingForm.dirty) {
      this.userInfo.billing = this.billingForm.value
      this.userInfo.billing.billingAddress = true
      console.log(this.userInfo);
      this.customerService.updateCustomerAddress(this.userInfo.id, this.userInfo.billing).subscribe((resp) => {
        console.log(resp)
        this.alert.successMessage("data updated successfully", 'update', 'toast-bottom-right', 5000);
        this.endEditMode('billing')
      })
    }
  }

  saveshippingData() {
    this.submitted = true
    console.log(this.shippingForm);
    if (this.shippingForm.valid && this.shippingForm.dirty) {
      this.userInfo.delivery = this.shippingForm.value
      this.userInfo.delivery.billingAddress = false
      console.log(this.userInfo);
      this.customerService.updateCustomerAddress(this.userInfo.id, this.userInfo.delivery).subscribe((resp) => {
        console.log(resp)
        this.alert.successMessage("data updated successfully", 'update', 'toast-bottom-right', 5000);
        this.endEditMode('shipping')
      })
    }
  }

  /*************************************
   * billingFields
   * 
  **************************************/


  feedBack(event: FormGroup) {
    console.log("event feedback", event);
    if (event.valid)
      console.log(event.value);
  }

  /*************************************
 * changePassword
 * 
**************************************/

  changePassword() {
    if (this.userInfo.emailAddress) {
      let authRequest = { username: this.userInfo.emailAddress, password: 'testpassword' }
      this.spinner.show();
      this.authService.forgetPassword(authRequest).subscribe(
        res => {
          this.alert.successMessage("passwordHasBeenReset", 'Success');
          this.spinner.hide();
        }, err => {
          this.alert.errorMessage("passwordNotReset", 'Error');
          this.spinner.hide();
        }
      );
    }
  }


  /*************************************
   * prepareFullNameChange()
   *************************************/
  fullNameModel = { fname: '', lname: '' }
  prepareFullNameChange() {
    this.fullNameModel = { fname: this.userInfo.firstName, lname: this.userInfo.lastName }
  }

  updateFullName() {
    console.log(this.fullNameModel);
    if (this.fullNameModel.fname === this.userInfo.firstName && this.fullNameModel.lname === this.userInfo.lastName) return
    this.customerService.updateCustomerFullName(this.userInfo.id, this.fullNameModel.fname, this.fullNameModel.lname).subscribe(resp => {
      console.log("on success populate cuurent user object");
      this.userInfo.firstName = this.fullNameModel.fname
      this.userInfo.lastName = this.fullNameModel.lname
      this.fullNameModel = { fname: '', lname: '' }
      this.alert.successMessage("data updated successfully", 'update', 'toast-bottom-right', 5000);
    })
  }

  /***********************************
   * updateEmailAddress()
   ***********************************/
  emailAddress = ''
  prepareEmailAddressChange() {
    this.emailAddress = this.userInfo.emailAddress
  }
  updateEmailAddress() {
    if (this.emailAddress === this.userInfo.emailAddress) return
    this.customerService.updateEmailAddress(this.userInfo.id, this.emailAddress).subscribe(resp => {
      console.log("on success populate cuurent user object");
      this.userInfo.emailAddress = this.emailAddress
      this.emailAddress = ''
      this.alert.successMessage("data updated successfully", 'update', 'toast-bottom-right', 5000);
    })
  }


  /***********************************
   * defaultLang()
   ***********************************/
  defaultLang = ''
  prepareDefaultLanguage() {
    this.defaultLang = this.userInfo.language
  }
  updateDefaultLanguage() {
    if (this.defaultLang === this.userInfo.language) return
    this.customerService.updateDefaultLanguage(this.userInfo.id, this.defaultLang).subscribe(resp => {
      console.log("on success populate cuurent user object");
      this.userInfo.language = this.defaultLang
      //this.defaultLang = ''
      this.alert.successMessage("data updated successfully", 'update', 'toast-bottom-right', 5000);
    })
  }

  /*************************************
* logout
* 
**************************************/

  logout() {
    this.authService.logout()
  }


}
