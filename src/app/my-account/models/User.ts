import { CommonDto } from 'src/app/core/models/Common/common-dto';

export class User extends CommonDto {
    public firstName: string = null;
    public email: string = null;
    public lastName: string = null;
    public comment: string = null;
    public phone: string = null;
    public subject: string = null;
    public CustomerId: Number;
    public name: string = null;
    public customer: string = null;




    constructor() {
        super();
    }
}