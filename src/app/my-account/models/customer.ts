import { CommonDto } from 'src/app/core/models/Common/common-dto';
import { Address } from './address';

export class Customer extends CommonDto {
    public language: string = null;
    public firstName: string = null;
    public emailAddress: string = null;
    public lastName: string = null;
    public provider: string = null;
    public clearPassword: string = "localhost:8080";
    public encodedPassword: string = null;
    public userName: string = null;
    public rating: number = null;
    public ratingCount: number = null;
    public gender: string = null;
    billing: Address = new Address()
    delivery: Address = new Address()
    supportedLanguages: Array<Object>;
    constructor() {
        super();
    }
}