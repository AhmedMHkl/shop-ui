import { CommonDto } from 'src/app/core/models/Common/common-dto';
export class ProductEntity extends CommonDto {
    price: Number;
    quantity: number;
    sku: String;
    productShipeable: boolean = false;
    preOrder: boolean = false;
    productVirtual: boolean = false;
    quantityOrderMaximum: number = -1;//default unlimited
    quantityOrderMinimum: number = 1;//default 1
    productIsFree: boolean;
    available: boolean;
    visible: boolean = true;
    productLength: Number;
    productWidth: Number;
    productHeight: Number;
    productWeight: Number;
    rating: Number = 0;
    ratingCount: number;
    sortOrder: number;
    dateAvailable: String;
    refSku: String;
    rentalDuration: number;
    rentalPeriod: number;
}
