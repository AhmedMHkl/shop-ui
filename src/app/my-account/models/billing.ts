export class Billing {
    public address: String = null;
    public city: String = null;
    public company: String = null;
    public country: String = null;
    public stateProvince: String = null;
    public postalCode: String = null;
    public phone: String = null;
    public zone: String = null;
    public firstName: String = null;
    public lastName: String = null;
    public billingAddress: String = null;
    public bilstateOther: String = null;
    constructor() {
    }
}