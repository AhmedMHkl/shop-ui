import { CommonDto } from 'src/app/core/models/Common/common-dto';
import { Billing } from './billing';
import { Delivery } from './delivery';
import { Customer } from './customer';

export class OrdersDetails extends CommonDto {
    public products: any[] = [];
    public emailAddress: string;
    public total: any = null;
    public recordsTotal: number = null
    public id: number = null
    public codrawuntry: number = null
    public recordsFiltered: number = null
    public postalCode: string = null;
    public phone: string = null;
    public zone: string = null;
    public firstName: string = null;
    public lastName: string = null;
    public billingAddress: boolean = true;
    public bilstateOther: string = null;
    public customerAgreed: boolean = null;
    public totals: any[] = [];
    public billing: Billing = new Billing()
    public delivery: Delivery = new Delivery()
    public customer: Customer = new Customer();
    public orderProductItems: any[] = [];
    public orderStatus: string = null;
    public datePurchased: string = null;

    public paymentType: any;
    public paymentModule: any;
    constructor() {
        super();
    }
}
