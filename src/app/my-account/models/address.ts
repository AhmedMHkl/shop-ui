export class Address {
    public address: String = null;
    public city: string = null;
    public country: string = null;
    public stateProvince: string = null;
    public postalCode: string = null;
    public phone: string = null;
    public zone: string = null;
    public firstName: string = null;
    public lastName: string = null;
    public billingAddress: boolean = true;
    public bilstateOther: string = null;
    public company: any;
    constructor() {
    }
}