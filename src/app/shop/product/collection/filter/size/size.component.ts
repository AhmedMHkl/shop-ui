import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SizeFilter } from 'src/app/shared/classes/product2';

@Component({
  selector: 'app-size',
  templateUrl: './size.component.html',
  styleUrls: ['./size.component.scss']
})
export class SizeComponent implements OnInit {

  public activeItem : any = '';

  // Using Input and Output EventEmitter
  @Input()  sizesFilters  :  SizeFilter[] = [];
  @Output() sizeFilters   :  EventEmitter<SizeFilter[]> = new EventEmitter<SizeFilter[]>();

  constructor() { }
  
  ngOnInit() {  
    console.log(this.sizesFilters);
  }

  // Click to call function 
  public changeSize(sizes: SizeFilter) {
    this.activeItem = sizes.size
    if(sizes.size) {
      this.sizeFilters.emit([sizes]);
    } else {
      this.sizeFilters.emit([]);
    }
  }
}
