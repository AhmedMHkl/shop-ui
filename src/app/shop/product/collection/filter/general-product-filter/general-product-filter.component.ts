import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef, AfterViewChecked } from '@angular/core';
declare var $: any;

@Component({
  selector: 'general-product-filter',
  templateUrl: './general-product-filter.component.html',
  styleUrls: ['./general-product-filter.component.scss']
})
export class GeneralProductFilterComponent implements OnInit, AfterViewChecked {

  @Input() productsAttributes: any[] = [];
  @Output() anyFilterEmitter: EventEmitter<any[]> = new EventEmitter<any[]>();

  // Array
  public checkedTagsArray: any[] = [];

  constructor(
    private changeDetector: ChangeDetectorRef,

  ) { }

  ngOnInit() {

    $('.reeeeeeeeeee').on('click', function (e) {
      e.preventDefault;
      var speed = 300;
      var thisItem = $(this).parent(),
        nextLevel = $(this).next('.ceeeeeeeeeeeee');
      if (thisItem.hasClass('open')) {
        thisItem.removeClass('open');
        nextLevel.slideUp(speed);
      } else {
        thisItem.addClass('open');
        nextLevel.slideDown(speed);
      }
    });

    console.log("productsAttributes", this.productsAttributes);
    // this.anyFilterEmitter.emit(this.checkedTagsArray);
  }

  ngAfterViewChecked() {
    this.changeDetector.detectChanges();
  }

  // value checked call this function
  checkedFilter(event) {
    let index = this.checkedTagsArray.indexOf(event.target.value);  // checked and unchecked value
    if (event.target.checked)
      this.checkedTagsArray.push(event.target.value); // push in array cheked value
    else
      this.checkedTagsArray.splice(index, 1);  // removed in array unchecked value
    this.anyFilterEmitter.emit(this.checkedTagsArray);
  }

}
