import { Component, OnInit, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import 'rxjs/add/observable/interval';

@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PriceComponent implements OnInit {

  // Using Output EventEmitter
  @Output() priceFilters = new EventEmitter();

  // define min, max and range
  public min: number = 100;
  public max: number = 10000;
  public range = [100, 10000];

  constructor() { }

  ngOnInit() { }

  // rangeChanged
  priceChanged(event: any) {
    console.log(event);
    this.priceFilters.emit(event);
  }

}