import { animate, style, transition, trigger } from "@angular/animations";
import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { Category } from "src/app/shared/classes/category";
import { Product } from "src/app/shared/classes/product";
import { Product2 } from '../../../../shared/classes/product2';
import { ProductsService } from '../../../../shared/services/products.service';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss'],
  //changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [  // angular animation
    trigger('Animation', [
      transition('* => fadeOut', [
        style({ opacity: 0.3 }),
        animate(200, style({ opacity: 0.3 }))
      ]),
      transition('* => fadeIn', [
        style({ opacity: 0.3 }),
        animate(250, style({ opacity: 0.3 }))
      ])
    ])
  ]
})
export class CollectionLeftSidebarComponent implements OnInit, AfterViewChecked {

  public allProducts: Product2[] = [];
  public filteredProducts: Product2[] = [];


  public colorFilters: any[] = [];
  public sizeFilters: any[] = [];
  public priceFilters: any[] = [];
  public tagsFilters: any[] = [];
  public generalFilters: any[] = [];
  public tags: any[] = [];
  public colors: any[] = [];
  public sizes: any[] = [];
  public sortByOrder: string = '';   // sorting
  public animation: any;   // Animation
  public categoryData: Category = new Category();
  categoryId: any;
  public allvariants: any[] = [];
  lastKey = ''      // key to offset next query from
  finished = false  // boolean when end of data is reached

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private productsService: ProductsService,
    private echoProductsService: EchoProductsService,
    private changeDetector: ChangeDetectorRef,
  ) {
    this.getAllProducts();
  }

  getAllProducts() {
    this.route.params.subscribe(params => {
      this.categoryId = +params['catId'];
      this.getcategoryById(this.categoryId);
      this.echoProductsService.getAllProducts(0, 10, this.categoryId).subscribe(
        res => {
          let products = res.products
          this.allProducts = this.filteredProducts = products;
          console.log("products ", products);
          this.productsService.getCategoryVariantsById(this.categoryId).subscribe(result => {
            this.allvariants = result
            console.log("allvariants", result);
            this.getTags(this.allProducts);
          })
        })
    })
  }

  ngOnInit() {

  }

  ngAfterViewChecked() {
    this.changeDetector.detectChanges();
  }

  catLoaded = false
  getcategoryById(id) {
    this.productsService.getCategoryById(id).subscribe(
      (res) => {
        console.log("category data : ", res);
        this.categoryData = res
        this.catLoaded = true
      })
  }

  public getTags(products) {
    let uniqueBrands = []
    let itemBrand = Array();
    products.map((product, index) => {
      if (product.manufacturer) {
        const index = uniqueBrands.map(function (e) { return e.id; }).indexOf(product.manufacturer.id);
        if (index === -1) uniqueBrands.push(product.manufacturer);
      }
    });
    for (let i = 0; i < uniqueBrands.length; i++) {
      itemBrand.push({ brand: uniqueBrands[i] })
    }
    this.tags = itemBrand
    console.log("this.tags", this.tags);
  }

  // Animation Effect fadeIn
  public fadeIn() {
    this.animation = 'fadeIn';
  }

  // Animation Effect fadeOut
  public fadeOut() {
    this.animation = 'fadeOut';
  }

  // Initialize filetr Items
  public filterItems(): Product2[] {
    // console.log("calling filter func :: ");
    this.filteredProducts = this.allProducts.filter((item: Product) => {
      ////////////
      let Tags: boolean = false;
      if (this.tagsFilters.length == 0) {
        Tags = true;
      } else {
        const atag = item.manufacturer ? this.tagsFilters.includes(item.manufacturer.code) : null;
        if (atag) Tags = true;
      }
      /////////////////////////////////////////////////
      let generalAttr: boolean = false;
      if (this.generalFilters.length == 0) {
        generalAttr = true;
      } else if (item.options != null) {
        item.options.forEach(opt => {
          const attr = opt.optionValues.find(ele => this.generalFilters.includes(ele.code));
          if (attr) generalAttr = true;
        })
      }
      /////////////////////////////////////////////
      let Price: boolean = false;
      if (this.priceFilters.length == 0) {
        Price = true;
      } else {
        if (item.price >= this.priceFilters[0] && item.price <= this.priceFilters[1]) {
          Price = true;
        }
      }

      //return Colors && Sizes && Tags && Price; // return true
      return generalAttr && Tags && Price;
    });
    // emit new product list
    //this.filteredProducts.emit(this.products);
    //
    return this.filteredProducts;
  }

  // Update tags filter
  public updateTagFilters(tags: any[]) {
    this.tagsFilters = tags;
    this.filterItems();
    this.animation == 'fadeOut' ? this.fadeIn() : this.fadeOut(); // animation
  }

  generalProductFilterListChanged(event) {
    console.log("generalProductFilterListChanged", event);
    this.generalFilters = event;
    this.filterItems();
  }

  // Update price filter
  public updatePriceFilters(price: any) {
    console.log("calling");
    let items = this.allProducts.filter(item => {
      if (item.price >= price[0] && item.price <= price[1])
        return true
    });
    this.filteredProducts = items;
  }

  public twoCol() {
    if ($('.product-wrapper-grid').hasClass("list-view")) { } else {
      $(".product-wrapper-grid").children().children().children().removeClass();
      $(".product-wrapper-grid").children().children().children().addClass("col-lg-6");
    }
  }

  public threeCol() {
    if ($('.product-wrapper-grid').hasClass("list-view")) { } else {
      $(".product-wrapper-grid").children().children().children().removeClass();
      $(".product-wrapper-grid").children().children().children().addClass("col-lg-4");
    }
  }

  public fourCol() {
    if ($('.product-wrapper-grid').hasClass("list-view")) { } else {
      $(".product-wrapper-grid").children().children().children().removeClass();
      $(".product-wrapper-grid").children().children().children().addClass("col-lg-3");
    }
  }

  public sixCol() {
    if ($('.product-wrapper-grid').hasClass("list-view")) { } else {
      $(".product-wrapper-grid").children().children().children().removeClass();
      $(".product-wrapper-grid").children().children().children().addClass("col-lg-2");
    }
  }

  // For mobile filter view
  public mobileFilter() {
    $('.collection-filter').css("left", "-15px");
  }

  // Infinite scroll
  // public onScroll() {
  //   this.lastKey = _.last(this.allItems)['id'];
  //   if (this.items.length > 0 && this.lastKey != _.last(this.items)['id']) {
  //     this.finished = false
  //   }
  //   // If data is identical, stop making queries
  //   if (this.items.length > 0 && this.lastKey == _.last(this.items)['id']) {
  //     this.finished = true
  //   }
  //   if (this.products.length < this.allItems.length) {
  //     let len = this.products.length;
  //     for (var i = len; i < len + 4; i++) {
  //       if (this.allItems[i] == undefined) return true
  //       this.products.push(this.allItems[i]);
  //     }
  //   }
  // }

  // sorting type ASC / DESC / A-Z / Z-A etc.
  public onChangeSorting(val) {
    this.sortByOrder = val;
    this.animation == 'fadeOut' ? this.fadeIn() : this.fadeOut(); // animation
  }

}
