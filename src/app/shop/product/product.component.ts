import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartItem } from '../../shared/classes/cart-item';
import { ProductsService } from '../../shared/services/products.service';
import { WishlistService } from '../../shared/services/wishlist.service';
import { CartService } from '../../shared/services/cart.service';
import { Observable, of } from 'rxjs';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { AuthService } from 'src/app/auth/auth.service';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';
import { EchoMessageService } from 'src/app/core/services/echo-message.service';
import { QuotationService } from 'src/app/shared/services/quotation.service';
import { DefaultImage } from 'src/app/shared/directives/defaultImage.directive';
import { Product } from 'src/app/shared/classes/product';

@Component({
  selector: 'product-card',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})

export class ProductComponent implements OnInit {

  @Input() product: any;

  public variantImage: any = '';
  public selectedItem: any = '';
  defaultImageUrl = "assets/images/fashion/product/1.jpg";

  constructor(
    private router: Router,
    public productsService: ProductsService,
    private wishlistService: WishlistService,
    private cartService: CartService,
    private echoProductService: EchoProductsService,
    private authService: AuthService,
    private echoMessageService: EchoMessageService,
    private quotationService: QuotationService
  ) {
  }

  ngOnInit() {
    //console.log(environment.apiUrl + this.product.image.imageUrl);
    if (this.product.image != null && this.product.image.imageUrl != null) {
      this.variantImage = environment.apiUrl + this.product.image.imageUrl
    }
  }

  // Add to cart
  public addToCart(product: Product, quantity: number = 1) {
    console.log(product);

    // >>>>>>> backend
    const cartItem = { attributes: [], product: product.id, quantity: quantity };
    const customerId = this.authService.loggedInUserId();
    if (customerId !== null && customerId !== undefined && customerId !== "") {
      // add to customer cart
      this.echoProductService.addLoggedInCustomerProductToCart(customerId, cartItem).subscribe(
        (resp) => {
          console.log(resp)
          localStorage.setItem("cartCode", resp.code);
          localStorage.setItem("cartId", resp.id);
          const res = this.cartService.addToCart(product, quantity);
          console.log("after add to localstorage cart :- ", res);
        },
        (err) => {
          console.log("err", err);
          this.echoMessageService.errorMessage(err.error.message, 'common_error_accured', 'toast-top-right', 5000);
        });
    }
    else {
      // add to Anonymous cart
      this.echoProductService.addAnonymousProductToCart(cartItem).subscribe(
        (resp) => {
          console.log(resp)
          localStorage.setItem("cartCode", resp.code);
          localStorage.setItem("cartId", resp.id);

          const res = this.cartService.addToCart(product, quantity);
          console.log("after add to localstorage cart :- ", res);
        },
        (err) => {
          console.log(err);
          this.echoMessageService.errorMessage(err.error.message, 'common_error_accured', 'toast-top-right', 5000);
        });
    }

  }

  // Add to compare
  public addToCompare(product: Product) {
    this.productsService.addToCompare(product);
  }

  // Add to wishlist
  public addToWishlist(product: Product) {
    this.wishlistService.addToWishlist(product);
  }

  // Change variant images
  public changeVariantImage(image) {
    this.variantImage = image;
    this.selectedItem = image;
  }

  // rate product
  updateProductRate(product, value) {
    console.log(product, value);
    product.rating = value;
    this.echoProductService.updateProduct(product).subscribe(res => {
      console.log(res);
    });
  }

  setProductId(id: number) {
    console.log("currentlyPreviewdProductId ", id);
    localStorage.setItem("currentlyPreviewdProductId", id.toString());
    this.echoProductService.currentlyPreviewdProductId = id;
  }

  // Add to QuotationList
  public addToQuotationList(product: Product){
    this.quotationService.addToQuoteList(product);
  }

  updateUrl($event){}
  
}
