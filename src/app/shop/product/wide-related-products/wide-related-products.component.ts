import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { environment } from 'src/environments/environment';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { AppConstants } from '../../../core/utils/AppConstants';
import { Product } from 'src/app/shared/classes/product';

@Component({
  selector: 'wide-related-products',
  templateUrl: './wide-related-products.component.html',
  styleUrls: ['./wide-related-products.component.scss']
})
export class WideRelatedProductsComponent implements OnInit {

  public products: Product[] = [];
  loading = true;
  emptyMsg = ''
  @Input() productId: string;
  defaultProductImageUrl = AppConstants.DEFAULTPRODUCTIMAGEURL;
  url: string = environment.apiUrl;


  // Team Slick slider config
  public teamSliderConfig = {
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 586,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
    ]
  };

  public productSlideConfig: any = {
    infinite: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 2,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 420,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    ]
  };


  constructor(
    private echoProductsService: EchoProductsService,
  ) { }

  ngOnInit() {
    this.echoProductsService.pageRelatedItems(this.productId).subscribe(
      (result) => {
        this.loading = false
        this.products = result || []
        console.log("Related products ", this.products);
        if (this.products && this.products.length == 0) {
          this.emptyMsg = 'No Data'
        }
      });
  }

}
