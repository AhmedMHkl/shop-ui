import { Component, OnInit, Input } from '@angular/core';
import { ProductReview } from 'src/app/shared/classes/product-review';
import { ProductReviewService } from 'src/app/shared/services/product-review.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { BaseComponent } from 'src/app/shared/classes/base-component';

@Component({
  selector: 'product-review',
  templateUrl: './product-review.component.html',
  styleUrls: ['./product-review.component.scss']
})
export class ProductReviewComponent extends BaseComponent implements OnInit {

  @Input() productId: number;
  review: ProductReview = new ProductReview();
  productReviews: ProductReview[] = [];
  isSubmitted: boolean = false;

  constructor(
    private reviewService: ProductReviewService,
    private translate: TranslateService,
    private toastrService: ToastrService,
    private spinner: NgxSpinnerService,

  ) {
    super()
  }

  ngOnInit() {
    this.getCurrentCustomerId()
    this.listProductReviews()
  }

  addReview(form) {
    this.review.productId = this.productId
    this.review.customerId = Number(this.currentCustomerId)
    console.log(this.review);
    this.isSubmitted = true;
    if (!form.valid)
      return false;

    this.reviewService.productReview(this.review).subscribe(
      (res) => {
        this.listProductReviews()
        this.loadNewReviewEntity()
        this.showReviewForm = false
      },
      (error) => {
        console.log("you can not add review twice");
        let msg = this.translate.instant("You can not add review twice");
        this.toastrService.error(msg);
      });
  }

  showReviewForm = true;
  loadNewReviewEntity() {
    this.review = new ProductReview();
  }

  listProductReviews() {
    this.reviewService.getProductReview(this.productId).subscribe(res => {
      console.log("productReviews : ", res);
      this.productReviews = res || [];
      this.hasAReview(this.productReviews)
    })
  }

  customerHasAReview = false;
  hasAReview(reviewList: ProductReview[]) {
    let res = reviewList.find(rev => rev.customerId == Number(this.currentCustomerId))
    if (res == undefined)
      this.customerHasAReview = false
    else
      this.customerHasAReview = true
  }

  removeProductReview(review) {
    this.reviewService.removeReview(review).subscribe(
      (sucess) => {
        this.spinner.show()
        this.toastrService.success("deleted successfully", "Deleting Reiew")
        this.loadNewReviewEntity()
        this.listProductReviews()
        this.spinner.hide()
      },
      (error) => { }
    )
  }

  editProductReview(review) {
    this.review = review
    this.showReviewForm = true
  }

  updateReview(form) {
    console.log(this.review);
    if (!form.valid)
      return false;

    this.review.customerId = Number(this.currentCustomerId)
    this.reviewService.updateReview(this.review).subscribe(
      (sucess) => {
        this.spinner.show()
        this.toastrService.success("updated successfully", "Updating Reiew")
        this.listProductReviews()
        this.spinner.hide()
      },
      (error) => { }
    )
  }

  updateReviewProductRate($event) {
    console.log($event);
    this.review.rating = $event;
    console.log(this.review)

  }

}
