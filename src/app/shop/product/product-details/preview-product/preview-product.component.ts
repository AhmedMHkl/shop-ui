import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as $ from 'jquery';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/auth/auth.service';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { Option } from 'src/app/shared/classes/option';
import { Product } from 'src/app/shared/classes/product';
import { ProductReview } from 'src/app/shared/classes/product-review';
import { Product2 } from 'src/app/shared/classes/product2';
import { ProductReviewService } from 'src/app/shared/services/product-review.service';
import { CartService } from '../../../../shared/services/cart.service';
import { ProductsService } from '../../../../shared/services/products.service';
import { WishlistService } from '../../../../shared/services/wishlist.service';

@Component({
  selector: 'app-product-left-sidebar',
  templateUrl: './preview-product.component.html',
  styleUrls: ['./preview-product.component.scss']
})
export class PreviewProductComponent implements OnInit {


  public product: Product2;
  public products: Product[] = [];
  public counter: number = 1;
  public selectedSize: any = '';
  public selectedColor: any = '';
  public iStyle = "width:100%; height:100%;";
  public productSize: any[] = [];
  public productColor: any[] = [];

  public isLoggedIn: boolean = false;
  public review: ProductReview = <ProductReview>{};
  public isSubmitted: boolean = false;
  public productId: number;
  public productReviews: ProductReview[] = [];
  public productImages: any[];

  priceBeforeUpdate = 0;
  priceUpdated = false;
  productOPtionsArr: any[] = [];

  //Get Product By Id
  constructor(private route: ActivatedRoute, private router: Router,
    public productsService: ProductsService, private wishlistService: WishlistService,
    private toastrService: ToastrService,
    private translate: TranslateService,
    private cartService: CartService,
    private echoProductService: EchoProductsService,
    private reviewService: ProductReviewService,
    private authService: AuthService) {
    this.route.params.subscribe(params => {
      const id = +params['id'];
      this.review.productId = +params['id'];
      this.productId = +params['id'];
      //this.productsService.getProduct(id).subscribe(product => this.product = product);
      this.echoProductService.getProductById(id).subscribe(product => {
        this.product = product;
        if (typeof product.options != "undefined" && product.options != null && product.options.length > 0) {
          product.options.forEach((option: Option) => {
            if (option.code == "COLOR") {
              this.productColor = option.optionValues;
            } else if (option.code == "SIZE") {
              this.productSize = option.optionValues;
            }
          });
        }
        // set product discount value in percentage
        this.product.discountPercentage = ((this.product.originalPriceAmount - this.product.price) / product.originalPriceAmount) * 100
        console.log(product);
        console.log(this.productSize);
        console.log(this.productColor);
        // handel images
        // if (this.product.images != null) {
        //   this.product.images.forEach(img =>
        //     img.imageUrl = environment.apiUrl + img.imageUrl
        //   );
        //   console.log(this.product.images);
        // }
      });
    });
  }

  ngOnInit() {

    //this.productsService.getProducts().subscribe(product => this.products = product);
    this.review.rating = 0;
    let isLoggedinStr = localStorage.getItem("isLoggedin") || "";
    let loggedInUserId = localStorage.getItem("loggedInUserId") || "";
    if (isLoggedinStr == "yes") {
      this.isLoggedIn = true;
      this.review.customerId = +loggedInUserId;
    }

    this.reviewService.getProductReview(this.review.productId).subscribe(res => {
      console.log(res);
      this.productReviews = res;
    })

  }

  // product zoom 
  onMouseOver(): void {
    document.getElementById("p-zoom").classList.add('product-zoom');
  }

  onMouseOut(): void {
    document.getElementById("p-zoom").classList.remove('product-zoom');
  }

  public slideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
  };

  public slideNavConfig = {
    vertical: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.product-slick',
    arrows: false,
    dots: false,
    focusOnSelect: true
  }

  public increment() {
    this.counter += 1;
  }

  public decrement() {
    if (this.counter > 1) {
      this.counter -= 1;
    }
  }

  // For mobile filter view
  public mobileSidebar() {
    $('.collection-filter').css("left", "-15px");
  }

  // Add to cart
  public addToCart(product: Product, quantity) {
    if (quantity == 0) return false;
    let res = this.cartService.addToCart(product, parseInt(quantity));
    // >>>>>>> backend
    console.log("after add to cart :- ", res);
    const cartItem = { attributes: this.productOPtionsArr, product: product.id, quantity: quantity };
    const customerId = this.authService.loggedInUserId();
    //if(res){
    if (customerId !== null && customerId !== undefined && customerId !== "") {
      // add tocustomer cart
      this.echoProductService.addLoggedInCustomerProductToCart(customerId, cartItem).subscribe(res => {
        console.log(res)
        localStorage.setItem("cartCode", res.code);
        localStorage.setItem("cartId", res.id);
      });
    }
    else {
      // add to Anonymous cart
      this.echoProductService.addAnonymousProductToCart(cartItem).subscribe(res => {
        console.log(res)
        localStorage.setItem("cartCode", res.code);
        localStorage.setItem("cartId", res.id);
      });
    }
    //}
  }

  // Add to cart
  public buyNow(product: Product, quantity) {

    if (quantity > 0)
      this.router.navigate(['/home/checkout']);
  }

  // Add to wishlist
  public addToWishlist(product: Product) {
    this.wishlistService.addToWishlist(product);
  }


  // Change size variant
  public changeSizeVariant(variant) {
    this.selectedSize = variant;
  }
  public changeColorVariant(variant) {
    this.selectedColor = variant;
  }

  onSubmit(form) {
    this.isSubmitted = true;
    if (!form.valid)
      return false;
    console.log(this.review, form);
    this.reviewService.productReview(this.review).subscribe(res => {

      this.reviewService.getProductReview(this.productId).subscribe(res => {
        console.log(res);
        this.productReviews = res;
      });
    },
      error => {
        console.log("you can not add review twice");
        let msg = this.translate.instant("You can not add review twice");
        this.toastrService.error(msg);
      });
  }

  updateReviewProductRate($event) {
    console.log($event);
    this.review.rating = $event;
    console.log(this.review)

  }


  updateProductPrice(optionValue) {
    console.log(optionValue, this.product);
    if (!this.priceUpdated) {
      this.priceUpdated = true
      this.priceBeforeUpdate = Number(this.product.price)
    }
    else
      this.product.price = this.priceBeforeUpdate
    this.updateProductOPtionsArr(optionValue)
    this.product.price = Number(this.product.price) + this.calcProductOPtionsArrPrices()
    //console.log(this.product.price)
  }

  private updateProductOPtionsArr(opv) {
    let hasOptionv = this.productOPtionsArr.find(x => x.code === opv.code)
    let Optionv_sibling = this.productOPtionsArr.find(x => x.optionId == opv.optionId)
    if (Optionv_sibling) {
      const index = this.productOPtionsArr.indexOf(Optionv_sibling);
      this.productOPtionsArr.splice(index, 1);
    }
    if (!hasOptionv) {
      this.productOPtionsArr.push(opv)
    }
  }

  private calcProductOPtionsArrPrices(): number {
    let price = 0;
    this.productOPtionsArr.forEach(x => price = Number(price) + Number(x.price));
    return price;
  }

  //   public product            :   Product = {};
  //   public products           :   Product[] = [];
  //   public counter            :   number = 1; 
  //   public selectedSize       :   any = '';
  // public iStyle               = "width:100%; height:100%;"
  //   //Get Product By Id
  //   constructor(
  //     private route: ActivatedRoute, private router: Router,
  //     public productsService: ProductsService, private wishlistService: WishlistService,
  //     private cartService: CartService) {
  //       this.route.params.subscribe(params => {
  //         const id = +params['id'];
  //         this.productsService.getProduct(id).subscribe(product => this.product = product)
  //       });
  //   }

  //   ngOnInit() {
  //     this.productsService.getProducts().subscribe(product => this.products = product);
  //   }

  //   // product zoom 
  //   onMouseOver(): void {
  //     document.getElementById("p-zoom").classList.add('product-zoom');
  //   }

  //   onMouseOut(): void {
  //     document.getElementById("p-zoom").classList.remove('product-zoom');
  //   }

  //   public slideConfig = {
  //     slidesToShow: 1,
  //     slidesToScroll: 1,
  //     arrows: true,
  //     fade: true,
  //   };

  //   public slideNavConfig = {
  //     vertical: false,
  //     slidesToShow: 3,
  //     slidesToScroll: 1,
  //     asNavFor: '.product-slick',
  //     arrows: false,
  //     dots: false,
  //     focusOnSelect: true
  //   }

  //   public increment() { 
  //       this.counter += 1;
  //   }

  //   public decrement() {
  //       if(this.counter >1){
  //          this.counter -= 1;
  //       }
  //   }

  //   // For mobile filter view
  //   public mobileSidebar() {
  //     $('.collection-filter').css("left", "-15px");
  //   }

  //   // Add to cart
  //   public addToCart(product: Product, quantity) {
  //     if (quantity == 0) return false;
  //     this.cartService.addToCart(product, parseInt(quantity));
  //   }

  //   // Add to cart
  //   public buyNow(product: Product, quantity) {
  //      if (quantity > 0) 
  //        this.cartService.addToCart(product,parseInt(quantity));
  //        this.router.navigate(['/home/checkout']);
  //   }

  //   // Add to wishlist
  //   public addToWishlist(product: Product) {
  //      this.wishlistService.addToWishlist(product);
  //   }


  //   // Change size variant
  //   public changeSizeVariant(variant) {
  //      this.selectedSize = variant;
  //   }

}
