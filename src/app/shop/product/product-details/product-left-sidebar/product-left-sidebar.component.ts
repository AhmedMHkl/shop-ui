import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';
import * as _ from 'lodash';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/auth/auth.service';
import { AppConstants } from 'src/app/core/utils/AppConstants';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { BaseComponentWithCart } from 'src/app/shared/classes/base-component-with-cart';
import { Product } from 'src/app/shared/classes/product';
import { environment } from 'src/environments/environment';
import { CartService } from '../../../../shared/services/cart.service';
import { WishlistService } from '../../../../shared/services/wishlist.service';
import { ProductsService } from 'src/app/shared/services/products.service';
import { QuotationService } from 'src/app/shared/services/quotation.service';

@Component({
  selector: 'app-product-left-sidebar',
  templateUrl: './product-left-sidebar.component.html',
  styleUrls: ['./product-left-sidebar.component.scss']
})
export class ProductLeftSidebarComponent extends BaseComponentWithCart implements OnInit {

  currentLang = localStorage.getItem(AppConstants.CURRENT_LANGUAGE_KEY);
  product: Product;
  counter: number = 1;
  isLoggedIn: boolean = false;
  productId: number;
  priceBeforeUpdate = 0;
  priceUpdated = false;
  // productOPtionsArr: any[] = [];
  pdays = 0
  phours = 0
  pminutes = 0
  pseconds = 0
  productOfferEndDate;
  productDiscounted = false;
  productAttributes: any[] = [];

  resultStyle = ''
  images: any[] = []

  //Get Product By Id
  constructor(
    private route: ActivatedRoute,
    protected router: Router,
    protected wishlistService: WishlistService,
    protected cartService: CartService,
    public echoProductService: EchoProductsService,
    protected authService: AuthService,
    private spinner: NgxSpinnerService,
    protected productsService: ProductsService,
    protected quotationService: QuotationService,
  ) {
    super(wishlistService, echoProductService, cartService, authService, router, productsService, quotationService);
    this.route.params.subscribe(params => {
      this.productId = +params['id'];
      this.getProductById(this.productId)
    });
  }

  getProductById(id: any) {
    this.spinner.show();
    this.echoProductService.getProductById(id).subscribe(
      (product) => {
        this.product = product;
        console.log("product Data", this.product);
        if (typeof product.options != "undefined" && product.options != null && product.options.length > 0)
          this.productAttributes = product.options;
        this.product.discountPercentage = (((this.product.originalPriceAmount - this.product.price) / product.originalPriceAmount) * 100).toFixed(2)
        if (this.product.images != null) {
          this.product.images.forEach(
            (img) => {
              img.imageUrl = environment.apiUrl + img.imageUrl
              this.images.push({
                source: img.imageUrl,
                thumbnail: img.imageUrl,
                title: 'Sopranos 1'
              });
            }
          );
        }
        this.product.description = this.product.descriptions.filter(ele => ele.language == this.currentLang)[0];
        if (this.product.discounted) {
          let prc = this.product.prices.find(p => p.defaultPrice && p.discounted);
          if (prc) {
            this.productOfferEndDate = prc.productPriceSpecialEndDate
            this.productDiscounted = true
            this.countDown(prc.productPriceSpecialEndDate)
          }
        }

        this.spinner.hide();

      },
      (err) => {

      });
  }

  ngOnInit() {
    if (this.currentLang === 'ar')
      this.resultStyle = "background-repeat: no-repeat;z-index: 2; border: 1px solid red;width:110%;height:100%;position:absolute;background-size: 900px 900px;-webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);top: 0;right: 105%;"
    else
      this.resultStyle = "background-repeat: no-repeat;z-index: 2; border: 1px solid red;width:110%;height:100%;position:absolute;background-size: 900px 900px;-webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);top: 0;left: 105%;"
    this.getCurrentCustomerId()
    setInterval(() => {
      this.countDown(this.productOfferEndDate);
    }, 1000);
    setTimeout(() => {
      console.log("productAttributes : ", this.productAttributes);
    }, 2000);
    let isLoggedinStr = localStorage.getItem("isLoggedin") || "";
    let loggedInUserId = localStorage.getItem("loggedInUserId") || "";
    if (isLoggedinStr == "yes") {
      this.isLoggedIn = true;
    }
  }

  // product zoom 
  onMouseOver(): void {
    document.getElementById("p-zoom").classList.add('product-zoom');
  }

  onMouseOut(): void {
    document.getElementById("p-zoom").classList.remove('product-zoom');
  }

  public slideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
  };

  public slideNavConfig = {
    vertical: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.product-slick',
    arrows: false,
    dots: false,
    focusOnSelect: true
  }

  public increment() {
    this.counter += 1;
  }

  public decrement() {
    if (this.counter > 1) {
      this.counter -= 1;
    }
  }

  // For mobile filter view
  public mobileSidebar() {
    $('.collection-filter').css("left", "-15px");
  }

  countDown(productOfferEndDate: Date) {
    if (!this.productDiscounted)
      return false
    let countDownDate = new Date(productOfferEndDate).getTime();
    let now = new Date().getTime(); // Get today's date and time
    let distance = countDownDate - now; // Find the distance between now and the count down date
    this.pdays = Math.floor(distance / (1000 * 60 * 60 * 24));
    this.phours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    this.pminutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    this.pseconds = Math.floor((distance % (1000 * 60)) / 1000);
  }

  productAttrChanged(attr, event) {
    console.log("attr selected", attr, event);
    // validate attrs - update price
    let attrId = event
    let currentOptionAttrIds: any[] = attr.optionValues.map(x => x.attributeId)


    if (attr.type !== 'checkbox') {
      this.productOPtionsArr.forEach(ele => {
        if (currentOptionAttrIds.includes(ele.id)) {
          // decrement price
          let price = this.priceToIncrementOrDecrement(attr.optionValues, ele.id)
          this.product.price -= price
        }
      })
      let removedArr = _.remove(this.productOPtionsArr, function (n) {
        return currentOptionAttrIds.includes(n.id);
      });

      // push attr 
      let attrObj = { id: Number(attrId) }
      this.productOPtionsArr.push(attrObj)

      // increment price
      let price = this.priceToIncrementOrDecrement(attr.optionValues, attrId)
      this.product.price += price

    } else {
      if (this.productOPtionsArr.map(ele => ele.id).includes(Number(attrId))) {
        let price = this.priceToIncrementOrDecrement(attr.optionValues, Number(attrId))
        this.product.price -= price
        this.productOPtionsArr = this.productOPtionsArr.filter(x => x.id != attrId)
      } else {
        // push attr 
        let attrObj = { id: Number(attrId) }
        this.productOPtionsArr.push(attrObj)

        // increment price
        let price = this.priceToIncrementOrDecrement(attr.optionValues, attrId)
        this.product.price += price
      }
    }





  }

  priceToIncrementOrDecrement(optionValues: any[], attrId: any): number {
    let optValue = optionValues.find(x => x.attributeId == attrId);
    return (optValue && optValue.price) ? Number(optValue.price) : 0;

  }


}
