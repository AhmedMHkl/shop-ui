import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { environment } from 'src/environments/environment';
import { Product } from 'src/app/shared/classes/product';

@Component({
  selector: 'related-product',
  templateUrl: './related-product.component.html',
  styleUrls: ['./related-product.component.scss']
})
export class RelatedProductComponent implements OnInit {

  public products: Product[] = [];
  @Input() productId: string;
  @Output() relatedArray = new EventEmitter<number>();
  defaultImageUrl = "assets/images/fashion/product/1.jpg";
  url: string = environment.apiUrl;

  constructor(private echoProductsService: EchoProductsService) { }

  ngOnInit() {
    this.echoProductsService.pageRelatedItems(this.productId).subscribe(
      (product) => {
        this.products = product;
        if (product)
          this.relatedArray.emit(this.products.length)
        console.log("new products ", this.products);
      });
  }

  setProductId(id: any) {
    this.echoProductsService.pageRelatedItems(id).subscribe(product => {
      this.products = product;
      console.log("inner related products ", this.products);
    });
  }
}
