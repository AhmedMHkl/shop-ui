import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { CategoryService } from 'src/app/core/services/category.service';
import { CommonCoreService } from 'src/app/core/services/common-core.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  // allCategories : any;
  categoryList: any[] = [];
  constructor(
    private commonCoreService: CommonCoreService,
    private categoryService: CategoryService,
  ) { }

  // collapse toggle
  ngOnInit() {
    this.categoryList = this.categoryService.cachedCategories()
    this.categoryService.getCategoryHierarchy().subscribe(res => {
      this.categoryList = res
    })
  }

  // For mobile view
  public mobileFilterBack() {
    $('.collection-filter').css("left", "-365px");
  }

}
