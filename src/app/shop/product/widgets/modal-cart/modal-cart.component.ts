import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductsService } from '../../../../shared/services/products.service';
import { Product } from 'src/app/shared/classes/product';
declare var $: any;

@Component({
  selector: 'app-modal-cart',
  templateUrl: './modal-cart.component.html',
  styleUrls: ['./modal-cart.component.scss']
})
export class ModalCartComponent implements OnInit, OnDestroy {
  
  public products : Product[] = [];
 
  constructor(private productsService: ProductsService,) { }

  ngOnInit() {
  	this.productsService.products2().subscribe(product => { 
      this.products = product['products']; 
      
      this.products = this.products.filter(prod => prod['available'] && prod['productAvailabilities']);
  	});
  }

  ngOnDestroy() {
    $('.addTocartModal').modal('hide');
  }

  relatedProducts(pro) {
     var relatedItems = this.products.filter(function(products) {
        if(products.id != pro.id)
        return products.category == pro.category;
    });
    return relatedItems;   
  }

}
