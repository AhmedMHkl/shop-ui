import { animate, style, transition, trigger } from "@angular/animations";
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { EsSearchService } from 'src/app/services/es-search.service';
import { Product } from 'src/app/shared/classes/product';
import { ProductsService } from '../../../shared/services/products.service';
import { ProductSearchService } from "src/app/services/product-search.service";
import { Subject } from "rxjs";
import { debounceTime, distinctUntilChanged, mergeMap } from "rxjs/operators";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  animations: [  // angular animation
    trigger('Animation', [
      transition('* => fadeOut', [
        style({ opacity: 0.1 }),
        animate(1000, style({ opacity: 0.1 }))
      ]),
      transition('* => fadeIn', [
        style({ opacity: 0.1 }),
        animate(1000, style({ opacity: 0.1 }))
      ])
    ])
  ]
})
export class SearchComponent implements OnInit {

  public searchProducts: Product[] = [];
  public categoryName: any;
  public productName: string;
  public animation: any;
  public searchTerms: any = '';
  text: string;
  results: string[];
  loading = true;
  constructor(
    private productSearchService: ProductSearchService,
    private searchService: EsSearchService,
    private echoProductService: EchoProductsService,
    private route: ActivatedRoute,
  ) { }

  searchTextChanged = new Subject<string>();

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      this.loading = true;
      this.searchTerms = params['keyword'] || ''
      this.categoryName = params['categoryName'] || null
      this.productName = params['productName'] || ''
      if (this.searchTerms !== '' && this.searchTerms !== undefined) this.startSearch(this.searchTerms)
      if (this.productName !== '' && this.productName !== undefined) this.startSearch(this.productName)
      if (this.categoryName !== '' && this.categoryName !== undefined) this.startSearch(this.categoryName, 'category')
    })

    ///////////////////////////////////////////////////////

    this.searchTextChanged.pipe(
      debounceTime(1000)
    ).subscribe(searchTextValue => {
      console.log("searchTextValue", searchTextValue);
      if (searchTextValue !== null)
        this.startSearch(searchTextValue);
    });
  }

  /// start search
  public startSearch(term: string, searchType: string = 'product') {
    console.log("term : ", term);
    this.productSearchService.search(term, searchType).subscribe(
      (resp) => {
        console.log("search result : ", resp);
        this.searchProducts = resp
        this.loading = false;
      });
  }

  autoComplete(value) {
    this.searchTextChanged.next(value);
  }

}
