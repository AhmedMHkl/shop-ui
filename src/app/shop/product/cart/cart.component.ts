import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Observable, of } from 'rxjs';
import { EchoMessageService } from 'src/app/core/services/echo-message.service';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { Product } from 'src/app/shared/classes/product';
import { QuotationService } from 'src/app/shared/services/quotation.service';
import { WishlistService } from 'src/app/shared/services/wishlist.service';
import { environment } from 'src/environments/environment';
import { CartItem } from '../../../shared/classes/cart-item';
import { CartService } from '../../../shared/services/cart.service';
import { ProductsService } from '../../../shared/services/products.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  public cartItems: Observable<CartItem[]> = of([]);
  public shoppingCartItems: CartItem[] = [];
  defaultImageUrl = "assets/images/fashion/product/1.jpg";
  host = ''
  constructor(
    private productsService: ProductsService,
    private cartService: CartService,
    private echoProductService: EchoProductsService,
    private alert: EchoMessageService,
    private confirmationService: ConfirmationService,
    private translateService: TranslateService,
    private wishlistService: WishlistService,
    private quotationService: QuotationService

  ) { }

  ngOnInit() {
    this.host = environment.apiUrl
    this.cartItems = this.cartService.getItems();
    console.log(this.cartItems);
    this.cartItems.subscribe(shoppingCartItems => {
      this.shoppingCartItems = shoppingCartItems;
      console.log(shoppingCartItems);

    });

  }

  // Increase Product Quantity
  public increment(item: CartItem, quantity: number = 1) {
    this.cartService.updateCartQuantity(item.product, quantity);
    // increment in backend : just set cartItem->quantity = new quantity --- working with customers and anonymous
    const cartCode = localStorage.getItem("cartCode");
    const cartItem = {
      product: item.product.id,
      quantity: item.quantity
    };
    this.echoProductService.modifyCart(cartCode, cartItem).subscribe(res => {
      console.log(res)
    });
  }

  // Decrease Product Quantity
  public decrement(item: CartItem, quantity: number = -1) {
    this.cartService.updateCartQuantity(item.product, quantity);
    // decrement in backend : just set cartItem->quantity = new quantity  --- working with customers and anonymous
    const cartCode = localStorage.getItem("cartCode");
    const cartItem = {
      product: item.product.id,
      quantity: item.quantity
    };
    this.echoProductService.modifyCart(cartCode, cartItem).subscribe(res => {
      console.log(res)
    });
  }

  // Get Total
  public getTotal(): Observable<number> {
    return this.cartService.getTotalAmount();
  }

  // Remove cart items
  public removeItem(item: CartItem) {
    this.confirmationService.confirm({
      message: this.translateService.instant('Are you sure that you want to perform this action?'),
      acceptLabel: this.translateService.instant('yes'),
      rejectLabel: this.translateService.instant('no'),
      accept: () => {
        console.log(item);
        // remove from backend : just set cartItem->quantity = 0  --- working with customers and anonymous
        const cartCode = localStorage.getItem("cartCode");
        const cartItem = {
          product: item.product.id,
          quantity: 0
        };
        this.echoProductService.modifyCart(cartCode, cartItem).subscribe(
          (resp) => {
            console.log(resp);
            this.cartService.removeFromCart(item);
            this.alert.successMessage('common_deleted_succesfully', "Success", 'toast-top-right', 5000);
          },
          (err) => {
            console.log(err);
            this.alert.errorMessage("Error", 'common_error_accured', 'toast-top-right', 5000);
          });
      }
    });
  }

  public addToWishlist(product: Product) {
    this.wishlistService.addToWishlist(product);
  }

  public addToQuotList(product: Product){
    this.quotationService.addToQuoteList(product);
  }
}
