import { AppConstants } from 'src/app/core/utils/AppConstants';
import { CommonCoreService } from 'src/app/core/services/common-core.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PayPalConfig, PayPalEnvironment, PayPalIntegrationType } from 'ngx-paypal';
// import {  IPayPalConfig,  ICreateOrderRequest } from 'ngx-paypal';
import { CartItem } from '../../../shared/classes/cart-item';
import { ProductsService } from '../../../shared/services/products.service';
import { CartService } from '../../../shared/services/cart.service';
import { OrderService } from '../../../shared/services/order.service';
import { Observable, of } from 'rxjs';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  // form group
  public checkoutForm: FormGroup;
  public cartItems: Observable<CartItem[]> = of([]);
  public checkOutItems: CartItem[] = [];
  public orderDetails: any[] = [];
  public amount: number;
  public payPalConfig?: PayPalConfig;
  public token: string = null;
  public payerID: string = null;
  public payingInProgress: boolean = false;
  countries: any[];
  paymentType: any;

  // Form Validator
  constructor(
    private fb: FormBuilder,
    private cartService: CartService,
    public productsService: ProductsService,
    private orderService: OrderService,
    private route: ActivatedRoute,
    private router: Router,
    private commonCoreService: CommonCoreService,
    private customerService: CustomerService,

  ) {
    this.checkoutForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      lastName: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      phone: ['', [Validators.required, Validators.pattern('[0-9]+')]],
      address: ['', [Validators.required, Validators.maxLength(50)]],
      country: ['', Validators.required],
      city: ['', Validators.required],
      postalCode: ['', Validators.required]
    })
  }

  ngOnInit() {

    this.cartItems = this.cartService.getItems();
    this.cartItems.subscribe(products => this.checkOutItems = products);
    this.getTotal().subscribe(amount => this.amount = amount);
    this.route.queryParams.subscribe(params => {
      const token = params['token'];
      const payerID = params['PayerID'];
      setTimeout(() => {
        if (token && payerID) {
          this.payingInProgress = true;
          let shippingDetails = localStorage.getItem(AppConstants.SHIPPING_DETAILS);
          this.orderService.createOrder(this.checkOutItems, shippingDetails, token, this.amount);
        }
      }, 1000);


    });
    this.commonCoreService.getAllCountries().subscribe(res => {
      this.countries = res;
    });
    // this.initConfig();

    this.customerService.getCustomerInfo().subscribe(resp => {
      this.checkoutForm.patchValue(resp.billing)
    })
  }


  // Get sub Total
  public getTotal(): Observable<number> {
    return this.cartService.getTotalAmount();
  }

  // stripe payment gateway
  stripeCheckout() {
    var handler = (<any>window).StripeCheckout.configure({
      key: 'PUBLISHBLE_KEY', // publishble key
      locale: 'auto',
      token: (token: any) => {
        // You can access the token ID with `token.id`.
        // Get the token ID to your server-side code for use.
        //let shippingDetails = localStorage.getItem(AppConstants.SHIPPING_DETAILS);
        this.orderService.createOrder(this.checkOutItems, this.checkoutForm.value, token.id, this.amount);
      }
    });
    handler.open({
      name: 'Multikart',
      description: 'Online Fashion Store',
      amount: this.amount * 100
    })
  }

  // Paypal payment gateway
  private initConfig(): void {
    this.payPalConfig = new PayPalConfig(PayPalIntegrationType.ClientSideREST, PayPalEnvironment.Sandbox, {
      commit: true,
      client: {
        sandbox: 'AeeQGC2oOGWnGwxAjMk_EGMKYhWf232cBGGiQ6SIf1SB9DC28iIMi9Wag21oD9gTr6puWBhic5R3ueAI', // client Id
      },
      button: {
        label: 'paypal',
        size: 'small',    // small | medium | large | responsive
        shape: 'rect',     // pill | rect
        //color: 'blue',   // gold | blue | silver | black
        //tagline: false  
      },
      onPaymentComplete: (data, actions) => {
        console.log(actions);
        console.log(data);

        this.orderService.createOrder(this.checkOutItems, this.checkoutForm.value, data.orderID, this.amount);
      },
      onCancel: (data, actions) => {
        console.log('OnCancel');
      },
      onError: (err) => {
        console.log('OnError');
      },
      transactions: [{
        amount: {
          currency: this.productsService.currency,
          total: this.amount
        }
      }]
    });
  }

  checkoutPayment() {
    // console.log(this.checkoutForm, "init payment ...");
    // console.log(this.paymentType, "choose payment ...");

    if(this.paymentType == false){
      console.log(this.paymentType, "Stripe payment ...");
      this.stripePayment();
      
    }else if(this.paymentType == true){
        console.log(this.paymentType, "Paypal payment ...");
        const cartId = localStorage.getItem("cartId");
        this.orderService.generateToken(cartId).subscribe(res => {
        let response = JSON.parse(res);
        localStorage.setItem(AppConstants.SHIPPING_DETAILS, JSON.stringify(this.checkoutForm.value));
        JSON.stringify(this.checkoutForm.value)
        window.location.replace(response.response.url);
      });
    }
  
  }

  stripePayment(){
    this.orderService.stripePayment().subscribe(res =>{
      console.log(res);
      
    })
  }

}
