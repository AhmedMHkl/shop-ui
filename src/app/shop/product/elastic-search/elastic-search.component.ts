import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { EsSearchService } from 'src/app/services/es-search.service';
import { Product } from 'src/app/shared/classes/product';
import { ProductsService } from 'src/app/shared/services/products.service';

@Component({
  selector: 'app-elastic-search',
  templateUrl: './elastic-search.component.html',
  styleUrls: ['./elastic-search.component.scss'],
  animations: [  // angular animation
    trigger('Animation', [
      transition('* => fadeOut', [
        style({ opacity: 0.1 }),
        animate(1000, style({ opacity: 0.1 }))
      ]),
      transition('* => fadeIn', [
        style({ opacity: 0.1 }),
        animate(1000, style({ opacity: 0.1 }))
      ])
    ])
  ]
})
export class ElasticSearchComponent implements OnInit {
  public searchProducts: Product[] = [];
  public categoryCode: any;
  public productName: string;
  public animation: any;
  public searchTerms: any = '';
  text: string;
  results: string[];
  loading = true;
  constructor(
    private productsService: ProductsService,
    private searchService: EsSearchService,
    private echoProductService: EchoProductsService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    // this.productsService.getProducts().subscribe(product => this.products = product);
    this.route.queryParams.subscribe(params => {
      this.loading = true;
      this.searchTerms = params['keyword'] || ''
      this.categoryCode = params['categoryCode'] || null
      this.productName = params['productName'] || ''
      console.log("search products", this.categoryCode);
      console.log("search products", this.productName);
      // this.getProductByCategoryIds();
      if (this.searchTerms !== '' && this.searchTerms !== undefined) this.startSearch(this.searchTerms)
      if (this.productName !== '' && this.productName !== undefined) this.startSearch(this.productName)
      if (this.categoryCode !== '' && this.categoryCode !== undefined) this.startSearch(this.categoryCode)
    })
  }

  /// start search
  public startSearch(term: string, keys: string = 'name') {
    this.searchService.productSearch(term).subscribe(resp => {
      console.log("ES result : ", resp);
      this.searchProducts = resp.products
      this.loading = false;
    });
  }

  // autoComplete
  autoComplete(event) {
    console.log("event", event);
    this.searchService.autoComplete(event.query).subscribe(
      (res: any) => {
        console.log("auto complete resukt : ", res);
        this.results = res.values
      }
    )
  }


}
