import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { Product } from 'src/app/shared/classes/product';
import { QuotationService } from 'src/app/shared/services/quotation.service';
import { WishlistService } from 'src/app/shared/services/wishlist.service';
import { environment } from 'src/environments/environment';
import { CartService } from '../../../shared/services/cart.service';
import { ProductsService } from '../../../shared/services/products.service';

@Component({
  selector: 'app-product-compare',
  templateUrl: './product-compare.component.html',
  styleUrls: ['./product-compare.component.scss']
})
export class ProductCompareComponent implements OnInit {

  public product: Observable<Product[]> = of([]);
  public products: Product[] = [];
  public Relatedproducts: Product[] = [];
  host = ''
  currentImg = ''

  constructor(private productsService: ProductsService,
    private wishlistService: WishlistService,
    private cartService: CartService,
    private quotationService: QuotationService,
    private echoProductsService: EchoProductsService) { }

  ngOnInit() {
    this.host = environment.apiUrl
    this.product = this.productsService.getComapreProducts();
    this.product.subscribe(
      (products) => {
        this.products = products
        console.log(this.products);
        this.AllProductsRealatedItems();
      }
    );
  }

  public productSlideConfig: any = {
    infinite: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 2,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 420,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    ]
  };


  // Add to cart
  public addToCart(product: Product, quantity: number = 1) {
    this.cartService.addToCart(product, quantity);
  }

  // Remove from compare list
  public removeItem(product: Product) {
    this.productsService.removeFromCompare(product);
  }

  addToWishList(product: Product) {
    this.wishlistService.addToWishlist(product);
  }

  public addToQuotationList(product: Product) {
    this.quotationService.addToQuoteList(product);
  }


  public AllProductsRealatedItems() {
    let ids = this.products.map(p => p.id)
    console.log("component ids ", ids);
    this.echoProductsService.AllProductsRelatedItems(ids).
      subscribe(res => {
        if (res) {
          this.Relatedproducts = res;
          console.log("RelatedItem", this.Relatedproducts)
        }
      });
  }
}
