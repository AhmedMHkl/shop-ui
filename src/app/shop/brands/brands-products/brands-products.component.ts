
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { trigger, transition, style, animate } from "@angular/animations";
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { Product2, ColorFilter, SizeFilter } from 'src/app/shared/classes/product2';
import { ProductsService } from 'src/app/shared/services/products.service';
import { EsSearchService } from 'src/app/services/es-search.service';
import { environment } from 'src/environments/environment';
import * as $ from 'jquery';
import { Product } from 'src/app/shared/classes/product';
import { CartService } from 'src/app/shared/services/cart.service';
import { WishlistService } from 'src/app/shared/services/wishlist.service';

@Component({
  selector: 'app-brands-products',
  templateUrl: './brands-products.component.html',
  styleUrls: ['./brands-products.component.scss'],


  animations: [  // angular animation
    trigger('Animation', [
      transition('* => fadeOut', [
        style({ opacity: 0.1 }),
        animate(1000, style({ opacity: 0.1 }))
      ]),
      transition('* => fadeIn', [
        style({ opacity: 0.1 }),
        animate(1000, style({ opacity: 0.1 }))
      ])
    ])
  ]
})
export class BrandsProductsComponent implements OnInit {

  public products: Product2[] = [];
  manufacturerId: number;
  documents: any[] = [];
  url: string = environment.apiUrl;
  public filteredProducts: Product2[] = [];
  public animation: any;   // Animation
  public sortByOrder: string = '';   // sorting
  defaultImageUrl = "assets/images/fashion/product/1.jpg";
  manufacturer: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private productsService: ProductsService,
    private esSearchService: EsSearchService,
    private cartService: CartService,
    private wishlistService: WishlistService
  ) { }

  ngOnInit() {
    this.productSearchByManufacturerId();
    this.getManufacturerById();
  }

  productSearchByManufacturerId() {
    this.route.params.subscribe(params => {
      //params['catId']
      this.manufacturerId = params['catId'];
      console.log("category cat", params['catId']);
      this.esSearchService.productSearchByManufacturerId(params['catId']).subscribe(
        (res) => {
          this.documents = res.products;
         
        });
    })
  }

  getManufacturerById() {
    this.route.params.subscribe(params => {
      //params['catId']
      this.manufacturerId = params['catId'];
      this.productsService.getManufacturerById(params['catId']).subscribe(
        (res) => {
          if (res) {
            this.manufacturer = res
          }
        });
    })
  }



  public twoCol() {
    if ($('.product-wrapper-grid').hasClass("list-view")) { } else {
      $(".product-wrapper-grid").children().children().children().removeClass();
      $(".product-wrapper-grid").children().children().children().addClass("col-lg-6");
    }
  }

  public threeCol() {
    if ($('.product-wrapper-grid').hasClass("list-view")) { } else {
      $(".product-wrapper-grid").children().children().children().removeClass();
      $(".product-wrapper-grid").children().children().children().addClass("col-lg-4");
    }
  }

  public fourCol() {
    if ($('.product-wrapper-grid').hasClass("list-view")) { } else {
      $(".product-wrapper-grid").children().children().children().removeClass();
      $(".product-wrapper-grid").children().children().children().addClass("col-lg-3");
    }
  }

  public sixCol() {
    if ($('.product-wrapper-grid').hasClass("list-view")) { } else {
      $(".product-wrapper-grid").children().children().children().removeClass();
      $(".product-wrapper-grid").children().children().children().addClass("col-lg-2");
    }
  }

  public addToCart(product: Product, quantity: number = 1) {
    this.cartService.addToCart(product, quantity);
  }

  // Add to compare
  public addToCompare(product: Product) {
    this.productsService.addToCompare(product);
  }

  // Add to wishlist
  public addToWishlist(product: Product) {
    this.wishlistService.addToWishlist(product);
  }

  public onChangeSorting(val) {
    this.sortByOrder = val;
    this.animation == 'fadeOut' ? this.fadeIn() : this.fadeOut(); // animation
  }
  public fadeIn() {
    this.animation = 'fadeIn';
  }

  // Animation Effect fadeOut
  public fadeOut() {
    this.animation = 'fadeOut';
  }

}
