import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/shared/services/products.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'brand-testmonials',
  templateUrl: './brand-testmonials.component.html',
  styleUrls: ['./brand-testmonials.component.scss']
})
export class BrandTestmonialsComponent implements OnInit {

  constructor(private productsService: ProductsService) { }
  data: any[] = [];
  DisplayedData: any[] = [];
  url = environment.apiUrl;

  ngOnInit() {
    this.productsService.getManufacturer().subscribe(res => {
      console.log("getManufacturer", res)
      if (res) { this.data = res; }
      for (let i of this.data) {
        if (i.image != "" && i.image!=null) {
          console.log("image", i.image)
          this.DisplayedData.push(i)
          i.image = this.url + "/" + i.image;
        }
      }
      console.log("DisplayedData", this.DisplayedData)

    });
  }

  public sliderConfig = {
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 3,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 586,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
    ]
  };

}
