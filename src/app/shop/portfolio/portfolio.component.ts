import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { environment } from 'src/environments/environment';
import { CommonCoreService } from 'src/app/core/services/common-core.service';
import { CategoryService } from 'src/app/core/services/category.service';
declare var $: any;

@Component({
    selector: 'app-portfolio',
    templateUrl: './portfolio.component.html',
    styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit, AfterViewInit {

    host = ''
    allCategoriesGalary: string[] = [];
    allCategoriesGalary2 = ['shoes', 'bags', 'watches']
    allImages: any[] = [];

    @ViewChild('gallery', { static: true }) galleryElement: ElementRef;

    constructor(
        private echoProductService: EchoProductsService,
        private categoryService: CategoryService
    ) {
        // $.getScript('assets/js/portfolio.js');
        this.categoryService.getCategoryHierarchy().subscribe(res => {
            console.log("getCategoryHierarchy", res);
            this.allCategoriesGalary = res
        })
    }

    ngOnInit() {
        this.host = environment.apiUrl
        this.getCategories();

    }

    getCategories() {
        this.echoProductService.getAllProductsLight().subscribe(
            (resp) => {
                this.allImages = resp;
                console.log("allImages : ", resp);
                let x1: any[] = []
                resp.forEach(x2 => x2.categories.split(',').forEach(x3 => x1.push(x3)));
                this.allCategoriesGalary2 = x1.filter((v, i) => x1.indexOf(v) === i).filter(e => e !== "")
                // $.getScript('assets/js/portfolio.js');
            });
    }

    filterByCategory(cat) {
        console.log(cat);
        
    }

    ngAfterViewInit() {
        console.log(this.galleryElement.nativeElement);
        $(this.galleryElement.nativeElement).magnificPopup({
            delegate: 'a',
            type: 'image',
            closeOnContentClick: false,
            closeBtnInside: false,
            mainClass: 'mfp-with-zoom mfp-img-mobile',
            image: {
                verticalFit: true,
                titleSrc: function (item) {
                    return item.el.attr('title') + ' &middot;';
                }
            },
            gallery: {
                enabled: true
            },
            zoom: {
                enabled: true,
                duration: 300, // don't foget to change the duration also in CSS
                opener: function (element) {
                    return element.find('img');
                }
            }
        });
        // $.getScript('assets/js/portfolio.js');
        $(document).ready(function(){
            $(".bg-top" ).parent().addClass('b-top');
            $(".bg-bottom" ).parent().addClass('b-bottom');
            $(".bg-center" ).parent().addClass('b-center');
            $(".bg_size_content").parent().addClass('b_size_content');
            $(".img-to-bg" ).parent().addClass('bg-size');
        
            $(".filter-button").click(function(){
                $(this).addClass('active').siblings('.active').removeClass('active');
                var value = $(this).attr('data-filter');
                if(value == "all")
                {
                    $('.filter').show('1000');
                }
                else
                {
                    $(".filter").not('.'+value).hide('3000');
                    $('.filter').filter('.'+value).show('3000');
                }
            });
            
        });
    }

}
