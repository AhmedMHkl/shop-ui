import { PortfolioComponent } from './portfolio/portfolio.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShopRoutingModule } from './shop-routing.module';
import { SharedModule } from '../shared/shared.module';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { BarRatingModule } from "ngx-bar-rating";
import { RangeSliderModule } from 'ngx-rangeslider-component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgxPayPalModule } from 'ngx-paypal';
import { NgxImgZoomModule } from 'ngx-img-zoom';

// Home-six components
import { HomeComponent } from './home/home.component';
import { CollectionBannerSixComponent } from './home/collection-banner/collection-banner.component';
import { ProductTabComponent } from './home/product-tab/product-tab.component';
import { ParallaxBannerComponent } from './home/parallax-banner/parallax-banner.component';
import { BlogComponent } from './home/blog/blog.component';

// Products Components 
import { ProductComponent } from './product/product.component';
import { ProductBoxComponent } from './product/product-box/product-box.component';
import { ProductBoxHoverComponent } from './product/product-box-hover/product-box-hover.component';
import { ProductBoxVerticalComponent } from './product/product-box-vertical/product-box-vertical.component';
import { ProductBoxMetroComponent } from './product/product-box-metro/product-box-metro.component';
import { CollectionLeftSidebarComponent } from './product/collection/collection/collection.component';
import { ColorComponent } from './product/collection/filter/color/color.component';
import { BrandComponent } from './product/collection/filter/brand/brand.component';
import { PriceComponent } from './product/collection/filter/price/price.component';
import { ProductLeftSidebarComponent } from './product/product-details/product-left-sidebar/product-left-sidebar.component';
import { SidebarComponent } from './product/product-details/sidebar/sidebar.component';
import { CategoriesComponent } from './product/widgets/categories/categories.component';
import { QuickViewComponent } from './product/widgets/quick-view/quick-view.component';
import { ModalCartComponent } from './product/widgets/modal-cart/modal-cart.component';
import { SearchComponent } from './product/search/search.component';
import { ProductCompareComponent } from './product/product-compare/product-compare.component';
import { WishlistComponent } from './product/wishlist/wishlist.component';
import { CartComponent } from './product/cart/cart.component';
import { CheckoutComponent } from './product/checkout/checkout.component';
import { SuccessComponent } from './product/success/success.component';
import { ExitPopupComponent } from './product/widgets/exit-popup/exit-popup.component';
import { AgeVerificationComponent } from './product/widgets/age-verification/age-verification.component';
import { NewsletterComponent } from './product/widgets/newsletter/newsletter.component';
import { SliderComponent } from './home/slider/slider.component';
import { SizeComponent } from './product/collection/filter/size/size.component';
import { GetTheAppComponent } from './get-the-app/get-the-app.component';
import { MainViewComponent } from './home/main-view/main-view.component';
import { FeaturedProductComponent } from './home/featured-product/featured-product.component';
import { QuotationSubscribeComponent } from './home/quotation-subscribe/quotation-subscribe.component';
import { ProductsOffersComponent } from './home/products-offers/products-offers.component';
import { ShopHomeComponent } from './home/shop-home/shop-home.component';
import { PreviewProductComponent } from './product/product-details/preview-product/preview-product.component';
import { RelatedProductComponent } from './product/widgets/new-product/related-product.component';
import { BrandsProductsComponent } from './brands/brands-products/brands-products.component';
import { BrandTestmonialsComponent } from './brands/brand-testmonials/brand-testmonials.component';
import { GeneralProductFilterComponent } from './product/collection/filter/general-product-filter/general-product-filter.component';
import { ProductReviewComponent } from './product/product-details/product-review/product-review.component';
import { WideRelatedProductsComponent } from './product/wide-related-products/wide-related-products.component';
import { ProductGridComponent } from './home/product-grid/product-grid.component';
import { ProductListComponent } from './home/product-list/product-list.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { ElasticSearchComponent } from './product/elastic-search/elastic-search.component';

@NgModule({
  exports: [
    ExitPopupComponent,
    ProductBoxHoverComponent,

  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ShopRoutingModule,
    SharedModule,
    SlickCarouselModule,
    BarRatingModule,
    RangeSliderModule,
    InfiniteScrollModule,
    NgxPayPalModule,
    NgxImgZoomModule,
    ScrollingModule,

  ],
  declarations: [
    // Home Six
    HomeComponent,
    SliderComponent,
    CollectionBannerSixComponent,
    ProductTabComponent,
    ParallaxBannerComponent,
    BlogComponent,
    // Product
    ProductComponent,
    ProductBoxComponent,
    ProductBoxHoverComponent,
    ProductBoxVerticalComponent,
    ProductBoxMetroComponent,
    CollectionLeftSidebarComponent,
    ColorComponent,
    BrandComponent,
    PriceComponent,
    ProductLeftSidebarComponent,
    SidebarComponent,
    CategoriesComponent,
    QuickViewComponent,
    ModalCartComponent,
    RelatedProductComponent,
    SearchComponent,
    ProductCompareComponent,
    WishlistComponent,
    CartComponent,
    CheckoutComponent,
    SuccessComponent,
    ExitPopupComponent,
    AgeVerificationComponent,
    NewsletterComponent,
    PortfolioComponent,
    SizeComponent,
    GetTheAppComponent,
    MainViewComponent,
    FeaturedProductComponent,
    QuotationSubscribeComponent,
    ProductsOffersComponent,
    ShopHomeComponent,
    PreviewProductComponent,
    BrandsProductsComponent,
    BrandTestmonialsComponent,
    GeneralProductFilterComponent,
    ProductReviewComponent,
    WideRelatedProductsComponent,
    ProductGridComponent,
    ProductListComponent,
    ElasticSearchComponent
  ]
})
export class ShopModule { }
