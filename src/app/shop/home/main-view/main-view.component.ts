import { Component, Input, OnInit } from '@angular/core';
import { CommonCoreService } from 'src/app/core/services/common-core.service';
import { Product } from 'src/app/shared/classes/product';
import { environment } from 'src/environments/environment';
import { CategoryService } from 'src/app/core/services/category.service';

@Component({
  selector: 'main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.scss']
})
export class MainViewComponent implements OnInit {

  @Input() products: Product;
  allCategories: any;
  public variantImage: any = '';
  public selectedItem: any = '';
  defaultImageUrl = "assets/images/fashion/product/1.jpg";
  projectUrl = environment.apiUrl;
  constructor(private categoryService: CategoryService) { }

  // collapse toggle
  ngOnInit() {
    this.categoryService.getAllCategories().subscribe(
      cats => {
        this.allCategories = cats;
        // console.log("all cats - from main view : ", cats);
      },
      error => {
        console.log(error);
      });

    this.categoryService.categoriesCountByProduct().subscribe(res => {
      // console.log("categoriesCountByProduct : ", res);

    })
  }

  public sliderConfig = {
    infinite: true,
    autoplay: true,
    speed: 300,
  };

}
