import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BaseComponentWithCart } from 'src/app/shared/classes/base-component-with-cart';
import { Router } from '@angular/router';
import { WishlistService } from 'src/app/shared/services/wishlist.service';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { CartService } from 'src/app/shared/services/cart.service';
import { AuthService } from 'src/app/auth/auth.service';
import { ProductsService } from 'src/app/shared/services/products.service';
import { QuotationService } from 'src/app/shared/services/quotation.service';

@Component({
  selector: 'product-list-view',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent extends BaseComponentWithCart implements OnInit {

  @Input() products: any[] = [];
  host = ''

  constructor(
    protected router: Router,
    protected wishlistService: WishlistService,
    protected cartService: CartService,
    public echoProductService: EchoProductsService,
    protected authService: AuthService,
    protected productsService: ProductsService,
    protected quotationService: QuotationService,
  ) {
    super(
      wishlistService,
      echoProductService,
      cartService,
      authService,
      router,
      productsService,
      quotationService
    );

  }

  ngOnInit() {
    this.host = environment.apiUrl;
    console.log("list view products : ", this.products);

  }
  updateProductRate(product, value) {
    console.log(product, value);
    product.rating = value;
    this.echoProductService.updateProduct(product).subscribe(res => {
      console.log(res);
    });
  }
}
