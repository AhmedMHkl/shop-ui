import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/classes/product';

@Component({
  selector: 'products-offers',
  templateUrl: './products-offers.component.html',
  styleUrls: ['./products-offers.component.scss']
})
export class ProductsOffersComponent implements OnInit {

  @Input() products: Product[] = [];
  constructor() { }

  ngOnInit() {
  }

}
