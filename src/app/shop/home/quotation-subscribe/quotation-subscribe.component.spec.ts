import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotationSubscribeComponent } from './quotation-subscribe.component';

describe('QuotationSubscribeComponent', () => {
  let component: QuotationSubscribeComponent;
  let fixture: ComponentFixture<QuotationSubscribeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuotationSubscribeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotationSubscribeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
