import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/shared/services/store.service';
import { EchoMessageService } from 'src/app/core/services/echo-message.service';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { OptinModel } from 'src/app/shared/model/optin-model';

@Component({
  selector: 'quotation-subscribe',
  templateUrl: './quotation-subscribe.component.html',
  styleUrls: ['./quotation-subscribe.component.scss']
})
export class QuotationSubscribeComponent implements OnInit {

  optin: OptinModel = new OptinModel();
  constructor(
    private echoProductsService: EchoProductsService,
    private alerMessage: EchoMessageService,
    private storeService: StoreService,


  ) { }

  ngOnInit() {
    this.optin.firstName = 'mohammed';
    this.optin.lastName = 'elhusary';
  }

  onSubmit() {
    this.echoProductsService.createNewsLetter(this.optin).subscribe(
      res => {
        console.log("mmmmmmmmmmmmmmmmmmm", res);
        this.alerMessage.successMessage("Subscribeed Done", "Subscribe");
      }, err => {
        console.log("errrrrrrrrrrrrrrrrror", err);
      }
    );
  }

  handleFileInput($event){}
}
