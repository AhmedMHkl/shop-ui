import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'app-product-tab',
	templateUrl: './product-tab.component.html',
	styleUrls: ['./product-tab.component.scss']
})
export class ProductTabComponent implements OnInit {

	@Input() visibleCategoryProducts: any[] = [];

	constructor() { }

	ngOnInit() {
		
	}

	// Slick slider config
	public categorySlideConfig: any = {
		infinite: true,
		speed: 300,
		slidesToShow: 5,
		slidesToScroll: 2,
		autoplay: true,
		autoplaySpeed: 3000,
		responsive: [{
		  breakpoint: 1200,
		  settings: {
			slidesToShow: 3,
			slidesToScroll: 3
		  }
		},
		{
		  breakpoint: 991,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 2
		  }
		},
		{
		  breakpoint: 420,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
		]
	  };

}
