import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as $ from 'jquery';
import { AppConstants } from 'src/app/core/utils/AppConstants';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { Product } from 'src/app/shared/classes/product';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {

  public products: Product[] = [];

  entityList: any[] = [];
  visibleCategoryProducts: any[] = [];
  filtereVisibleCategoryProducts: any[] = [];
  productInSale: any[] = [];
  visableCategory: any[] = [];
  entity: any;
  viewType = 'list'
  totalCount: number = -1;
  start: number = 0;
  pageSize: number = AppConstants.PAGE_SIZE;
  loading: boolean = true;
  productName: string = null;
  throttle = 500;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = '';
  newList: any[] = [];
  

  constructor(private echoProductsService: EchoProductsService, ) { }

  ngOnInit() {
    //this.getProductsByCategory()
    this.gridView('list')
    this.listProductsInsale(10);
  }

  onScrollDown(ev) {
    // add another 10 items
    this.start = this.start + 1;
    this.getAllProducts(this.start * this.pageSize, this.pageSize, this.productName)
    this.direction = 'down'
  }


  getAllProducts(start, pageSize, productName) {
    if (this.entityList.length > 0) {
      this.loading = true;
    }
    //this.echoProductsService.getAllProducts(start, pageSize).subscribe(
    this.echoProductsService.listLightProducts(start, pageSize).subscribe(
      (products) => {
        console.log(this.entityList.length)
        if (this.entityList.length > 0) {
          this.newList = products;
          console.log(this.entityList)
          this.entityList = this.entityList.concat(this.newList);
        } else {
          this.entityList = products
        }
        //this.totalCount = products.totalCount
        if (this.totalCount === -1)
          this.countAllProducts();
        this.loading = false;
      });
  }

  countAllProducts() {
    this.echoProductsService.countLightProduct().subscribe(
      (count) => {
        this.totalCount = count;
      })
  }

  sortByName() {
   // this.productName = ''
   this.entityList.sort((a, b) => a.name.localeCompare(b.name))
    console.log("name", this.entityList)
  }

  getProductsByCategory() {
    this.echoProductsService.visibleCategoryProducts().subscribe(res => {
      this.visibleCategoryProducts = res
      this.visibleCategoryProducts.forEach((item) => {
        if (item.products.length > 0)
          this.filtereVisibleCategoryProducts.push(item)
      })
    })
  }

  listProductsInsale(count: number) {
    this.echoProductsService.listRandomProductsInsale(count).subscribe(res => {
      this.productInSale = res;
      console.log("Product In Sale: ", this.productInSale);

    })
  }

  adjustCss(className) {
    $(".btn").removeClass("active")
    $('.' + className).addClass('active')
  }

  listView(className) {
    this.adjustCss(className)
    if (this.entityList.length == 0)
      this.getAllProducts(this.start, this.pageSize, this.productName)
    this.viewType = 'list'
  }

  gridView(className) {
    this.adjustCss(className)
    if (this.entityList.length == 0)
      this.getAllProducts(this.start, this.pageSize, this.productName)
    this.viewType = 'grid'
  }

  byCategoryView(className) {
    this.adjustCss(className)
    if (this.visibleCategoryProducts.length == 0)
      this.getProductsByCategory()
    this.viewType = 'byCat'
  }

  // Slick slider config
  public productSlideConfig: any = {
    infinite: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 2,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 420,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    ]
  };

  sortByPrice(){
    console.log("sort")

   this.entityList.sort((a, b) => a.price - b.price)
    console.log("sort",this.entityList)
  }

}
