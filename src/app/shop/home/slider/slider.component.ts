import { Component, OnInit } from '@angular/core';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  constructor(
    private echoProductService: EchoProductsService,
  ) { }

  sliderProducts: any[];
  bg_img = "/assets/images/home-banner/12.jpg"
  img_prefix: any;

  ngOnInit() {
    this.img_prefix = environment.apiUrl

    this.echoProductService.getRandomProducts(5).subscribe(res => {
      console.log("slider ::::>>", res);
      res.forEach(p => {
        console.log(p.images[0].imageUrl);
        
      });
      this.sliderProducts = res
    });
  }

  // Slick slider config
  public sliderConfig: any = {
    autoplay: true,
    autoplaySpeed: 3000
  };

}
