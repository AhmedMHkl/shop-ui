import { Component, OnInit } from '@angular/core';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-collection-banner-six',
  templateUrl: './collection-banner.component.html',
  styleUrls: ['./collection-banner.component.scss']
})
export class CollectionBannerSixComponent implements OnInit {

  host = ''
  saleProducts: any[] = [];

  constructor(
    private echoProductsService: EchoProductsService,

  ) { }

  ngOnInit() {

    this.host = environment.apiUrl

    this.echoProductsService.listRandomProductsInsale(3).subscribe(res => {
      console.log("Sale Products ::::: ", res);

      this.saleProducts = res;

    });
  }

  // Collection banner
  public category = [{
    image: 'assets/images/furniture/2banner1.jpg',
    save: 'save 50%',
    title: 'Sofa',
    link: '/home/collection/furniture'
  }, {
    image: 'assets/images/furniture/2banner2.jpg',
    save: 'save 50%',
    title: 'Bean Bag',
    link: '/home/collection/furniture'
  }, {
    image: 'assets/images/furniture/2banner3.jpg',
    save: 'save 50%',
    title: 'Chair',
    link: '/home/collection/furniture'
  }]

}
