import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'product-grid-view',
  templateUrl: './product-grid.component.html',
  styleUrls: ['./product-grid.component.scss']
})
export class ProductGridComponent implements OnInit {

  @Input() products: any[] = [];

  constructor() { }

  ngOnInit() {
  }

}
