import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/classes/product';

@Component({
  selector: 'featured-product',
  templateUrl: './featured-product.component.html',
  styleUrls: ['./featured-product.component.scss']
})
export class FeaturedProductComponent implements OnInit {
  @Input() products: Product;
  constructor() { }

  ngOnInit() {
  }

}
