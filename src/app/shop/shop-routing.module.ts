import { PortfolioComponent } from './portfolio/portfolio.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CollectionLeftSidebarComponent } from './product/collection/collection/collection.component';
import { ProductLeftSidebarComponent } from './product/product-details/product-left-sidebar/product-left-sidebar.component';
import { SearchComponent } from './product/search/search.component';
import { WishlistComponent } from './product/wishlist/wishlist.component';
import { ProductCompareComponent } from './product/product-compare/product-compare.component';
import { CartComponent } from './product/cart/cart.component';
import { CheckoutComponent } from './product/checkout/checkout.component';
import { SuccessComponent } from './product/success/success.component';
import { HomeComponent } from './home/home.component';
import { GetTheAppComponent } from './get-the-app/get-the-app.component';
import { ShopHomeComponent } from './home/shop-home/shop-home.component';
import { PreviewProductComponent } from './product/product-details/preview-product/preview-product.component';
import { BrandsProductsComponent } from './brands/brands-products/brands-products.component';
import { ElasticSearchComponent } from './product/elastic-search/elastic-search.component';


// Routes
const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  }, {
    path: 'new-home',
    component: ShopHomeComponent
  },
  {
    path: 'collection/:catId',
    component: CollectionLeftSidebarComponent
  },
  {
    path: 'brand/:catId',
    component: BrandsProductsComponent
  },
  {
    path: 'product/:id',
    component: ProductLeftSidebarComponent
  }
  ,
  {
    path: 'preview-product/:id',
    component: PreviewProductComponent
  },
  {
    path: 'search',
    component: SearchComponent
  },
  {
    path: 'es-search',
    component: ElasticSearchComponent
  },
  {
    path: 'wishlist',
    component: WishlistComponent
  },
  {
    path: 'compare',
    component: ProductCompareComponent
  },
  {
    path: 'cart',
    component: CartComponent
  },
  {
    path: 'checkout',
    component: CheckoutComponent
  },
  {
    path: 'checkout/success',
    component: SuccessComponent
  },
  {
    path: 'portfolio',
    component: PortfolioComponent
  }
  ,
  {
    path: 'get_the_app',
    component: GetTheAppComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopRoutingModule { }
