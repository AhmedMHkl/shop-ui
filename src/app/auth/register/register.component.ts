import { AppConstants } from 'src/app/core/utils/AppConstants';
import { AuthenticationService } from './../../core/services/authentication.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { CommonCoreService } from 'src/app/core/services/common-core.service';
import { Router } from '@angular/router';
import { EchoMessageService } from 'src/app/core/services/echo-message.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  register: FormGroup;
  submitted: boolean = false;
  countries: any[];
  zones: any[] = [];
  constructor(
    private commonService: CommonCoreService,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private alert: EchoMessageService,
    private router: Router,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.commonService.getAllCountries().subscribe(countries => {
      console.log(countries);
      this.countries = countries;
    });

    this.register = this.formBuilder.group({
      billing: this.formBuilder.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.compose([
          Validators.required,
          //Validators.pattern(/^[a-zA-Z0-9_-]*$/)
        ])],
        country: ['', Validators.required],
        stateProvince: ['', Validators.required]
      }),
      emailAddress: ['', Validators.compose([Validators.required, Validators.email])],
      userName: [''],
      clearPassword: ['', Validators.required],
      checkPassword: ['', Validators.required],
      privacy: [false, Validators.required],
      subscribeInNewsLetter: [false]

    }, { validator: this.checkIfMatchingPasswords('clearPassword', 'checkPassword') });
  }

  get regControls() { return this.register.controls }
  get regBillingControls() {
    let billingGroup = this.register.controls['billing'];
    return billingGroup['controls'];
  }

  onSubmit() {

  }

  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true })
      }
      else {
        return passwordConfirmationInput.setErrors(null);
      }
    }
  }

  countryChanged() {
    let cc = this.regBillingControls['country'].value
    this.commonService.getCountryZones(cc).subscribe(zones => {
      console.log(zones);
      this.zones = zones;
    });
  }

  display: boolean = false;

  showDialog() {
    this.display = true;
  }

  registerCustomer() {
    this.submitted = true;
    console.log(this.register);
    if (this.regControls['privacy'].value == false)
      this.regControls['privacy'].setErrors({ 'agreed': true });
    if (this.register.invalid)
      return false;

    console.log("sdsdssd");


    this.spinner.show();
    this.regControls['userName'].setValue(this.regControls['emailAddress'].value);
    this.commonService.register(this.register.value).subscribe(
      res => {
        console.log(res);
        this.authenticationService.setloggedIn(true);
        localStorage.setItem(AppConstants.REMEMBER_ME, "yes");
        localStorage.setItem(AppConstants.AUTHORIZATION_KEY, res.body.token);
        localStorage.setItem(AppConstants.IS_LOGGED_IN_KEY, "yes");
        localStorage.setItem(AppConstants.LOGGED_IN_USER_Id, res.body.id);
        //this.router.navigate(['/']);
        location.replace("/");  // work around to reload all data correctly
        //location.reload();
        this.spinner.hide();
        this.alert.successMessage("registerationDoneSuccessfully", 'Success');
      },
      error => {
        this.spinner.hide();
        console.log("ERR :: ", error);
        this.alert.errorMessage("error.error", 'Error');
        this.regControls['emailAddress'].setErrors({ 'preExists': true });
      }
    );
  }

}
