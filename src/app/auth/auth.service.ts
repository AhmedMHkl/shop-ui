import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { AppConstants } from '../core/utils/AppConstants';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url = environment.apiUrl;
  private endpoint = "/api/v1";

  isLoggedIn = new Subject<boolean>();

  constructor(
    private http: HttpClient,
    private router: Router,


  ) { }

  login(loginEnttiy: any): Observable<any> {
    return this.http.post<any>(`${this.url}${this.endpoint}/customer/login`, loginEnttiy);
  }

  // AuthenticationRequest {username,password}
  forgetPassword(AuthenticationRequest: any): any {
    return this.http.post<any>(`${this.url}${this.endpoint}/customer/password/reset`, AuthenticationRequest);
  }

  public loggedInUserId() {
    return localStorage.getItem(AppConstants.LOGGED_IN_USER_Id);
  }

  logout() {
    localStorage.removeItem(AppConstants.IS_LOGGED_IN_KEY);
    localStorage.removeItem(AppConstants.AUTHORIZATION_KEY);
    localStorage.removeItem(AppConstants.LOGGED_IN_USER_KEY);
    localStorage.removeItem(AppConstants.REMEMBER_ME);
    localStorage.removeItem(AppConstants.wishlistItem);
    localStorage.removeItem(AppConstants.quotationItems);
    localStorage.removeItem("cartItem");
    localStorage.removeItem("cartCode");
    localStorage.removeItem("cartId");
    localStorage.removeItem("loggedInUserId");
    //localStorage.removeItem("wishlistItem");
    //localStorage.removeItem("compareItem");
    sessionStorage.clear();
    this.isLoggedIn.next(false);
    //this.router.navigate(['/']);
    location.replace("/");
  }

}
