import { EchoMessageService } from './../../core/services/echo-message.service';
import { AuthenticationService } from './../../core/services/authentication.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AppConstants } from 'src/app/core/utils/AppConstants';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import { EchoProductsService } from 'src/app/services/echo-products.service';
import { CartService } from 'src/app/shared/services/cart.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private authService: AuthService,
        private alerMessage: EchoMessageService,
        private translateService: TranslateService,
        private echoProductsService: EchoProductsService,
        private cartService: CartService
    ) { }

    ngAfterViewInit() {
    }

    ngOnDestroy() {
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required],
            rememberMe: ['']
        });

        if (localStorage.getItem(AppConstants.AUTHORIZATION_KEY)) {
            this.router.navigate(['/']);
            location.reload();
        }
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        //console.log("Remember Me " + this.f.rememberMe.value);
        this.loading = true;
        this.authService.login({ username: this.f.email.value, password: this.f.password.value })
            .subscribe(resp => {
                this.authenticationService.setloggedIn(true);

                this.authService.isLoggedIn.next(true)

                console.log(resp);

                const helper = new JwtHelperService();
                // const token = resp.headers.get(AppConstants.AUTHORIZATION_KEY);
                const token = resp.token
                const decodedToken = helper.decodeToken(token);
                const expirationDate = helper.getTokenExpirationDate(token);
                const isExpired = helper.isTokenExpired(token);

                localStorage.setItem(AppConstants.LOGGED_IN_USER_KEY, JSON.stringify(decodedToken));

                if (this.f.rememberMe.value) {
                    localStorage.setItem(AppConstants.REMEMBER_ME, "yes");
                    localStorage.setItem(AppConstants.AUTHORIZATION_KEY, resp.token);
                } else {
                    localStorage.setItem(AppConstants.REMEMBER_ME, "no");
                    sessionStorage.setItem(AppConstants.AUTHORIZATION_KEY, resp.token);
                }

                localStorage.setItem(AppConstants.IS_LOGGED_IN_KEY, "yes");
                localStorage.setItem(AppConstants.LOGGED_IN_USER_Id, resp.id);

                // get customer cart from backend
                let customerCartItems = JSON.parse(localStorage.getItem("cartItem"));
                if (!customerCartItems) {
                    const cartItems = [];
                    this.echoProductsService.getShopingCartByCustomerId(resp.id).subscribe(
                        res => {
                            console.log("cart from backend : ", res);
                            res.products.forEach(element => {
                                const item = { product: element, quantity: element.quantity }
                                cartItems.push(item);
                            });
                            localStorage.setItem("cartItem", JSON.stringify(cartItems));
                            localStorage.setItem("cartCode", res.code);
                            localStorage.setItem("cartId", res.id);
                            this.router.navigate(['/']);
                            location.replace("/");  // work around to reload all data correctly
                        },
                        error => {
                            console.log(error);
                            //this.router.navigate(['/home']);
                            this.router.navigate(['/']);
                            location.replace("/");  // work around to reload all data correctly
                        });
                } else {
                    this.router.navigate(['/']);
                }
            },
                error => {
                    var positionLayout = "toast-top-right";
                    if (sessionStorage.getItem(AppConstants.CURRENT_LANGUAGE_KEY) == "ar" || localStorage.getItem(AppConstants.CURRENT_LANGUAGE_KEY) == "ar") {
                        positionLayout = "toast-top-left";
                    }
                    if (error)
                        this.alerMessage.errorMessage("error", "Login failed", positionLayout, 1000);
                    else
                        this.alerMessage.errorMessage("Incorrect user name or password", "Login failed", positionLayout, 1000);

                    console.log('--------------------- Error in Login -------------------');
                    this.loading = false;
                });
    }

}
