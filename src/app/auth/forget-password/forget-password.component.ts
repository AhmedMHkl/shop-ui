import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../auth.service';
import { EchoMessageService } from 'src/app/core/services/echo-message.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {

  @Input() userEmail: string;

  constructor(private authService: AuthService,
    private alert: EchoMessageService,
    private spinner: NgxSpinnerService,
    private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    if (!this.userEmail) {
      this.alert.errorMessage('enter_your_email', 'Error');
      return;
    } else {

      let authRequest = { username: this.userEmail, password: 'testpassword' }
      console.log(authRequest);
      //return

      this.spinner.show();
      this.authService.forgetPassword(authRequest).subscribe(
        res => {
          console.log("Success ", res);
          this.alert.successMessage("passwordHasBeenReset", 'Success');
          this.router.navigate(['/home']);
          this.spinner.hide();
        },
        err => {
          console.log("Error ", err);
          if (err instanceof HttpErrorResponse) {
            if (err.status == 401)
              this.alert.errorMessage("This Email Address not found", 'Error');
          } else
            this.alert.errorMessage("UnKnown Error ...", 'Error');

          this.spinner.hide();
        }
      );
    }
  }

}
