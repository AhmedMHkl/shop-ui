import { Injectable } from '@angular/core';
import { BaseEntity } from '../shared/classes/base-entity';
import { Product } from '../shared/classes/product';

@Injectable({
  providedIn: 'root'
})
export class BaseCacheService {

  private cache = new Map();
  private cacheSize = 10;
  private expireOn = 1000 * 60 * 5;   // 5 minutes
  private alive: Date;

  constructor() {
    this.alive = new Date()
  }

  set<A extends BaseEntity>(element) {
    if (this.isExpired())
      this.clear()
    if (this.cache.size < 100)
      this.cache.set(element.id, element)
  }

  setList(elements: any[]) {
    if (this.isExpired())
      this.clear()
    elements.forEach(element => {
      this.set(element)
    });
  }

  get(id: number): Product {
    if (this.cache.has(id))
      return this.cache.get(id)
    else
      return null
  }

  delete(id) {
    if (this.cache.has(id))
      this.cache.delete(id)
  }

  clear() {
    if (this.cache.size > 0)
      this.cache.clear()
  }

  isExpired(): boolean {
    if (this.alive.getMilliseconds() + this.expireOn > new Date().getMilliseconds())
      return false;
    else
      return true;
  }

}
