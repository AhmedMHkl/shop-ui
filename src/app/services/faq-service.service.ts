import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FaqServiceService {

  private url = environment.apiUrl;
  private endPoint = "/api/v1";
  private currentLanguage = localStorage.getItem("currentLanguage");

  private headers = new HttpHeaders({
    "Content-Type": "application/json",
    "Accept": "application/json"
  });
  constructor(private http: HttpClient) { }

  getAllFAQs(): Observable<any>{
    return this.http.get<any>(`${this.url}${this.endPoint}/faqs`, {
      headers: this.headers,
    })
  }
}
