import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EsSearchService {

  private url = environment.apiUrl;
  private endPoint = "/api/v1";
  private currentLanguage = localStorage.getItem("currentLanguage")
  private esUrl = "http://192.168.1.2:9200";
  //private esUrl = "http://localhost:8080";
  private esEndPoint = "/product_en_default";

  constructor(
    private http: HttpClient,

  ) { }

  public productSearch(query: string): Observable<any> {
    const searchProductRequest = {
      query: query,
      count: 10,
      start: 0,
    }
    return this.http.post(`${this.url}${this.endPoint}/search`, searchProductRequest, {
      params: new HttpParams().set("lang", this.currentLanguage)
    });
  }

  public autoComplete(query: string) {
    return this.http.post(`${this.url}${this.endPoint}/search/autocomplete`, { query: query }, {
      params: new HttpParams().set("lang", this.currentLanguage)
    })
  }

  // public productSearchByManufacturerId(query: any): Observable<any> {

  //   return this.http.get(`${this.url}${this.esEndPoint}/_search?q=manufacturer:` + query);
  // }


  public productSearchByManufacturerId(manufacturerId: any): Observable<any> {
    return this.http.get(`${this.url}${this.endPoint}/products`, {
      params: new HttpParams()
        .set('lang', this.currentLanguage)
        .set('manufacturer', manufacturerId)
    })
  }
}
