import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductSearchService {

  private url = environment.apiUrl;
  private endPoint = "/api/v1";
  private currentLanguage = localStorage.getItem("currentLanguage")

  constructor(
    private http: HttpClient) { }

  public search(keyword: string, type: string = 'product'): Observable<any> {
    return this.http.get(`${this.url}${this.endPoint}/product/search`, {
      params: new HttpParams()
        .set('keyword', keyword)
        .set('type', type)
        .set('language', this.currentLanguage)
        .set('merchantStore', 'DEFAULT')
    });
  }

}
