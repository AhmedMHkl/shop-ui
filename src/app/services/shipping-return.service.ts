import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShippingReturnService {

  private url = environment.apiUrl;
  private endPoint = "/api/v1";
  private currentLanguage = localStorage.getItem("currentLanguage");

  private headers = new HttpHeaders({
    "Content-Type": "application/json",
    "Accept": "application/json"
  });
  constructor(private http: HttpClient) { }

getSystemInfo(): Observable<any> {
    return this.http.get<any>(`${this.url}${this.endPoint}/AllSystemInfo`,{
      params: new HttpParams()
        .set('lang', this.currentLanguage)
    }
    );
  }

  getSystemInfoDetails(systemInfoId: number): Observable<any> {
    return this.http.get<any>(`${this.url}${this.endPoint}/detailsBySystemInfoId/${systemInfoId}`, {
      params: new HttpParams()
        .set('lang', this.currentLanguage)
    }
    );
  }

}
