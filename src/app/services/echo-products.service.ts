import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Product } from '../shared/classes/product';
import { BaseCacheService } from './base-cache.service';

@Injectable({
  providedIn: 'root'
})
export class EchoProductsService extends BaseCacheService {

  private url = environment.apiUrl;
  private endPoint = "/api/v1";
  public currency: string = 'USD';
  private currentLanguage = localStorage.getItem("currentLanguage");
  public currentlyPreviewdProductId: number = -1;

  constructor(
    private http: HttpClient
  ) {
    super()
  }

  /* 
    list products [ paging - sort - filtering ]
  */
  public getAllProducts(start: number = null, pageSize: number = null, categoryId: string = null): Observable<any> {
    this.currentLanguage = this.currentLanguage == null ? 'en' : this.currentLanguage
    let param = categoryId == null ? '' : '?category=' + categoryId
    return this.http.get(`${this.url}${this.endPoint}/products${param}`, {
      params: new HttpParams()
        .set('lang', this.currentLanguage)
        .set('start', start.toString())
        .set('count', pageSize.toString())
      // .set('productName', productName)
      //.set('category', categoryId)
    }).pipe(
      tap((ele) => {
        console.log("Process " + ele)
        this.setList(ele.products)
      },
        (err) => console.error(err),
      )
    );
  }

  public listLightProducts(first: any = 0, limit: any = 10): any {
    return this.http.get<any>(`${this.url}${this.endPoint}/products/light`,
      {
        params: new HttpParams()
          .set('start', first.toString())
          .set('pageSize', limit.toString())
          .set('lang', this.currentLanguage || 'en')
        //.set('store', 'DEFAULT')
      }
    );
  }

  public countLightProduct(): any {
    return this.http.get<any>(`${this.url}${this.endPoint}/products/light/count`,
      {
        params: new HttpParams()
          .set('lang', this.currentLanguage || 'en')
        //.set('store', 'DEFAULT')
      }
    );
  }

  public getAllLighyProducts(categoryId: string = null): Observable<any> {
    this.currentLanguage = this.currentLanguage == null ? 'en' : this.currentLanguage
    let param = categoryId == null ? '' : '?category=' + categoryId
    return this.http.get(`${this.url}${this.endPoint}/light/products${param}`, {
      params: new HttpParams()
        .set('lang', this.currentLanguage)
    });
  }

  public getAllLight(categoryId: string = null): Observable<any> {
    this.currentLanguage = this.currentLanguage == null ? 'en' : this.currentLanguage
    // let param = categoryId == null ? '' : '?category=' + categoryId
    //${param}
    return this.http.get(`${this.url}${this.endPoint}/light`, {
      params: new HttpParams()
        .set('lang', this.currentLanguage)
    });
  }

  public getProductById(id: number): Observable<Product> {
    if (this.get(id))
      return of(this.get(id))
    return this.http.get<Product>(`${this.url}${this.endPoint}/products/${id}`, {
      params: new HttpParams()
        .set('lang', this.currentLanguage)
    }).pipe(
      tap(ele => {
        this.set(ele)
      },
        (err) => { })
    );
  }

  public getProductByCategoryIds(ids: any[]): Observable<any> {
    // if (this.get(id))
    //   return of(this.get(id))
    let param = ids == null ? [] : '?categoryIds=' + ids
    return this.http.get<any>(`${this.url}${this.endPoint}/getProductsByCategoryIds${param}`, {
      params: new HttpParams()
        .set('lang', this.currentLanguage)
    }).pipe(
      tap(ele => {
        this.set(ele)
      },
        (err) => { })
    );
  }

  public getProductByName(productName: string): Observable<any> {
    // if (this.get(id))
    //   return of(this.get(id))
    let param = productName == null ? '' : '?productName=' + productName
    return this.http.get<any>(`${this.url}${this.endPoint}/getProductsByCategoryIds${param}`, {
      params: new HttpParams()
        .set('lang', this.currentLanguage)
    }).pipe(
      tap(ele => {
        this.set(ele)
      },
        (err) => { })
    );
  }

  public getVisableCategory(visable: boolean): Observable<any> {
    // return this.http.get('assets/data/products.json').map((res:any) => res.json())
    return this.http.get<any>(`${this.url}${this.endPoint}/categories/list/${visable}`);
  }

  // Observable Product Array
  public getAllProductsLight(): Observable<any> {
    return this.http.get(`${this.url}${this.endPoint}/productLight/100`);
  }

  public listRandomProductsInsale(count: number): Observable<any> {
    return this.http.get(`${this.url}${this.endPoint}/products/random/insale/${count}`,
      { params: new HttpParams().set('lang', this.currentLanguage) });
  }

  public getRandomProducts(count: number): Observable<any> {
    return this.http.get(`${this.url}${this.endPoint}/products/random/${count}`);
  }


  public updateProduct(product: any): Observable<any> {
    return this.http.put(`${this.url}${this.endPoint}/auth/products/${product.id}`, product);
  }

  // cart

  public addLoggedInCustomerProductToCart(customerId: any, cartItem: any): Observable<any> {
    return this.http.post(`${this.url}${this.endPoint}/customers/${customerId}/cart`, cartItem);
  }

  public addAnonymousProductToCart(cartItem: any): Observable<any> {
    return this.http.post(`${this.url}${this.endPoint}/cart`, cartItem);
  }

  public getShopingCartByCustomerId(customerId: any): Observable<any> {
    return this.http.get(`${this.url}${this.endPoint}/customers/${customerId}/cart`);
  }

  public getShopingCartByCode(cartCode: any): Observable<any> {
    return this.http.get(`${this.url}${this.endPoint}/cart/${cartCode}`);
  }

  public modifyCart(cartCode: any, cartItem: any): Observable<any> {
    return this.http.put(`${this.url}${this.endPoint}/cart/${cartCode}`, cartItem);
  }

  public createNewsLetter(optin: any): Observable<any> {
    return this.http.post(`${this.url}${this.endPoint}/newsletter`, optin);
  }

  ///// FEATURED
  public pageFeaturedProducts(): Observable<any> {
    return this.http.get<any>(`${this.url}${this.endPoint}/product/relatationship/featured`);
  }

  // RELATED
  public pageRelatedItems(productId: string): Observable<any[]> {
    return this.http.get<any>(`${this.url}${this.endPoint}/product/${productId}/related`);
  }

  public AllProductsRelatedItems(ids): Observable<any> {
    return this.http.get<any>(`${this.url}${this.endPoint}/products/compare/related`, {
      params: new HttpParams().set("Ids", ids)
        .set('lang', this.currentLanguage)
    });
  }


  // get product reviews
  public getProductReview(productId: number): Observable<any> {
    return this.http.get(`${this.url}${this.endPoint}/products/${productId}/reviews`);
  }

  public getCategoryVariants(cat: any): Observable<any> {
    return this.http.get(`${this.url}${this.endPoint}/category/${cat}/variants`, {
      params: new HttpParams()
        .set('lang', this.currentLanguage || 'ar')
    });
  }

  // get product reviews count
  public getProductReviewCount(productId: number): Observable<any> {
    return this.http.get(`${this.url}${this.endPoint}/products/${productId}/reviews/count`);
  }

  // get Update product reviews count
  public updateProductReviewCount(productId: number): Observable<any> {
    return this.http.post(`${this.url}${this.endPoint}/updateProductReviewCount/${productId}`, {});
  }

  // /visible/category/products
  public visibleCategoryProducts(): Observable<any> {
    this.currentLanguage = this.currentLanguage == null ? 'en' : this.currentLanguage
    return this.http.get(`${this.url}${this.endPoint}/visible/category/products`, {
      params: new HttpParams()
        .set('lang', this.currentLanguage)
    });
  }

}
