import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserInfo } from '../my-account/dashboard/model/user-info';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private url = environment.apiUrl;
  private endPoint = "/api/v1";

  private headers = new HttpHeaders({
    "Content-Type": "application/json",
    "Accept": "application/json"
  });

  constructor(
    private http: HttpClient
  ) { }

  public getCustomerInfo(): Observable<UserInfo> {
    return this.http.get<UserInfo>(`${this.url}${this.endPoint}/auth/customers/profile`, {
      headers: this.headers,
    });
  }

  public updateCustomerAddress(id: string, customerAddress: any): Observable<any> {
    return this.http.put(`${this.url}${this.endPoint}/customer/${id}/billing`, customerAddress)
  }

  public updateCustomerFullName(id: string, fname: string, lname: string): Observable<any> {
    return this.http.put(`${this.url}${this.endPoint}/customer/${id}/full-name`, null, {
      headers: this.headers,
      params: new HttpParams()
        .set('fname', fname.toString())
        .set('lname', lname.toString())
    })
  }

  public updateEmailAddress(id: string, email: string): Observable<any> {
    return this.http.put(`${this.url}${this.endPoint}/customer/${id}/email-address`, null, {
      headers: this.headers,
      params: new HttpParams()
        .set('newEmailAddress', email.toString())
    })
  }

  public updateDefaultLanguage(id: string, langCode: string): Observable<any> {
    return this.http.put(`${this.url}${this.endPoint}/customer/${id}/default-language`, null, {
      headers: this.headers,
      params: new HttpParams()
        .set('langCode', langCode.toString())
    })
  }

}
